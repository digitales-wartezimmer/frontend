import { shallowMount } from '@vue/test-utils'
import TravelReturn from '@/pages/travel-return.vue'

describe('TravelReturn Page', () => {
  it('renders', () => {
    const wrapper = shallowMount(TravelReturn, {
      mocks: {
        $t: msg => msg,
        localePath: () => {},
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': true,
      },
    })
    expect(wrapper.html()).toMatchSnapshot()
    const content = wrapper.find('landingpagecontent-stub')
    expect(content.exists()).toBeTruthy()
    expect(content.attributes('formurl')).toEqual('/forms/travel-return?startOver=true')
  })
})
