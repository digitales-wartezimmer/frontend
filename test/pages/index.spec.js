import { shallowMount } from '@vue/test-utils'
import Index from '@/pages/index.vue'

describe('Index Page', () => {
  it('renders', () => {
    const wrapper = shallowMount(Index, {
      mocks: {
        $t: msg => msg,
        localePath: () => {},
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': true,
      },
    })
    expect(wrapper.html()).toMatchSnapshot()
    const content = wrapper.find('landingpagecontent-stub')
    expect(content.exists()).toBeTruthy()
    expect(content.attributes('formurl')).toEqual('/forms/master?startOver=true')
  })
})
