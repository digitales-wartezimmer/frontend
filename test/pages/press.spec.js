import { shallowMount } from '@vue/test-utils'
import Press from '@/pages/press.vue'

describe('Press Page', () => {
  it('renders', () => {
    const wrapper = shallowMount(Press, {
      mocks: {
        $t: msg => msg,
        localePath: () => {},
      },
      stubs: {
        'font-awesome-icon': true,
      },
    })
    expect(wrapper.html()).toMatchSnapshot()
  })
})
