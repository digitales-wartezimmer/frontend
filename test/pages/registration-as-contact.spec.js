import { shallowMount } from '@vue/test-utils'
import RegistrationAsContact from '@/pages/registration-as-contact.vue'

describe('RegistrationAsContact Page', () => {
  it('renders', () => {
    const wrapper = shallowMount(RegistrationAsContact, {
      mocks: {
        $t: msg => msg,
        localePath: () => {},
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': true,
      },
    })
    expect(wrapper.html()).toMatchSnapshot()
    const content = wrapper.find('landingpagecontent-stub')
    expect(content.exists()).toBeTruthy()
    expect(content.attributes('formurl')).toEqual('/forms/registration-as-contact?startOver=true')
  })
})
