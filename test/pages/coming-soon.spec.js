import { shallowMount } from '@vue/test-utils'
import ComingSoon from '@/pages/coming-soon.vue'

describe('ComingSoon Page', () => {
  it('renders', () => {
    const wrapper = shallowMount(ComingSoon, {
      mocks: {
        $t: msg => msg,
        localePath: () => {},
      },
      stubs: {
        'font-awesome-icon': true,
      },
    })
    expect(wrapper.html()).toMatchSnapshot()
  })
})
