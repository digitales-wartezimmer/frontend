import { shallowMount } from '@vue/test-utils'
import About from '@/pages/about.vue'

describe('About Page', () => {
  it('renders', () => {
    const wrapper = shallowMount(About, {
      mocks: {
        $t: msg => msg,
        localePath: () => {},
      },
      stubs: {
        'font-awesome-icon': true,
      },
    })
    expect(wrapper.html()).toMatchSnapshot()
  })
})
