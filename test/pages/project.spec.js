import { shallowMount } from '@vue/test-utils'
import Project from '@/pages/project.vue'

describe('Project Page', () => {
  it('renders', () => {
    const wrapper = shallowMount(Project, {
      mocks: {
        $t: msg => msg,
        localePath: () => {},
      },
      stubs: {
        'font-awesome-icon': true,
      },
    })
    expect(wrapper.html()).toMatchSnapshot()
  })
})
