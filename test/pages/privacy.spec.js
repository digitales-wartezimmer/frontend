import { shallowMount } from '@vue/test-utils'
import Privacy from '@/pages/privacy.vue'

describe('Privacy Page', () => {
  it('renders', () => {
    const wrapper = shallowMount(Privacy, {
      mocks: {
        $t: msg => msg,
        localePath: () => {},
      },
    })
    expect(wrapper.html()).toMatchSnapshot()
  })
})
