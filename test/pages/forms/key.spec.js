import { createLocalVue, mount } from '@vue/test-utils'
import { ValidationObserver } from 'vee-validate'
import FormKey from '@/pages/forms/_form/_key.vue'
import { fields, formIDs, formMap } from '@/content/forms/__mocks__'

jest.mock('@/content/forms')

jest.useFakeTimers()

describe('Form Key Page', () => {
  let store
  let route
  let router
  const localVue = createLocalVue()
  localVue.component('ValidationObserver', ValidationObserver)

  function mountCmp (key) {
    return mount(FormKey, {
      stubs: {
        nuxt: true,
        'fetch-health-office': true,
        FormReviewer: true,
        FormSubmitter: true,
        EmailDisplay: true,
        FormContent: true,
        Loading: true,
        QuarantineDate: true,
        Button: true,
        StartOverButton: true,
        Success: true,
      },
      mocks: {
        $t: msg => msg,
        localePath: arg => arg,
        $store: {
          ...store,
          getters: {
            ...store.getters,
            'formAStore/getField': jest.fn().mockImplementation((fieldName) => {
              if (fieldName === 'responseData') {
                return {
                  data: {
                    email: 'data',
                  },
                }
              }
              if (fieldName === 'quarantineStartDate') {
                return {
                  date: 'quarantine',
                }
              }
              return fieldName === 'currentStep' ? key : fieldName
            }),
          },
        },
        $route: {
          ...route,
          params: {
            ...route.params,
            key,
          },
        },
        $router: router,
        $config: {
          API_URL: '/test/endpoint',
        },
      },
      localVue,
    })
  }

  beforeEach(() => {
    store = {
      getters: {},
      commit: jest.fn(),
      state: {
        idempotencyToken: '123abc',
      },
    }
    route = {
      params: {
        form: formIDs[0],
        key: 'A',
      },
      query: {
        my: 'query',
      },
      name: 'form_key',
    }
    router = {
      replace: jest.fn(),
      push: jest.fn(),
    }
    jest.resetAllMocks()
  })
  it('renders', () => {
    const cmp = mountCmp('A')
    expect(cmp.html()).toMatchSnapshot()
  })
  it('validates the form id', () => {
    const cmp = mountCmp('A')
    const validate = cmp.vm.$options.validate
    expect(validate({
      params: {
        form: undefined,
        key: 'fetch_health_office',
      },
    })).toBeFalsy()
    expect(validate({
      params: {
        form: null,
        key: 'fetch_health_office',
      },
    })).toBeFalsy()
    expect(validate({
      params: {
        form: '',
        key: 'fetch_health_office',
      },
    })).toBeFalsy()
    expect(validate({
      params: {
        form: 'other',
        key: 'fetch_health_office',
      },
    })).toBeFalsy()
    formIDs.forEach((id) => {
      expect(validate({
        params: {
          form: id,
          key: 'invalid',
        },
      })).toBeFalsy()
      expect(validate({
        params: {
          form: id,
          key: Object.values(formMap[id].form.steps)[0].key,
        },
      })).toBeTruthy()
    })
  })
  it('sets the current step on mount', () => {
    mountCmp('A')
    expect(store.commit).toHaveBeenCalledWith('formAStore/updateField', {
      path: 'currentStep',
      value: 'A',
    })
  })
  describe('it can navigate forwards', () => {
    it('can navigate forwards', async () => {
      const cmp = mountCmp(fields.fieldWithNextPrevious.key)
      expect(cmp.html()).toMatchSnapshot()
      const btn = cmp.findAll('button-stub').at(0)
      expect(btn.exists()).toBeTruthy()
      expect(btn.text()).toContain('pages.form.proceed')
      await btn.trigger('click')
      await cmp.vm.proceed()
      expect(router.push).toHaveBeenCalledWith({
        name: 'form_key',
        params: {
          form: 'formA',
          key: 'NEXT',
        },
        query: {
          my: 'query',
        },
      })
    })
    it('a field can hide the forwards button', async () => {
      const cmp = mountCmp(fields.fieldWithDisabledNextPrevious.key)
      expect(cmp.html()).toMatchSnapshot()
      const btns = cmp.findAll('button-stub')
      expect(btns.length).toEqual(0)

      await cmp.vm.proceed()
      expect(router.push).not.toHaveBeenCalled()
    })
    it('handles empty forwards value', async () => {
      const cmp = mountCmp(fields.fieldWithEmptyNextPrevious.key)
      expect(cmp.html()).toMatchSnapshot()
      const btns = cmp.findAll('button-stub')
      expect(btns.length).toEqual(0)

      await cmp.vm.proceed()
      expect(router.push).not.toHaveBeenCalled()
    })
    it('can navigate forwards depending on function', async () => {
      const cmp = mountCmp(fields.fieldWithNextPreviousFn.key)
      expect(cmp.html()).toMatchSnapshot()
      const btn = cmp.findAll('button-stub').at(0)
      expect(btn.exists()).toBeTruthy()
      expect(btn.text()).toContain('pages.form.proceed')
      await btn.trigger('click')
      await cmp.vm.proceed()
      expect(router.push).toHaveBeenCalledWith({
        name: 'form_key',
        params: {
          form: 'formA',
          key: 'NEXT',
        },
        query: {
          my: 'query',
        },
      })
    })
  })
  describe('it can navigate backwards', () => {
    it('can navigate backwards', async () => {
      const cmp = mountCmp(fields.fieldWithNextPrevious.key)
      expect(cmp.html()).toMatchSnapshot()
      const btn = cmp.findAll('button-stub').at(1)
      expect(btn.exists()).toBeTruthy()
      expect(btn.text()).toContain('pages.form.back')
      await btn.trigger('click')
      await cmp.vm.back()
      expect(router.push).toHaveBeenCalledWith({
        name: 'form_key',
        params: {
          form: 'formA',
          key: 'PREV',
        },
        query: {
          my: 'query',
        },
      })
    })
    it('a field can hide the backwards button', async () => {
      const cmp = mountCmp(fields.fieldWithDisabledNextPrevious.key)
      expect(cmp.html()).toMatchSnapshot()
      const btns = cmp.findAll('button-stub')
      expect(btns.length).toEqual(0)

      await cmp.vm.back()
      expect(router.push).not.toHaveBeenCalled()
    })
    it('handles empty backwards value', async () => {
      const cmp = mountCmp(fields.fieldWithEmptyNextPrevious.key)
      expect(cmp.html()).toMatchSnapshot()
      const btns = cmp.findAll('button-stub')
      expect(btns.length).toEqual(0)

      await cmp.vm.back()
      expect(router.push).not.toHaveBeenCalled()
    })
    it('can navigate backwards depending on function', async () => {
      const cmp = mountCmp(fields.fieldWithNextPreviousFn.key)
      expect(cmp.html()).toMatchSnapshot()
      const btn = cmp.findAll('button-stub').at(1)
      expect(btn.exists()).toBeTruthy()
      expect(btn.text()).toContain('pages.form.back')
      await btn.trigger('click')
      await cmp.vm.back()
      expect(router.push).toHaveBeenCalledWith({
        name: 'form_key',
        params: {
          form: 'formA',
          key: 'PREV',
        },
        query: {
          my: 'query',
        },
      })
    })
  })
  it('handles an onchange method on field change', () => {
    const cmp = mountCmp(fields.fieldWithOnChange.key)
    const content = cmp.find('formcontent-stub')
    expect(content.exists()).toBeTruthy()
    content.vm.$emit('change', 'test Val')
    expect(store.commit).toHaveBeenCalledWith('test', 'test Val')
  })
  it('handles an onchange method for fields without onchange', () => {
    const cmp = mountCmp(fields.singleField.key)
    const content = cmp.find('formcontent-stub')
    expect(content.exists()).toBeTruthy()
    content.vm.$emit('change', 'test Val')
    expect(store.commit).not.toHaveBeenCalledWith('test', 'test Val')
  })
  describe('renders the different fields', () => {
    it('renders a single field', () => {
      const cmp = mountCmp('singleField')
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.find('formcontent-stub')
      expect(content.exists()).toBeTruthy()
      expect(content.props('field')).toEqual(fields.singleField.section.fields[0])
    })
    it('renders a section with multiple fields', () => {
      const cmp = mountCmp('sectionMoreFields')
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.findAll('formcontent-stub')
      expect(content.length).toEqual(2)
      expect(content.at(0).props('field')).toEqual(fields.sectionMoreFields.section.fields[0])
      expect(content.at(1).props('field')).toEqual(fields.sectionMoreFields.section.fields[1])
    })
    it('does not render a hidden field', () => {
      const cmp = mountCmp('hiddenField')
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.find('formcontent-stub')
      expect(content.exists()).toBeTruthy()
      expect(content.props('field')).toEqual(fields.hiddenField.section.fields[0])
    })
    it('renders a multi field', () => {
      const cmp = mountCmp('multiField')
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.findAll('formcontent-stub')
      expect(content.length).toEqual(2)
      expect(content.at(0).props('field')).toEqual(fields.multiField.section.fields[0][0])
      expect(content.at(0).props('multi')).toBeTruthy()
      expect(content.at(1).props('field')).toEqual(fields.multiField.section.fields[0][1])
      expect(content.at(1).props('multi')).toBeTruthy()
    })
    it('renders a FetchHealthOffice Component', () => {
      const cmp = mountCmp(fields.fetchHealthOffice.key)
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.find('fetch-health-office-stub')
      expect(content.exists()).toBeTruthy()
      expect(content.props('zip')).toEqual('zip')
    })
    it('renders a FormReviewer Component', () => {
      const cmp = mountCmp(fields.formReviewer.key)
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.find('formreviewer-stub')
      expect(content.exists()).toBeTruthy()
      expect(content.props('fields')).toMatchSnapshot()
      expect(content.props('storeKey')).toEqual('formAStore')
      expect(content.props('formId')).toEqual('formA')
    })
    it('renders a FormSubmitter Component', () => {
      const cmp = mountCmp(fields.formSubmitter.key)
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.find('formsubmitter-stub')
      expect(content.exists()).toBeTruthy()
      expect(content.attributes('form-id')).toEqual('formA')
      expect(content.props('url')).toEqual('/test/endpoint')
      expect(content.props('data')).toEqual({ foo: 'bar' })
    })
    it('renders a EmailDisplay Component', () => {
      const cmp = mountCmp(fields.emailDisplay.key)
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.find('emaildisplay-stub')
      expect(content.exists()).toBeTruthy()
      expect(content.props('responseData')).toEqual({ data: { email: 'data' } })
    })
    it('renders a Success Component', () => {
      const cmp = mountCmp(fields.success.key)
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.find('success-stub')
      expect(content.exists()).toBeTruthy()
      expect(content.props('responseData')).toEqual({ data: { email: 'data' } })
    })
    it('renders a StartOverButton Component', () => {
      const cmp = mountCmp(fields.startOverButton.key)
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.find('startoverbutton-stub')
      expect(content.exists()).toBeTruthy()
    })
    it('renders a QuarantineDate Component', () => {
      const cmp = mountCmp(fields.quarantineDate.key)
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.find('quarantinedate-stub')
      expect(content.exists()).toBeTruthy()
      expect(content.props('date')).toEqual({ date: 'quarantine' })
    })
    it('renders a Loading Component', () => {
      const cmp = mountCmp(fields.loading.key)
      expect(cmp.html()).toMatchSnapshot()
      const content = cmp.find('loading-stub')
      expect(content.exists()).toBeTruthy()
    })
  })

  describe('it can change to another form', () => {
    it('changes to another form on change form step', () => {
      mountCmp(fields.changeForm.key)
      expect(router.replace).toHaveBeenCalledWith({
        name: 'forms-form',
        params: {
          form: 'otherForm',
          key: 'changeForm',
        },
        query: {
          my: 'query',
          startOver: 'true',
          previousForm: 'formA|stepA',
        },
      })
    })
    it('changes to another form on function evaluation', () => {
      mountCmp(fields.changeFormWithFn.key)
      expect(router.replace).toHaveBeenCalledWith({
        name: 'forms-form',
        params: {
          form: 'anotherForm',
          key: 'changeFormWithFn',
        },
        query: {
          my: 'query',
          startOver: 'true',
          previousForm: 'formA|stepA',
        },
      })
    })
    it('handles empty form on form change', () => {
      mountCmp(fields.changeFormWithoutForm.key)
      expect(router.replace).toHaveBeenCalledWith({
        name: 'forms-form',
        params: {
          form: 'formA',
          key: 'changeFormWithoutForm',
        },
        query: {
          my: 'query',
          previousForm: 'formA|stepA',
        },
      })
    })
  })
  describe('it chan navigate back from one form to the previous form', () => {
    beforeEach(() => {
      route.query.previousForm = 'prevForm|prevStep'
    })
    it('displays the back button', () => {
      const cmp = mountCmp(fields.changeFormBack.key)
      expect(cmp.html()).toMatchSnapshot()
      const btn = cmp.findAll('button-stub').at(0)
      expect(btn.exists()).toBeTruthy()
      expect(btn.text()).toContain('pages.form.back')
    })
    it('navigates back to the previous form on button click', async () => {
      const cmp = mountCmp(fields.changeFormBack.key)
      expect(cmp.html()).toMatchSnapshot()
      const btn = cmp.findAll('button-stub').at(0)
      expect(btn.exists()).toBeTruthy()
      await btn.trigger('click')
      await cmp.vm.back()
      expect(router.push).toHaveBeenCalledWith({
        name: 'form_key',
        params: {
          form: 'prevForm',
          key: 'prevStep',
        },
        query: {
          my: 'query',
        },
      })
    })
  })
})
