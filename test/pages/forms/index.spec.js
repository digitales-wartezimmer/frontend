import { shallowMount } from '@vue/test-utils'
import FormIndex from '@/pages/forms/_form/index.vue'
import { formIDs } from '@/content/forms'

describe('Form Index Page', () => {
  let store
  let route
  let router

  function mountCmp () {
    return shallowMount(FormIndex, {
      stubs: {
        nuxt: true,
      },
      mocks: {
        $t: msg => msg,
        localePath: arg => arg,
        $store: store,
        $route: route,
        $router: router,
      },
    })
  }

  beforeEach(() => {
    store = {
      getters: {
        'masterForm/getField': jest.fn().mockReturnValue('risk_area'),
      },
      commit: jest.fn(),
    }
    route = {
      params: {
        form: formIDs[0],
      },
      query: {},
    }
    router = {
      push: jest.fn(),
      replace: jest.fn(),
    }
  })
  it('renders', () => {
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('validates the form id', () => {
    const cmp = mountCmp()
    const validate = cmp.vm.$options.validate
    expect(validate({ params: { form: undefined } })).toBeFalsy()
    expect(validate({ params: { form: null } })).toBeFalsy()
    expect(validate({ params: { form: '' } })).toBeFalsy()
    expect(validate({ params: { form: 'other' } })).toBeFalsy()
    formIDs.forEach((id) => {
      expect(validate({ params: { form: id } })).toBeTruthy()
    })
  })
  it('can read and update the current step', () => {
    const cmp = mountCmp(formIDs[0])
    expect(cmp.vm.currentStep).toEqual('risk_area')
    cmp.vm.currentStep = 'other'
    expect(store.commit).toHaveBeenCalledWith('masterForm/updateField', {
      path: 'currentStep',
      value: 'other',
    })
  })
  it('redirects to first step even with value stored in current step', () => {
    mountCmp(formIDs[0])
    expect(router.replace).toHaveBeenCalledWith({
      name: 'forms-form-key',
      params: {
        form: 'master',
        key: 'contact',
      },
      query: {},
    })
  })
  it('redirects to first step', () => {
    store.getters['masterForm/getField'].mockReturnValue(undefined)
    mountCmp(formIDs[0])
    expect(router.replace).toHaveBeenCalledWith({
      name: 'forms-form-key',
      params: {
        form: 'master',
        key: 'contact',
      },
      query: {},
    })
  })
  it('clears data on startOver param', () => {
    route.query.startOver = 'true'
    mountCmp(formIDs[0])
    expect(store.commit).toHaveBeenCalledWith('masterForm/reset')
    expect(router.replace).toHaveBeenCalledWith({
      name: 'forms-form-key',
      params: {
        form: 'master',
        key: 'contact',
      },
      query: {},
    })
  })
})
