import { shallowMount } from '@vue/test-utils'
import Sitemap from '@/pages/sitemap.vue'

describe('Sitemap Page', () => {
  it('renders', () => {
    const wrapper = shallowMount(Sitemap, {
      mocks: {
        $t: msg => msg,
        localePath: () => {},
      },
      stubs: {
        'font-awesome-icon': true,
      },
    })
    expect(wrapper.html()).toMatchSnapshot()
  })
})
