import { shallowMount } from '@vue/test-utils'
import FormLayout from '@/layouts/form.vue'
import { formIDs } from '@/content/forms'

describe('FormLayout', () => {
  const store = {
    getters: {
      'masterForm/getField': jest.fn().mockReturnValue('contact'),
    },
    commit: jest.fn(),
  }
  function mountCmp (formId) {
    return shallowMount(FormLayout, {
      propsData: {
        error: {
          statusCode: 404,
        },
      },
      stubs: {
        'client-only': true,
        nuxt: true,
      },
      mocks: {
        $t: msg => msg,
        localePath: arg => arg,
        $i18n: {
          locale: 'de',
        },
        $nuxtI18nSeo: () => ({
          link: [{ link: 'seo' }],
          meta: [{ meta: 'seo' }],
          htmlAttrs: [{ htmlAttrs: 'seo' }],
        }),
        $route: {
          params: {
            form: formId,
          },
        },
        $store: store,
      },
    })
  }
  it('renders', () => {
    const cmp = mountCmp(formIDs[0])
    expect(cmp.html()).toMatchSnapshot()
    const progress = cmp.find('formprogressbar-stub')
    expect(progress.exists()).toBeTruthy()
    expect(progress.props('currentStepKey')).toEqual('fetch_health_office')
  })
  it('handles undefined form id', () => {
    const cmp = mountCmp(undefined)
    expect(cmp.html()).toMatchSnapshot()
    const progress = cmp.find('formprogressbar-stub')
    expect(progress.exists()).toBeFalsy()
  })
  it('can update the current step', () => {
    const cmp = mountCmp(formIDs[0])
    cmp.vm.currentStep = 'other'
    expect(store.commit).toHaveBeenCalledWith('masterForm/updateField', { path: 'currentStep', value: 'other' })
  })
  it('validates the form id', () => {
    const cmp = mountCmp(formIDs[0])
    const validate = cmp.vm.$options.validate
    expect(validate({ params: { form: undefined } })).toBeFalsy()
    expect(validate({ params: { form: null } })).toBeFalsy()
    expect(validate({ params: { form: '' } })).toBeFalsy()
    expect(validate({ params: { form: 'other' } })).toBeFalsy()
    formIDs.forEach((id) => {
      expect(validate({ params: { form: id } })).toBeTruthy()
    })
  })
})
