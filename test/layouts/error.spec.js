import { shallowMount } from '@vue/test-utils'
import ErrorLayout from '@/layouts/error.vue'

describe('ErrorLayout', () => {
  const te = jest.fn()
  function mountCmp () {
    return shallowMount(ErrorLayout, {
      propsData: {
        error: {
          statusCode: 404,
        },
      },
      mocks: {
        $t: msg => msg,
        $te: te,
        localePath: arg => arg,
        $i18n: {
          locale: 'de',
        },
        $nuxtI18nSeo: () => ({
          link: [{ link: 'seo' }],
          meta: [{ meta: 'seo' }],
          htmlAttrs: [{ htmlAttrs: 'seo' }],
        }),
      },
    })
  }
  it('renders', () => {
    te.mockImplementation(arg => arg)
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('renders a generic error if error is unknown', () => {
    te.mockImplementation(() => undefined)
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
})
