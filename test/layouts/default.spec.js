import { shallowMount } from '@vue/test-utils'
import DefaultLayout from '@/layouts/default.vue'

describe('DefaultLayout', () => {
  function mountCmp (slots) {
    return shallowMount(DefaultLayout, {
      stubs: {
        nuxt: true,
      },
      slots,
      mocks: {
        $t: msg => msg,
        localePath: arg => arg,
        $i18n: {
          locale: 'de',
        },
        $nuxtI18nSeo: () => ({
          link: [{ link: 'seo' }],
          meta: [{ meta: 'seo' }],
          htmlAttrs: [{ htmlAttrs: 'seo' }],
        }),
      },
    })
  }
  it('renders', () => {
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('renders the header', () => {
    const cmp = mountCmp()
    expect(cmp.find('head-section-stub').exists()).toBeTruthy()
  })
  it('renders the footer', () => {
    const cmp = mountCmp()
    const footer = cmp.find('footer-section-stub')
    expect(footer.exists()).toBeTruthy()
    expect(footer.props('items').length).toEqual(3)
  })
  it('can render the default slot', () => {
    const cmp = mountCmp({
      default: '<p>Default Slot</p>',
    })
    expect(cmp.html()).toMatchSnapshot()
    expect(cmp.html()).toContain('Default Slot')
  })
  it('sets the head meta data', () => {
    const cmp = mountCmp()
    expect(cmp.vm.$options.head.apply(cmp.vm)).toMatchSnapshot()
  })
})
