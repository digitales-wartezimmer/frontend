import dayjs from 'dayjs'

describe('Timezones', () => {
  it('should always be UTC', () => {
    expect(new Date().getTimezoneOffset()).toBe(0)
    expect(dayjs().utcOffset()).not.toBeGreaterThan(0)
    expect(dayjs().utcOffset()).not.toBeLessThan(0)
  })
})
