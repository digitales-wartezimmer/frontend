import { createLocalVue, mount } from '@vue/test-utils'
import { ValidationProvider, extend } from 'vee-validate'
import { required } from 'vee-validate/dist/rules.umd.js'
import axios from 'axios'
import FetchHealthOffice from '@/components/modules/FetchHealthOffice.vue'

jest.mock('axios')

const healthOffice = {
  _id: '123456',
  name: 'Landratsamt München',
  department: 'Abteilung 4 - Öffentliches Gesundheits- und Veterinärwesen',
  code: '1.09.1.84.',
  transmissionType: 'MAIL',
  pdfServiceEnabled: false,
  address: {
    street: 'Mariahilfplatz 17',
    postCode: '81541',
    place: 'München',
  },
  contact: {
    phone: '089 6221-1000',
    fax: '089 6221-1164',
    mail: 'Gesundheitswesen@lra-m.bayern.de',
  },
  jurisdiction: [],
}

const getData = jest.fn()

describe('FetchHealthOffice', () => {
  const localVue = createLocalVue()
  extend('required', required)
  localVue.component('ValidationProvider', ValidationProvider)

  function mountCmp (props = {}) {
    return mount(FetchHealthOffice, {
      propsData: {
        zip: null,
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
        'validation-provider': true,
      },
      mocks: {
        $t: msg => msg,
        $tc: msg => msg,
        $axios: axios,
        $config: {
          HEALTH_OFFICE_URL: 'https://test.com/healthOffice',
        },
      },
      localVue,
    })
  }
  beforeEach(() => {
    jest.clearAllMocks()
    getData.mockReturnValue([healthOffice])
    axios.get.mockImplementation(() => Promise.resolve({
      status: 200,
      headers: {
        'content-type': ['application/json'],
      },
      data: getData(),
    }))
  })

  it('renders', () => {
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('requests the data on zip change', async () => {
    const cmp = mountCmp()
    await cmp.setProps({
      zip: '85737',
    })
    await cmp.vm.$nextTick()
    expect(cmp.html()).toMatchSnapshot()
    expect(cmp.text()).toContain('Landratsamt München')
    await cmp.setProps({
      zip: '',
    })
    await cmp.vm.$nextTick()
    expect(cmp.html()).toMatchSnapshot()
    expect(cmp.text()).not.toContain('Landratsamt München')
  })
  describe('it displays several states', () => {
    let cmp
    beforeEach(() => {
      cmp = mountCmp()
    })
    it('shows a no zip state', async () => {
      await cmp.setProps({
        zip: undefined,
      })
      expect(cmp.html()).toMatchSnapshot()
    })
    it('shows a loading state', async () => {
      await cmp.setProps({
        zip: '85737',
      })
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('ui.fetch-health-office.loading')
      const icon = cmp.find('font-awesome-icon-stub')
      expect(icon.exists()).toBeTruthy()
      expect(icon.attributes('icon')).toEqual('slash')
      expect(icon.attributes('spin')).toEqual('')
    })
    it('shows a state that none were found', async () => {
      getData.mockReturnValue([])
      await cmp.setProps({
        zip: '85737',
      })
      await cmp.vm.$nextTick()
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('ui.fetch-health-office.no-match')
    })
    it('shows a state where a single health office is displayed', async () => {
      await cmp.setProps({
        zip: '85737',
      })
      await cmp.vm.$nextTick()
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('Landratsamt München')
    })
    it('shows a state where multiple health offices is displayed', async () => {
      getData.mockReturnValue([healthOffice, {
        ...healthOffice,
        name: 'Landratsamt Freising',
      }])
      await cmp.setProps({
        zip: '85737',
      })
      await cmp.vm.$nextTick()
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('Landratsamt München')
      expect(cmp.text()).toContain('Landratsamt Freising')
    })
    it('shows an error state', async () => {
      axios.get.mockImplementation(() => Promise.reject(new Error('TEst Error')))
      await cmp.setProps({
        zip: '85737',
      })
      await cmp.vm.$nextTick()
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('error')
    })
    it('shows an unknown error state', async () => {
      axios.get.mockImplementation(() => Promise.resolve({
        status: 404,
        headers: {
          'content-type': ['application/json'],
        },
        data: {},
      }))
      await cmp.setProps({
        zip: '85737',
      })
      await cmp.vm.$nextTick()
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('unknown-error')
    })
  })
  describe('it displays the correct information', () => {
    let cmp
    beforeEach(async () => {
      cmp = mountCmp()
      getData.mockReturnValue([healthOffice])
      await cmp.setProps({
        zip: '85737',
      })
      await cmp.vm.$nextTick()
    })
    it('renders the info', () => {
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain(healthOffice.name)
      expect(cmp.text()).toContain(healthOffice.department)
      expect(cmp.text()).toContain(healthOffice.address.street)
      expect(cmp.text()).toContain(healthOffice.address.postCode)
      expect(cmp.text()).toContain(healthOffice.address.place)
      expect(cmp.text()).toContain(healthOffice.contact.phone)
      expect(cmp.text()).toContain(healthOffice.contact.fax)
      expect(cmp.text()).toContain(healthOffice.contact.mail)
    })
    it('parses the correct phone number link', async () => {
      let link = cmp.find('.m-fetch-health-office__match-phone a')
      expect(link.exists()).toBeTruthy()
      expect(link.attributes('href')).toEqual('tel:004908962211000')

      getData.mockReturnValue([{
        ...healthOffice,
        contact: {
          ...healthOffice.contact,
          phone: '+49 089 1234 567',
        },
      }])
      await cmp.setProps({
        zip: '12345',
      })
      await cmp.vm.$nextTick()

      link = cmp.find('.m-fetch-health-office__match-phone a')
      expect(link.exists()).toBeTruthy()
      expect(link.attributes('href')).toEqual('tel:+490891234567')
    })
    it('renders an email link', () => {
      const link = cmp.find('.m-fetch-health-office__match-mail a')
      expect(link.exists()).toBeTruthy()
      expect(link.attributes('href')).toEqual('mailto:Gesundheitswesen@lra-m.bayern.de')
    })
  })
  it('sets the validation state', async () => {
    const cmp = mountCmp()
    jest.spyOn(cmp.vm.$refs.field, 'validate')
    jest.spyOn(cmp.vm.$refs.field, 'syncValue')
    getData.mockReturnValue([healthOffice])
    await cmp.setProps({
      zip: '85737',
    })
    await cmp.vm.$nextTick()
    expect(cmp.vm.$refs.field.syncValue).toHaveBeenCalled()
    expect(cmp.vm.$refs.field.validate).toHaveBeenCalled()
  })
})
