import { shallowMount } from '@vue/test-utils'
import BigLinks from '@/components/modules/BigLinks.vue'
import BigLink from '@/components/elements/BigLink.vue'

describe('BigLinks', () => {
  const links = [{
    to: '/route',
    external: false,
    caption: 'First Link',
    theme: 'primary',
    description: 'My Description',
    icon: ['my-icon'],
  }, {
    to: '/other',
    external: true,
    caption: 'Second Link',
    theme: 'gitlab',
    description: 'My Description',
    icon: ['my-icon'],
  }, {
    to: '/yet_another',
    caption: 'Third Link',
    theme: 'gitlab',
    description: 'My Description',
    icon: ['my-icon'],
  }]
  it('renders the links', () => {
    const wrapper = shallowMount(BigLinks, {
      propsData: {
        links,
      },
      stubs: {
        BigLink: true,
      },
    })
    for (let i = 0; i < links.length; i++) {
      const link = links[i]
      const el = wrapper.findAllComponents(BigLink).at(i)
      expect(el.exists()).toBeTruthy()
      expect(el.props()).toEqual({
        ...link,
        external: link.external || false,
        download: false,
      })
    }
  })
})
