import { shallowMount } from '@vue/test-utils'
import HeadSection from '@/components/modules/HeadSection.vue'

describe('HeadSection', () => {
  it('renders a header', () => {
    const wrapper = shallowMount(HeadSection, {
      mocks: {
        $t: msg => msg,
        $i18n: { locale: 'en' },
        switchLocalePath: () => {},
        localePath: () => {},
      },
      stubs: {
        'font-awesome-icon': true,
      },
      propsData: {
        heading: 'Digitales Wartezimmer',
      },
    })
    const html = wrapper.html()
    expect(html).toContain('<header')
  })
  it('renders a heading', () => {
    const wrapper = shallowMount(HeadSection, {
      mocks: {
        $t: msg => msg,
        $i18n: { locale: 'en' },
        switchLocalePath: () => {},
        localePath: () => {},
      },
      stubs: {
        'font-awesome-icon': true,
      },
      propsData: {
        heading: 'Digitales Wartezimmer',
      },
    })
    const html = wrapper.html()
    expect(html).toContain('<h1')
    expect(html).toContain('head.title')
  })
  it('renders a lead paragraph', () => {
    const wrapper = shallowMount(HeadSection, {
      mocks: {
        $t: msg => msg,
        $i18n: { locale: 'en' },
        switchLocalePath: () => {},
        localePath: () => {},
      },
      stubs: {
        'font-awesome-icon': true,
      },
      propsData: {
        heading: 'Digitales Wartezimmer',
        lead: 'Lorem ipsum dolor sit amet',
      },
    })
    const html = wrapper.html()
    expect(html).toContain('Lorem ipsum dolor sit amet')
  })
})
