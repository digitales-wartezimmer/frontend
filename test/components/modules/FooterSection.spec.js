import { shallowMount } from '@vue/test-utils'
import FooterSection from '@/components/modules/FooterSection.vue'

describe('FooterSection', () => {
  const switchLocalePath = jest.fn().mockImplementation(alias => `/locale/${alias}`)
  const localePath = jest.fn().mockImplementation(path => path)
  function mountCmp (props = {}, mocks = {}) {
    return shallowMount(FooterSection, {
      propsData: {
        items: [
          { title: 'Link 1', to: '/1' },
          { title: 'Link 2', to: '/2' },
          { title: 'Link 3', to: '/3' },
        ],
        ...props,
      },
      stubs: {
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
        'client-only': true,
      },
      mocks: {
        $t: msg => msg,
        $i18n: {
          locale: 'de',
        },
        switchLocalePath,
        localePath,
        ...mocks,
      },
    })
  }
  it('renders', () => {
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('renders the item links', () => {
    const cmp = mountCmp()
    const links = cmp.findAll('.m-footer-section__item')
    expect(links.length).toEqual(3 + 1)
    for (let i = 0; i <= 2; i++) {
      const link = links.at(i)
      expect(link.text()).toContain(`Link ${i + 1}`)
      expect(link.find('a').attributes('href')).toEqual(`/${i + 1}`)
    }
  })
  it('renders a language button', () => {
    const cmp = mountCmp()
    const link = cmp.find('.m-footer-section__item--primary')
    expect(link.exists())
    expect(link.text()).toEqual('Read in English')
    expect(link.find('a').attributes('href')).toEqual('/locale/en')
  })
  it('renders a different language button, based on language', () => {
    const cmp = mountCmp({}, {
      $i18n: { locale: 'en' },
    })
    const link = cmp.find('.m-footer-section__item--primary')
    expect(link.exists())
    expect(link.text()).toEqual('Auf Deutsch lesen')
    expect(link.find('a').attributes('href')).toEqual('/locale/de')
  })
})
