import { shallowMount } from '@vue/test-utils'
import LandingPageContent from '@/components/modules/LandingPageContent.vue'

describe('LandingPageContent', () => {
  function mountCmp (props = {}, mocks = {}) {
    return shallowMount(LandingPageContent, {
      propsData: {
        formUrl: '/my/form/url',
        title: 'My Title',
        description: 'My long description',
        ...props,
      },
      stubs: {
        'nuxt-link': true,
        'client-only': true,
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
        localePath: url => url,
        ...mocks,
      },
    })
  }
  it('renders', () => {
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('contains title and description', () => {
    const cmp = mountCmp()
    expect(cmp.text()).toContain('My Title')
    expect(cmp.text()).toContain('My long description')
  })
  it('renders a button to start the form', () => {
    const cmp = mountCmp()
    const btn = cmp.find('button-stub')
    expect(btn.exists()).toBeTruthy()
    expect(btn.props('to')).toEqual('/my/form/url')
  })
})
