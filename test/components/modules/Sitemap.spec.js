import { shallowMount } from '@vue/test-utils'
import Sitemap from '@/components/modules/Sitemap.vue'

describe('Sitemap', () => {
  it('renders', () => {
    const wrapper = shallowMount(Sitemap, {
      mocks: {
        $t: msg => msg,
        $i18n: { locale: 'en' },
        switchLocalePath: () => {},
        localePath: (arg) => {
          if (typeof arg === 'string') {
            return arg
          }
          return JSON.stringify(arg)
        },
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': true,
      },
    })
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
  })
})
