import { createLocalVue, mount } from '@vue/test-utils'
import { ValidationObserver, ValidationProvider } from 'vee-validate'
import Contacts from '@/components/modules/form/Contacts.vue'

describe('Contacts', () => {
  let store
  let getter
  const emptyContact = { firstName: '', lastName: '', phone: '', email: '', address: { street: '', houseNumber: '', zip: '', city: '' } }
  const localVue = createLocalVue()
  localVue.component('ValidationObserver', ValidationObserver)
  localVue.component('ValidationProvider', ValidationProvider)
  beforeEach(() => {
    getter = jest.fn().mockReturnValue([])
    store = {
      getters: {
        'myFormStore/getField': getter,
      },
      commit: jest.fn(),
    }
  })
  function mountCmp (props = {}) {
    return mount(Contacts, {
      propsData: {
        storeKey: 'myFormStore',
        formId: 'my-form',
        field: {
          key: 'fieldKey',
        },
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
        FieldInput: true,
        Button: true,
      },
      mocks: {
        $t: msg => msg,
        localePath: str => str,
        $store: store,
      },
      localVue,
    })
  }
  it('renders', () => {
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('renders a button that can add contact', async () => {
    const cmp = mountCmp()
    const btn = cmp.find('button-stub')
    expect(btn.exists()).toBeTruthy()
    await btn.trigger('click')
    expect(cmp.html()).toMatchSnapshot()
    let expected = [emptyContact, emptyContact]
    expect(cmp.emitted('change', [expected])).toBeTruthy()
    expect(store.commit).toHaveBeenCalledWith('myFormStore/updateField', { path: 'data.fieldKey', value: expected })
    jest.clearAllMocks()
    await btn.trigger('click')
    expect(cmp.html()).toMatchSnapshot()
    expected = [emptyContact, emptyContact, emptyContact]
    expect(cmp.emitted('change', [expected])).toBeTruthy()
    expect(store.commit).toHaveBeenCalledWith('myFormStore/updateField', { path: 'data.fieldKey', value: expected })
  })
  it('can mount with preexisting contacts', () => {
    getter.mockReturnValue([{ firstName: 'Test', lastName: 'Contact', phone: '', email: 'test@test.de', address: { street: 'Test', houseNumber: '12', zip: '1234', city: 'Testy' } }])
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('sends change event on update', () => {
    const cmp = mountCmp()
    const expected = [{ firstName: 'New', lastName: 'TEst', phone: '', email: 'test@test.de', address: { street: 'Test', houseNumber: '12', zip: '1234', city: 'Testy' } }]
    cmp.vm.contacts = expected
    cmp.vm.updateContacts()
    expect(cmp.emitted('change', [expected])).toBeTruthy()
    expect(store.commit).toHaveBeenCalledWith('myFormStore/updateField', { path: 'data.fieldKey', value: expected })
  })
  it('can remove a contact', async () => {
    getter.mockReturnValue([
      { firstName: 'First', lastName: 'Contact', phone: '', email: 'test@test.de', address: { street: 'Test', houseNumber: '12', zip: '1234', city: 'Testy' } },
      { firstName: 'Second', lastName: 'Contact', phone: '', email: 'test@test.de', address: { street: 'Test', houseNumber: '12', zip: '1234', city: 'Testy' } },
      { firstName: 'Third', lastName: 'Contact', phone: '', email: 'test@test.de', address: { street: 'Test', houseNumber: '12', zip: '1234', city: 'Testy' } },
    ])
    const cmp = mountCmp()
    const btns = cmp.findAll('.m-contacts__contact__remove')
    expect(btns.length).toEqual(3)
    await btns.at(1).trigger('click')
    const expected = [
      { firstName: 'First', lastName: 'Contact', phone: '', email: 'test@test.de', address: { street: 'Test', houseNumber: '12', zip: '1234', city: 'Testy' } },
      { firstName: 'Third', lastName: 'Contact', phone: '', email: 'test@test.de', address: { street: 'Test', houseNumber: '12', zip: '1234', city: 'Testy' } },
    ]
    expect(cmp.emitted('change', [expected])).toBeTruthy()
    expect(store.commit).toHaveBeenCalledWith('myFormStore/updateField', { path: 'data.fieldKey', value: expected })
    expect(cmp.html()).toMatchSnapshot()
  })
})
