import { shallowMount } from '@vue/test-utils'
import Success from '@/components/modules/form/Success.vue'

let responseData
describe('Success', () => {
  beforeEach(() => {
    responseData = {
      data: {},
    }
  })
  function mountCmp (props = {}) {
    return shallowMount(Success, {
      propsData: {
        responseData,
        formId: 'my-form',
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
        localePath: str => str,
      },
    })
  }
  it('renders', () => {
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('renders a start over button', () => {
    const cmp = mountCmp()
    const btn = cmp.findAll('enhancedbutton-stub').at(0)
    expect(btn.props('href')).toEqual('/forms/my-form?startOver=true')
  })
})
