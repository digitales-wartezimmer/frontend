import { shallowMount } from '@vue/test-utils'
import FormReviewer from '@/components/modules/form/FormReviewer.vue'

let field
describe('FormReviewer', () => {
  function mountCmp (props = {}) {
    return shallowMount(FormReviewer, {
      propsData: {
        fields: [field, field, field],
        formId: 'myForm',
        storeKey: 'myFormStore',
        active: true,
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
    })
  }

  let cmp

  beforeEach(() => {
    field = {
      key: 'myField',
      type: 'text',
      alias: 'my-field',
      hidden: undefined,
      rules: 'required',
      value: 'Test Value',
      choices: [{
        alias: 'yes',
        value: 'Y',
      }, {
        alias: 'maybe',
        value: 'M',
      }, {
        alias: 'no',
        value: 'N',
      }],
    }
    cmp = mountCmp()
  })

  it('renders', () => {
    expect(cmp.html()).toMatchSnapshot()
    expect(cmp.findAll('.m-form-reviewer__field').length).toEqual(3)
  })
  describe('text field', () => {
    it('renders a text field', async () => {
      field.type = 'text'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('Test Value')
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders a textarea field', async () => {
      field.type = 'textarea'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('Test Value')
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders a component field', async () => {
      field.type = 'component'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('Test Value')
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders an empty value', async () => {
      field.type = 'text'
      field.value = undefined
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).not.toContain('Test Value')
      expect(cmp.text()).toContain('-')
    })
  })
  describe('date field', () => {
    it('renders a date field', async () => {
      field.type = 'date'
      field.value = '20.01.1990'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('20.01.1990')
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders a non german date field', async () => {
      field.type = 'date'
      field.value = '1990-01-20'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('20.01.1990')
    })
    it('renders an empty date field', async () => {
      field.type = 'date'
      field.value = ''
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('-')
    })
    it('renders an invalid date field', async () => {
      field.type = 'date'
      field.value = 'invalid'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('-')
    })
  })
  describe('radio or switch field', () => {
    it('renders a radio field', async () => {
      field.type = 'radio'
      field.value = 'Y'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('pages.myForm.fields.my-field.options.yes.caption')
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders an unknown radio field', async () => {
      field.type = 'radio'
      field.value = 'O'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('?')
    })
    it('renders a switch field', async () => {
      field.type = 'switch'
      field.value = 'Y'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('pages.myForm.fields.my-field.options.yes.caption')
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders an unknown switch field', async () => {
      field.type = 'switch'
      field.value = 'O'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('?')
    })
  })
  describe('checkbox', () => {
    it('renders a checkbox with a single value', async () => {
      field.type = 'checkbox'
      field.value = 'Y'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('pages.myForm.fields.my-field.options.yes.caption')
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders a checkbox with a single unknown value', async () => {
      field.type = 'checkbox'
      field.value = 'O'
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('?')
    })
    it('renders a checkbox with multiple values', async () => {
      field.type = 'checkbox'
      field.value = ['Y', 'N']
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('pages.myForm.fields.my-field.options.yes.caption')
      expect(cmp.text()).toContain('pages.myForm.fields.my-field.options.no.caption')
    })
    it('renders a checkbox with multiple values where some are missing', async () => {
      field.type = 'checkbox'
      field.value = ['Y', 'N', '', 'O']
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('pages.myForm.fields.my-field.options.yes.caption')
      expect(cmp.text()).toContain('pages.myForm.fields.my-field.options.no.caption')
      expect(cmp.text()).toContain('?')
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders a checkbox with no values', async () => {
      field.type = 'checkbox'
      field.value = []
      await cmp.setProps({ fields: [field] })
      expect(cmp.text()).toContain('ui.form-reviewer.empty')
      expect(cmp.html()).toMatchSnapshot()
    })
  })
})
