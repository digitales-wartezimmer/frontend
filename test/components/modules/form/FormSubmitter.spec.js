import { shallowMount } from '@vue/test-utils'
import axios from 'axios'
import FormSubmitter from '@/components/modules/form/FormSubmitter.vue'

jest.mock('axios')
jest.useFakeTimers()

describe('FormSubmitter', () => {
  function mountCmp (props = {}) {
    return shallowMount(FormSubmitter, {
      propsData: {
        data: { foo: 'bar' },
        url: '/endpoints/test',
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
        $axios: axios,
        $store: {
          state: {
            idempotencyToken: 'abc123def456',
          },
        },
      },
    })
  }

  let cmp

  beforeEach(() => {
    axios.post.mockReturnValue(new Promise((resolve) => {
      resolve({
        status: 201,
        data: { foo: 'bar' },
      })
    }))
    cmp = mountCmp()
  })

  it('renders', () => {
    expect(cmp.html()).toMatchSnapshot()
  })
  it('sends the data', () => {
    jest.runAllTimers()
    expect(axios.post).toHaveBeenCalledWith('/endpoints/test', {
      foo: 'bar',
      idempotencyToken: 'abc123def456',
      timestamp: expect.any(String),
    })
  })

  describe('loading', () => {
    it('renders a loading state', () => {
      expect(cmp.text()).toContain('ui.form-submitter.heading.loading')
      expect(cmp.text()).toContain('ui.form-submitter.description.loading')
      expect(cmp.find('.m-form-submitter__error-message').exists()).toBeFalsy()
    })
  })

  describe('success', () => {
    beforeEach(() => {
      jest.runAllTimers()
    })
    it('renders a success message', () => {
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('ui.form-submitter.heading.success')
      expect(cmp.text()).toContain('ui.form-submitter.description.success')
      expect(cmp.find('.m-form-submitter__error-message').exists()).toBeFalsy()
    })
    it('emits a success event', async () => {
      jest.runAllTimers()
      await cmp.vm.$nextTick()
      expect(cmp.emitted('success')).toBeTruthy()
      expect(cmp.emitted('success')[0][0]).toEqual({ foo: 'bar' })
    })
  })
  describe('error', () => {
    it('the response has an invalid status code', async () => {
      axios.post.mockReturnValue(new Promise((resolve) => {
        resolve({
          status: 404,
          statusText: 'Not Found',
          data: { foo: 'bar' },
        })
      }))
      jest.runAllTimers()
      await cmp.vm.$nextTick()
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('ui.form-submitter.heading.error')
      expect(cmp.text()).toContain('ui.form-submitter.description.error')
      expect(cmp.text()).toContain('Not Found')
      await cmp.vm.$nextTick()
      expect(cmp.emitted('success')).not.toBeDefined()
    })
    it('an unknown error appears', async () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      axios.post.mockReturnValue(new Promise((resolve, reject) => {
        const err = new Error('Some Error')
        reject(err)
      }))
      jest.runAllTimers()
      await cmp.vm.$nextTick()
      await cmp.vm.$nextTick()
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('ui.form-submitter.heading.error')
      expect(cmp.text()).toContain('ui.form-submitter.description.error')
      expect(cmp.text()).toContain('Some Error')
      await cmp.vm.$nextTick()
      expect(cmp.emitted('success')).not.toBeDefined()
    })
    it('an unknown error with message appears', async () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      axios.post.mockReturnValue(new Promise((resolve, reject) => {
        const err = new Error('Some Error')
        err.response = { status: 500, data: { message: 'Something happened' } }
        reject(err)
      }))
      jest.runAllTimers()
      await cmp.vm.$nextTick()
      await cmp.vm.$nextTick()
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('ui.form-submitter.heading.error')
      expect(cmp.text()).toContain('ui.form-submitter.description.error')
      expect(cmp.text()).toContain('Something happened')
      await cmp.vm.$nextTick()
      expect(cmp.emitted('success')).not.toBeDefined()
    })
    it('renders a cancel button', async () => {
      axios.post.mockReturnValue(new Promise((resolve) => {
        resolve({
          status: 404,
          statusText: 'Not Found',
          data: { foo: 'bar' },
        })
      }))
      jest.runAllTimers()
      await cmp.vm.$nextTick()
      expect(cmp.html()).toMatchSnapshot()
      const btn = cmp.find('button-stub')
      expect(btn.exists()).toBeTruthy()
      expect(btn.text()).toContain('ui.form-submitter.back_button')
      btn.vm.$emit('click')
      await cmp.vm.$nextTick()
      expect(cmp.emitted('cancel')).toBeTruthy()
    })
  })
})
