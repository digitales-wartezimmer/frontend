import { createLocalVue, mount } from '@vue/test-utils'
import { ValidationProvider, extend } from 'vee-validate'
import { required } from 'vee-validate/dist/rules.umd.js'
import VueDatePicker from '@mathieustan/vue-datepicker'
import FormContent from '@/components/modules/form/FormContent.vue'
import FieldInput from '@/components/elements/fields/FieldInput'

let field
describe('FormContent', () => {
  const localVue = createLocalVue()
  extend('required', required)
  localVue.component('ValidationProvider', ValidationProvider)
  localVue.component('VueDatePicker', VueDatePicker)

  let store
  beforeEach(() => {
    field = {
      key: 'myField',
      type: 'text',
      alias: 'my-field',
      hidden: undefined,
      rules: 'required',
      choices: [{
        alias: 'yes',
        value: 'Y',
      }, {
        alias: 'maybe',
        value: 'M',
      }, {
        alias: 'no',
        value: 'N',
      }],
    }
    store = {
      getters: {
        'my-form-store/getField': jest.fn().mockReturnValue('Test Value'),
      },
      commit: jest.fn(),
      state: {
        'my-form-store': {
          customState: 'content',
        },
      },
    }
  })
  function mountCmp (props = {}) {
    return mount(FormContent, {
      propsData: {
        multi: false,
        formId: 'my-form',
        storeKey: 'my-form-store',
        disabled: false,
        field,
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
        'validation-provider': true,
        VueDatePicker: true,
      },
      mocks: {
        $t: msg => msg,
        $store: store,
        $i18n: {
          locale: 'de',
        },
      },
      localVue,
    })
  }
  it('renders', () => {
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })

  describe('Type text', () => {
    let cmp
    beforeEach(() => {
      field.type = 'text'
      cmp = mountCmp()
    })
    it('renders', () => {
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders the input field', () => {
      const input = cmp.find('input')
      expect(input.exists()).toBeTruthy()
      expect(input.attributes('type')).toEqual('text')
      expect(input.element.value).toEqual('Test Value')
    })
    it('can change the value', () => {
      const input = cmp.find('input')
      input.setValue('New Val')
      cmp.vm.value = 'New Val'
      expect(store.commit).toHaveBeenCalledWith('my-form-store/updateField', { path: 'data.myField', value: 'New Val' })
    })
  })
  describe('Type date', () => {
    let cmp
    beforeEach(() => {
      field.type = 'date'
      store.getters['my-form-store/getField'].mockReturnValue('01.01.1990')
      cmp = mountCmp()
    })
    it('renders the datepicker field', () => {
      const input = cmp.find('vuedatepicker-stub')
      expect(input.exists()).toBeTruthy()
      expect(input.attributes('value')).toContain('Mon Jan 01 1990 00:00:00')
    })
    it('can change the value', () => {
      cmp.vm.value = '1990-02-02'
      expect(store.commit).toHaveBeenCalledWith('my-form-store/updateField', { path: 'data.myField', value: '1990-02-02' })
    })
  })
  describe('Type radio', () => {
    let cmp
    beforeEach(() => {
      field.type = 'radio'
      store.getters['my-form-store/getField'].mockReturnValue('Y')
      cmp = mountCmp()
    })
    it('renders', () => {
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders the options field', () => {
      const inputs = cmp.findAll('input')
      let input = inputs.at(0)
      expect(input.exists()).toBeTruthy()
      expect(input.attributes('type')).toEqual('radio')
      expect(input.element.value).toEqual('Y')
      input = inputs.at(1)
      expect(input.exists()).toBeTruthy()
      expect(input.attributes('type')).toEqual('radio')
      expect(input.element.value).toEqual('M')
      input = inputs.at(2)
      expect(input.exists()).toBeTruthy()
      expect(input.attributes('type')).toEqual('radio')
      expect(input.element.value).toEqual('N')
    })
    it('can change the value', () => {
      const input = cmp.find('input')
      input.setChecked()
      cmp.vm.value = 'Y'
      expect(store.commit).toHaveBeenCalledWith('my-form-store/updateField', { path: 'data.myField', value: 'Y' })
    })
  })
  describe('Type switch', () => {
    let cmp
    beforeEach(() => {
      field.type = 'switch'
      store.getters['my-form-store/getField'].mockReturnValue('Y')
      cmp = mountCmp()
    })
    it('renders', () => {
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders the options field', () => {
      const inputs = cmp.findAll('input')
      let input = inputs.at(0)
      expect(input.exists()).toBeTruthy()
      expect(input.attributes('type')).toEqual('radio')
      expect(input.element.value).toEqual('Y')
      input = inputs.at(1)
      expect(input.exists()).toBeTruthy()
      expect(input.attributes('type')).toEqual('radio')
      expect(input.element.value).toEqual('M')
      input = inputs.at(2)
      expect(input.exists()).toBeTruthy()
      expect(input.attributes('type')).toEqual('radio')
      expect(input.element.value).toEqual('N')
    })
    it('can change the value', () => {
      const input = cmp.find('input')
      input.setChecked()
      cmp.vm.value = 'Y'
      expect(store.commit).toHaveBeenCalledWith('my-form-store/updateField', { path: 'data.myField', value: 'Y' })
    })
  })
  describe('Type checkbox', () => {
    let cmp
    beforeEach(() => {
      field.type = 'checkbox'
      store.getters['my-form-store/getField'].mockReturnValue(['Y'])
      cmp = mountCmp()
    })
    it('renders', () => {
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders the options field', () => {
      const inputs = cmp.findAll('input')
      let input = inputs.at(0)
      expect(input.exists()).toBeTruthy()
      expect(input.attributes('type')).toEqual('checkbox')
      expect(input.element.value).toEqual('Y')
      input = inputs.at(1)
      expect(input.exists()).toBeTruthy()
      expect(input.attributes('type')).toEqual('checkbox')
      expect(input.element.value).toEqual('M')
      input = inputs.at(2)
      expect(input.exists()).toBeTruthy()
      expect(input.attributes('type')).toEqual('checkbox')
      expect(input.element.value).toEqual('N')
    })
    it('can change the value', () => {
      const input = cmp.find('input')
      input.setChecked()
      cmp.vm.value = ['Y']
      expect(store.commit).toHaveBeenCalledWith('my-form-store/updateField', { path: 'data.myField', value: ['Y'] })
    })
  })
  describe('Type title', () => {
    let cmp
    beforeEach(() => {
      field.type = 'title'
      field.content = 'This is my content'
      cmp = mountCmp()
    })
    it('renders', () => {
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('This is my content')
    })
  })
  describe('Type paragraph', () => {
    let cmp
    beforeEach(() => {
      field.type = 'paragraph'
      field.content = 'This is my content'
      cmp = mountCmp()
    })
    it('renders', () => {
      expect(cmp.html()).toMatchSnapshot()
      expect(cmp.text()).toContain('This is my content')
    })
  })
  describe('Type textarea', () => {
    let cmp
    beforeEach(() => {
      field.type = 'textarea'
      cmp = mountCmp()
    })
    it('renders', () => {
      expect(cmp.html()).toMatchSnapshot()
    })
    it('renders the input field', () => {
      const input = cmp.find('textarea')
      expect(input.exists()).toBeTruthy()
      expect(input.element.value).toEqual('Test Value')
    })
    it('can change the value', () => {
      const input = cmp.find('textarea')
      input.setValue('New Val')
      cmp.vm.value = 'New Val'
      expect(store.commit).toHaveBeenCalledWith('my-form-store/updateField', { path: 'data.myField', value: 'New Val' })
    })
  })
  it('has a fallback for unknown types', () => {
    field.type = 'other'
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('can display multi elements', () => {
    const cmp = mountCmp({
      multi: true,
    })
    const el = cmp.find('.m-form-content')
    expect(el.exists()).toBeTruthy()
    expect(el.classes('m-form-content--multi')).toBeTruthy()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('can disable an element', () => {
    const cmp = mountCmp({
      disabled: true,
    })
    expect(cmp.findComponent(FieldInput).props('disabled')).toBeTruthy()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('can provide a fallback value', () => {
    store.getters['my-form-store/getField'].mockReturnValue(undefined)
    const cmp = mountCmp()
    const input = cmp.find('input')
    expect(input.exists()).toBeTruthy()
    expect(input.attributes('type')).toEqual('text')
    expect(input.element.value).toEqual('')
  })
  it('can hide the element', () => {
    let cmp = mountCmp({
      field: {
        ...field,
        hidden: true,
      },
    })
    expect(cmp.html()).toMatchSnapshot()
    const hidden = jest.fn().mockReturnValue(false)
    cmp = mountCmp({
      field: {
        ...field,
        hidden,
      },
    })
    expect(hidden).toHaveBeenCalledWith({
      customState: 'content',
    })
    expect(cmp.html()).toMatchSnapshot()
  })
  it('can set the rules', () => {
    let cmp = mountCmp({
      field: {
        ...field,
        rules: undefined,
      },
    })
    expect(cmp.html()).toMatchSnapshot()
    expect(cmp.findComponent(FieldInput).props('rules')).toEqual('')
    const rules = jest.fn().mockReturnValue('required')
    cmp = mountCmp({
      field: {
        ...field,
        rules,
      },
    })
    expect(rules).toHaveBeenCalledWith({
      customState: 'content',
    })
    expect(cmp.findComponent(FieldInput).props('rules')).toEqual('required')
    expect(cmp.html()).toMatchSnapshot()
    cmp = mountCmp({
      field: {
        ...field,
        rules: 'required',
      },
    })
    expect(cmp.findComponent(FieldInput).props('rules')).toEqual('required')
    expect(cmp.html()).toMatchSnapshot()
  })
})
