import { shallowMount } from '@vue/test-utils'
import copyToClipboard from 'copy-to-clipboard'
import * as FS from 'file-saver'
import Papa from 'papaparse'
import EmailDisplay from '@/components/modules/form/EmailDisplay.vue'

jest.mock('copy-to-clipboard')
jest.mock('file-saver')

let responseData
describe('EmailDisplay', () => {
  beforeEach(() => {
    responseData = {
      data: {
        healthOfficeEmail: 'recipient@mail.de',
        subject: 'This is my Subject',
        body: 'This is my body. It contains a      lot of   whitespace. And \r\n some breaks \n \r \n\r that hopefully get exluded',
      },
    }
  })
  function mountCmp (props = {}) {
    return shallowMount(EmailDisplay, {
      propsData: {
        responseData,
        formId: 'my-form',
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
        localePath: str => str,
      },
    })
  }
  it('renders', () => {
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('copies the elements to clipboard', () => {
    const cmp = mountCmp()
    let link = cmp.findAll('.result-display__copy').at(0)
    expect(link.exists()).toBeTruthy()
    link.trigger('click')
    expect(copyToClipboard).toHaveBeenCalledWith('[TEST] ' + responseData.data.subject)
    link = cmp.findAll('.result-display__copy').at(1)
    expect(link.exists()).toBeTruthy()
    link.trigger('click')
    expect(copyToClipboard).toHaveBeenCalledWith(responseData.data.healthOfficeEmail)
    link = cmp.findAll('.result-display__copy').at(2)
    expect(link.exists()).toBeTruthy()
    link.trigger('click')
    expect(copyToClipboard).toHaveBeenCalledWith(expect.any(String))
  })
  it('can open the email in the mail client', () => {
    const cmp = mountCmp()
    const btn = cmp.findAll('button-stub').at(0)
    expect(btn.props('to')).toMatchSnapshot()
  })
  it('handles no response body', async () => {
    const cmp = mountCmp({ responseData: { data: null } })
    expect(cmp.html()).toMatchSnapshot()

    await cmp.setProps({ responseData: { data: {} } })
    expect(cmp.html()).toMatchSnapshot()
  })
  it('renders a start over button', () => {
    const cmp = mountCmp()
    const btn = cmp.findAll('button-stub').at(1)
    expect(btn.props('to')).toEqual('/forms/my-form?startOver=true')
  })
  it('does not render an attachment button if no csv attachment is present', () => {
    const cmp = mountCmp()
    expect(cmp.find('.result-display__download').exists()).toBeFalsy()
  })
  describe('it can provide a csv attachment', () => {
    beforeEach(() => {
      responseData = {
        data: {
          ...responseData,
          csvAttachment: {
            fileName: 'myAttachment.csv',
            contentData: [{ col1: 'a', col2: 'b' }, { col1: 'c', col2: 'd' }],
          },
        },
      }

      jest.spyOn(FS, 'saveAs')
      jest.clearAllMocks()
    })
    it('renders a button that downloads the csv', () => {
      const cmp = mountCmp()
      const downloadButton = cmp.find('.result-display__download')
      expect(downloadButton.exists()).toBeTruthy()
      expect(downloadButton.text()).toContain('myAttachment.csv')
      downloadButton.trigger('click')
      expect(FS.saveAs).toHaveBeenCalledWith(new Blob([], { type: 'text/csv;charset=utf-8' }), 'myAttachment.csv')
    })
    it('does not download if no data is present', () => {
      responseData = {
        data: {
          ...responseData,
          csvAttachment: undefined,
        },
      }
      const cmp = mountCmp()
      const downloadButton = cmp.find('.result-display__download')
      expect(downloadButton.exists()).toBeFalsy()
      cmp.vm.download()
      expect(FS.saveAs).not.toHaveBeenCalled()
    })
    it('does not download if data parsing failed', () => {
      jest.spyOn(Papa, 'unparse').mockReturnValue(undefined)
      const cmp = mountCmp()
      const downloadButton = cmp.find('.result-display__download')
      expect(downloadButton.exists()).toBeTruthy()
      downloadButton.trigger('click')
      expect(FS.saveAs).not.toHaveBeenCalled()
    })
  })
})
