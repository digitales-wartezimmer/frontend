import { shallowMount } from '@vue/test-utils'
import FormProgressBar from '@/components/modules/form/FormProgressBar.vue'
import { PROGRESS_STEP_IDS } from '@/content/forms'

const progress = {
  1: {
    key: PROGRESS_STEP_IDS.TEST,
    titleVisible: true,
    title: 'Test 1',
  },
  2: {
    key: PROGRESS_STEP_IDS.TRAVEL,
    titleVisible: false,
    title: 'Test 2',
  },
  3: {
    key: PROGRESS_STEP_IDS.REVIEW,
    titleVisible: true,
    title: 'Test 3',
  },
  4: {
    key: PROGRESS_STEP_IDS.SUBMIT,
    titleVisible: true,
    title: 'Test 4',
  },
}
describe('FormProgressBar', () => {
  function mountCmp (props = {}) {
    return shallowMount(FormProgressBar, {
      propsData: {
        progress,
        currentStepKey: PROGRESS_STEP_IDS.TEST,
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
    })
  }
  it('renders', () => {
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
    expect(cmp.findAll('.e-form-progress-bar__item').length).toEqual(4)
  })
  it('handles invalid current step', () => {
    const cmp = mountCmp({
      currentStepKey: undefined,
    })
    expect(cmp.html()).toMatchSnapshot()
  })
  it('renders the titles', () => {
    const cmp = mountCmp()
    const items = cmp.findAll('.e-form-progress-bar__item')
    expect(items.at(0).text()).toContain('Test 1')
    expect(items.at(2).text()).toContain('Test 3')
    expect(items.at(3).text()).toContain('Test 4')
  })
  it('hides the title if not configured', () => {
    const cmp = mountCmp()
    const item = cmp.findAll('.e-form-progress-bar__item').at(1)
    expect(item.text()).not.toContain('Test 2')
    expect(item.classes()).toContain('e-form-progress-bar__item--no-text')
  })
  it('marks the current step', async () => {
    const cmp = mountCmp({ currentStepKey: PROGRESS_STEP_IDS.TRAVEL })
    expect(cmp.html()).toMatchSnapshot()
    expect(cmp.findAll('.e-form-progress-bar__item--active').length).toEqual(2)
    await cmp.setProps({ currentStepKey: PROGRESS_STEP_IDS.REVIEW })
    expect(cmp.html()).toMatchSnapshot()
    expect(cmp.findAll('.e-form-progress-bar__item--active').length).toEqual(3)
  })
})
