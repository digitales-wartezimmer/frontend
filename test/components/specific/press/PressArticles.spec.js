import { shallowMount } from '@vue/test-utils'
import PressArticles from '@/components/specific/press/PressArticles'

describe('PressArticles', () => {
  function mountCmp (props = {}) {
    return shallowMount(PressArticles, {
      propsData: {
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
    })
  }

  it('renders', () => {
    const cmp = mountCmp()
    expect(cmp.html()).toMatchSnapshot()
  })
  it('can display all buttons', async () => {
    const cmp = mountCmp()
    expect(cmp.findAll('articlecard-stub').length).toEqual(6)
    let btn = cmp.find('.press-articles__load-button')
    expect(btn.exists()).toBeTruthy()

    await btn.vm.$emit('click')
    await cmp.vm.$nextTick

    expect(cmp.html()).toMatchSnapshot()
    expect(cmp.findAll('articlecard-stub').length).toBeGreaterThan(6)
    btn = cmp.find('.press-articles__load-button')
    expect(btn.exists()).toBeFalsy()
  })
})
