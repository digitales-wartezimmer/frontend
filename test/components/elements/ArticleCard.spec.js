import { mount } from '@vue/test-utils'
import ArticleCard from '@/components/elements/ArticleCard.vue'

describe('ArticleCard', () => {
  function mountCmp () {
    return mount(ArticleCard, {
      propsData: {
        title: 'Title',
        text: 'Test Text',
        url: 'https://digitales-wartezimmer.org',
        logoUrl: '/test-logo.png',
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
      mocks: {
        $t: msg => msg,
      },
    })
  }
  it('renders', () => {
    const wrapper = mountCmp()
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    expect(html).toContain('e-article-card')
    expect(html).toContain('<article')
    expect(html).toContain('Title')
    expect(html).toContain('Test Text')
  })
  it('renders the image', () => {
    const wrapper = mountCmp()
    const img = wrapper.find('img')
    expect(img.exists()).toBeTruthy()
    expect(img.attributes('src')).toBeDefined()
  })
  it('renders the link', () => {
    const wrapper = mountCmp()
    const link = wrapper.find('a')
    expect(link.exists()).toBeTruthy()
    expect(link.text()).toEqual('pages.press.articles-section.read_article')
    expect(link.attributes('href')).toEqual('https://digitales-wartezimmer.org')
  })
  it('shortens a long description text', async () => {
    const wrapper = mountCmp()
    expect(wrapper.find('.e-article-card__description').text()).toEqual('Test Text')
    await wrapper.setProps({
      text: 'This is a very long text, which is longer than five plus ten words. Trust me and count them.',
    })
    expect(wrapper.find('.e-article-card__description').text()).toEqual('This is a very long text, which is longer than five plus ten words. Trust...')
  })
})
