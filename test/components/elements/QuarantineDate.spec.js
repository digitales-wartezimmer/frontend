import { mount } from '@vue/test-utils'
import dayjs from 'dayjs'
import QuarantineDate from '@/components/elements/QuarantineDate.vue'

describe('QuarantineDate', () => {
  function mountCmp (props = {}) {
    return mount(QuarantineDate, {
      propsData: {
        date: dayjs('2020-07-13'),
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
    })
  }
  it('renders', () => {
    const wrapper = mountCmp()
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    expect(wrapper.text()).toContain('ui.quarantine-date.text')
    expect(wrapper.text()).toContain('27.07.2020')
  })
  it('calculates the date', async () => {
    const wrapper = mountCmp()
    expect(wrapper.text()).toContain('27.07.2020')
    await wrapper.setProps({
      date: dayjs('1990-01-01'),
    })
    expect(wrapper.text()).toContain('15.01.1990')
    await wrapper.setProps({
      date: dayjs('1999-12-31'),
      quarantineDuration: 10,
    })
    expect(wrapper.text()).toContain('10.01.2000')
  })
  it('handles invalid date', () => {
    const wrapper = mountCmp({
      date: {},
    })
    expect(wrapper.text()).toContain('ui.quarantine-date.after_n_days')
  })
})
