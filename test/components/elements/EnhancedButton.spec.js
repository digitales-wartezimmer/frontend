import { mount } from '@vue/test-utils'
import EnhancedButton from '@/components/elements/EnhancedButton.vue'

describe('EnhancedButton', () => {
  it('renders a button with a default slot', () => {
    const wrapper = mount(EnhancedButton, {
      slots: {
        default: 'Caption',
      },
    })
    const html = wrapper.html()
    expect(html).toContain('<button')
    expect(html).toContain('Caption')
  })

  it('handles a click event', () => {
    const wrapper = mount(EnhancedButton, {
      slots: {
        default: 'Caption',
      },
    })
    const btn = wrapper.find('button')
    expect(btn.exists()).toBeTruthy()
    btn.trigger('click')
    expect(wrapper.emitted('click')).toBeTruthy()
  })

  it('can render as link', () => {
    const wrapper = mount(EnhancedButton, {
      propsData: {
        tag: 'a',
      },
      slots: {
        default: 'Caption',
      },
    })
    const btn = wrapper.find('a')
    expect(btn.exists()).toBeTruthy()
    btn.trigger('click')
    expect(wrapper.emitted('click')).toBeTruthy()
  })

  it('can have a different theme', () => {
    const wrapper = mount(EnhancedButton, {
      propsData: {
        theme: 'positive',
      },
      slots: {
        default: 'Caption',
      },
    })
    const html = wrapper.html()
    expect(html).toContain('e-enhanced-button--theme-positive')
  })

  it('can have a different type', () => {
    const wrapper = mount(EnhancedButton, {
      propsData: {
        type: 'submit',
      },
      slots: {
        default: 'Caption',
      },
    })
    const html = wrapper.html()
    expect(html).toContain('type="submit"')
  })
})
