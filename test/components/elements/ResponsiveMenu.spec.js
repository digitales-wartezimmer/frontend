import { mount } from '@vue/test-utils'
import ResponsiveMenu from '@/components/elements/ResponsiveMenu.vue'

describe('ResponsiveMenu', () => {
  it('renders a ResponsiveMenu toggle', () => {
    const wrapper = mount(ResponsiveMenu, {
      mocks: {
        $t: msg => msg,
        $config: { TWITTER_URL: '', YOUTUBE_URL: '' },
        $router: {
          resolve: () => ({ route: {} }),
        },
        getRouteBaseName: () => 'test',
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
      propsData: {
        items: [],
      },
    })
    expect(wrapper.find('button.e-responsive-menu__toggle').exists()).toBe(true)
  })
  it('opens a list of menu items, when toggled', async () => {
    const wrapper = mount(ResponsiveMenu, {
      mocks: {
        $t: msg => msg,
        $config: { TWITTER_URL: '', YOUTUBE_URL: '' },
        $router: {
          resolve: () => ({ route: {} }),
        },
        getRouteBaseName: () => 'test',
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
      propsData: {
        items: [
          { to: '/about', title: 'About' },
          { to: '/signup', title: 'Signup' },
          { to: '/signin', title: 'Signin' },
        ],
      },
    })
    wrapper.find('button.e-responsive-menu__toggle').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.findAll('.e-responsive-menu__item').at(1).text()).toBe('Signup')
  })
})
