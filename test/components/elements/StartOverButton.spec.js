import { shallowMount } from '@vue/test-utils'
import StartOverButton from '@/components/elements/StartOverButton.vue'

describe('StartOverButton', () => {
  function mountCmp (props = {}) {
    return shallowMount(StartOverButton, {
      propsData: {
        ...props,
      },
      mocks: {
        $t: msg => msg,
        localePath: arg => arg,
      },
    })
  }
  it('renders', () => {
    const wrapper = mountCmp()
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    const button = wrapper.find('button-stub')
    expect(button.exists()).toBeTruthy()
    expect(button.attributes('to')).toEqual('/')
    expect(wrapper.text()).toContain('ui.back_to_start')
  })
})
