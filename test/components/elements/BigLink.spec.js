import { mount } from '@vue/test-utils'
import BigLink from '@/components/elements/BigLink.vue'

describe('BigLink', () => {
  it('renders', () => {
    const wrapper = mount(BigLink, {
      propsData: {
        to: '/test/route',
        caption: 'My Link',
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
    })
    const html = wrapper.html()
    expect(html).toContain('e-big-link')
    expect(html).toContain('<a')
    expect(html).toContain('My Link')
  })
  it('renders a description', () => {
    const wrapper = mount(BigLink, {
      propsData: {
        to: '/test/route',
        caption: 'My Link',
        description: 'This is my description',
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
    })
    const html = wrapper.html()
    expect(html).toContain('This is my description')
  })
  it('can have a theme', () => {
    const wrapper = mount(BigLink, {
      propsData: {
        to: '/test/route',
        caption: 'My Link',
        theme: 'gitlab',
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
    })
    const html = wrapper.html()
    expect(html).toContain('e-big-link--gitlab')
  })
  it('renders a normal link', () => {
    const wrapper = mount(BigLink, {
      propsData: {
        to: '/test/route',
        caption: 'My Link',
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
    })
    expect(wrapper.attributes('href')).toEqual('/test/route')
    expect(wrapper.attributes('target')).toEqual('_self')
    expect(wrapper.find('font-awesome-icon-stub').attributes('icon')).toEqual('arrow-right')
  })
  it('renders an external link', () => {
    const wrapper = mount(BigLink, {
      propsData: {
        to: 'https://test.de/test/route',
        caption: 'My Link',
        external: true,
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
    })
    expect(wrapper.attributes('href')).toEqual('https://test.de/test/route')
    expect(wrapper.attributes('target')).toEqual('_blank')
    expect(wrapper.find('font-awesome-icon-stub').attributes('icon')).toEqual('external-link-alt')
  })
  it('renders as a download button', () => {
    const wrapper = mount(BigLink, {
      propsData: {
        to: 'https://test.de/test/route',
        caption: 'My Link',
        download: true,
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
    })
    expect(wrapper.attributes('href')).toEqual('https://test.de/test/route')
    expect(wrapper.attributes('target')).toEqual('_blank')
    expect(wrapper.find('font-awesome-icon-stub').attributes('icon')).toEqual('')
  })
})
