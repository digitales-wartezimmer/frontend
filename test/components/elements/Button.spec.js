import { mount } from '@vue/test-utils'
import Button from '@/components/elements/Button.vue'

describe('Button', () => {
  function mountCmp (props = {}) {
    return mount(Button, {
      propsData: {
        ...props,
      },
      slots: {
        default: 'My Link Text',
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
      mocks: {
        $t: msg => msg,
      },
    })
  }
  it('renders', () => {
    const wrapper = mountCmp()
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    expect(wrapper.element.tagName).toEqual('BUTTON')
    expect(wrapper.classes()).toContain('e-button')
    expect(wrapper.classes()).toContain('e-button--primary')
    expect(wrapper.classes()).toContain('e-button--width-normal')
    expect(wrapper.attributes('target')).toEqual('_self')
    expect(wrapper.text()).toEqual('My Link Text')
  })
  it('can be disabled', () => {
    const wrapper = mountCmp({
      disabled: true,
    })
    expect(wrapper.attributes('disabled')).toBeTruthy()
    expect(wrapper.classes()).toContain('e-button--disabled')
  })
  it('can change the width', async () => {
    const wrapper = mountCmp({
      width: 'normal',
    })
    expect(wrapper.classes()).toContain('e-button--width-normal')
    expect(wrapper.classes()).not.toContain('e-button--width-wide')
    await wrapper.setProps({
      width: 'wide',
    })
    expect(wrapper.classes()).not.toContain('e-button--width-normal')
    expect(wrapper.classes()).toContain('e-button--width-wide')
  })
  it('can change the theme', async () => {
    const wrapper = mountCmp({
      theme: 'primary',
    })
    expect(wrapper.classes()).toContain('e-button--primary')
    expect(wrapper.classes()).not.toContain('e-button--secondary')
    expect(wrapper.classes()).not.toContain('e-button--tertiary')
    expect(wrapper.classes()).not.toContain('e-button--ghost')
    await wrapper.setProps({
      theme: 'secondary',
    })
    expect(wrapper.classes()).not.toContain('e-button--primary')
    expect(wrapper.classes()).toContain('e-button--secondary')
    expect(wrapper.classes()).not.toContain('e-button--tertiary')
    expect(wrapper.classes()).not.toContain('e-button--ghost')
    await wrapper.setProps({
      theme: 'tertiary',
    })
    expect(wrapper.classes()).not.toContain('e-button--primary')
    expect(wrapper.classes()).not.toContain('e-button--secondary')
    expect(wrapper.classes()).toContain('e-button--tertiary')
    expect(wrapper.classes()).not.toContain('e-button--ghost')
    await wrapper.setProps({
      theme: 'ghost',
    })
    expect(wrapper.classes()).not.toContain('e-button--primary')
    expect(wrapper.classes()).not.toContain('e-button--secondary')
    expect(wrapper.classes()).not.toContain('e-button--tertiary')
    expect(wrapper.classes()).toContain('e-button--ghost')
  })
  it('can be a link', () => {
    const wrapper = mountCmp({
      to: '/test',
      link: true,
    })
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    expect(wrapper.element.tagName).toEqual('A')
    expect(wrapper.attributes('href')).toEqual('/test')
    expect(wrapper.attributes('target')).toEqual('_self')
  })
  it('can display an icon', () => {
    const wrapper = mountCmp({
      icon: ['arrow-right'],
    })
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    const icon = wrapper.find('font-awesome-icon-stub')
    expect(icon.exists()).toBeTruthy()
    expect(icon.attributes('icon')).toEqual('arrow-right')
  })
  it('can be an external link', () => {
    const wrapper = mountCmp({
      to: 'https://example.com/test',
      link: true,
      external: true,
    })
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    expect(wrapper.element.tagName).toEqual('A')
    expect(wrapper.attributes('href')).toEqual('https://example.com/test')
    expect(wrapper.attributes('target')).toEqual('_blank')
  })
  it('emits click event on click', () => {
    const wrapper = mountCmp()
    expect(wrapper.emitted('click')).toBeFalsy()
    wrapper.trigger('click')
    expect(wrapper.emitted('click')).toBeTruthy()
  })
})
