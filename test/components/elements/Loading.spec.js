import { mount } from '@vue/test-utils'
import Loading from '@/components/elements/Loading.vue'

describe('Loading', () => {
  function mountCmp (props = {}) {
    return mount(Loading, {
      propsData: {
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
    })
  }
  it('renders', () => {
    const wrapper = mountCmp()
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    const icon = wrapper.find('font-awesome-icon-stub')
    expect(icon.exists()).toBeTruthy()
    expect(icon.classes()).toContain('e-loading__icon')
    expect(icon.attributes('icon')).toEqual('spinner')
    expect(icon.attributes('pulse')).toEqual('')
    expect(wrapper.text()).toContain('ui.loading')
  })
})
