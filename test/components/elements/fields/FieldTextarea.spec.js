import Vue from 'vue'
import { createLocalVue, mount } from '@vue/test-utils'
import { ValidationProvider, extend } from 'vee-validate'
import { required, min, max } from 'vee-validate/dist/rules.umd.js'
import FieldTextarea from '@/components/elements/fields/FieldTextarea.vue'
import Field from '@/components/elements/fields/Field.vue'

describe('FieldTextarea', () => {
  const localVue = createLocalVue()
  extend('required', required)
  extend('min', min)
  extend('max', max)
  localVue.component('ValidationProvider', ValidationProvider)

  it('renders', () => {
    const wrapper = mount(FieldTextarea, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    const html = wrapper.html()
    expect(html).toContain('textarea')
    expect(html).toContain('e-field e-field--textarea')
    expect(html).toContain('myValue')
    expect(html).toContain('My Value Field')
  })
  it('passes the rules', () => {
    const wrapper = mount(FieldTextarea, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    expect(wrapper.findComponent(ValidationProvider).props('rules')).toEqual('required|min:4')
  })
  it('displays valid state on change', async () => {
    const wrapper = mount(FieldTextarea, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.find('textarea').setValue('testytest')
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    await Vue.nextTick()
    expect(wrapper.find('.e-field__label').classes('valid')).toBeTruthy()
    expect(wrapper.find('.e-field__textarea').classes('valid')).toBeTruthy()
  })
  it('displays error on change', async () => {
    const wrapper = mount(FieldTextarea, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.find('textarea').setValue('')
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    await Vue.nextTick()
    expect(wrapper.find('.e-field__label').classes('invalid')).toBeTruthy()
    expect(wrapper.find('.e-field__textarea').classes('invalid')).toBeTruthy()
    expect(wrapper.html()).toContain('My Value Field is not valid.')
  })
  it('emits change event on blur', async () => {
    const wrapper = mount(FieldTextarea, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.find('textarea').setValue('testytest')
    wrapper.find('textarea').trigger('blur')
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    expect(wrapper.emitted('change', 'testytest')).toBeTruthy()
  })

  it('emits no change event on blur with invalid input', async () => {
    const wrapper = mount(FieldTextarea, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.find('textarea').setValue('wr')
    wrapper.find('textarea').trigger('blur')
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    expect(wrapper.emitted('change')).toBeFalsy()
  })
  it('updates cache on value change from parent', async () => {
    const wrapper = mount(FieldTextarea, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.setProps({
      value: 'testytest',
    })
    await Vue.nextTick()
    expect(wrapper.vm.cache).toEqual('testytest')
  })
  it('displays max remaining character count', async () => {
    const wrapper = mount(FieldTextarea, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        rules: 'required|min:4|max:500',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.find('textarea').setValue('testytest')
    wrapper.find('textarea').trigger('blur')
    expect(wrapper.vm.remainingCharacters).toEqual(500 - 'testytest'.length)
    await Vue.nextTick()
    expect(wrapper.html()).toContain('ui.input.remaining')
  })
  it('passes focus event on to Field component', async () => {
    const wrapper = mount(FieldTextarea, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    expect(wrapper.findComponent(Field).emitted('focus')).toBeFalsy()
    await wrapper.find('textarea').trigger('focus')
    expect(wrapper.findComponent(Field).emitted('focus')).toBeTruthy()
  })
})
