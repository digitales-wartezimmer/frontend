import { shallowMount } from '@vue/test-utils'
import Field from '@/components/elements/fields/Field.vue'

describe('Field', () => {
  function mountCmp (props = {}) {
    return shallowMount(Field, {
      propsData: {
        alias: 'my-alias',
        name: 'My Field',
        rules: '',
        ...props,
      },
      slots: {
        default: '<p>Field Content</p>',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
    })
  }

  it('renders', () => {
    const wrapper = mountCmp()
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    expect(html).toContain('<p>Field Content</p>')
    expect(html).toContain('field-my-alias')
    expect(html).toContain('My Field')
  })
  it('shows a valid state', () => {
    const wrapper = mountCmp({
      errors: [],
      changed: true,
    })
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    expect(wrapper.find('.e-field__label').classes()).toContain('valid')
  })
  it('shows an error state', () => {
    const wrapper = mountCmp({
      errors: ['this is required'],
      changed: true,
    })
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    expect(html).toContain('this is required')
    expect(wrapper.find('.e-field__label').classes()).toContain('invalid')
    expect(wrapper.find('.e-field__notes__note').classes()).toContain('error')
  })
  describe('displays a required mark', () => {
    it('handles a rule string', async () => {
      const wrapper = mountCmp({
        rules: 'required',
      })
      expect(wrapper.find('.e-field__label__required-mark').exists()).toBeTruthy()
      await wrapper.setProps({
        rules: 'other_rule',
      })
      expect(wrapper.find('.e-field__label__required-mark').exists()).toBeFalsy()
      await wrapper.setProps({
        rules: 'min_array_length:1',
      })
      expect(wrapper.find('.e-field__label__required-mark').exists()).toBeTruthy()
    })
    it('handles a rule object', async () => {
      const wrapper = mountCmp({
        rules: { required: true },
      })
      expect(wrapper.find('.e-field__label__required-mark').exists()).toBeTruthy()
      await wrapper.setProps({
        rules: { other_rule: true },
      })
      expect(wrapper.find('.e-field__label__required-mark').exists()).toBeFalsy()
      await wrapper.setProps({
        rules: { required: false },
      })
      expect(wrapper.find('.e-field__label__required-mark').exists()).toBeFalsy()
    })
  })
  describe('handles transitioning label', () => {
    let wrapper
    beforeEach(() => {
      wrapper = mountCmp({
        hasTransitioningLabel: true,
      })
    })
    it('renders', () => {
      expect(wrapper.html()).toBeTruthy()
      expect(wrapper.classes()).toContain('e-field--transitioning-label')
    })
    it('handle classes from props as string', async () => {
      await wrapper.setProps({
        fieldClass: 'class1 class2',
        isEmpty: false,
      })
      expect(wrapper.classes()).toEqual(['e-field', 'class1', 'class2', 'e-field--transitioning-label'])
    })
    it('handle classes from props as array', async () => {
      await wrapper.setProps({
        fieldClass: ['class1', 'class2'],
        isEmpty: false,
      })
      expect(wrapper.classes()).toEqual(['e-field', 'class1', 'class2', 'e-field--transitioning-label'])
    })
    it('handle classes from props as object', async () => {
      await wrapper.setProps({
        fieldClass: { class1: true, class2: true },
        isEmpty: false,
      })
      expect(wrapper.classes()).toEqual(['e-field', 'class1', 'class2', 'e-field--transitioning-label'])
    })
    it('handles empty state', async () => {
      await wrapper.setProps({
        isEmpty: true,
      })
      expect(wrapper.classes()).toContain('e-field--empty')
      await wrapper.setProps({
        isEmpty: false,
      })
      expect(wrapper.classes()).not.toContain('e-field--empty')
    })
    it('handles focus event', async () => {
      expect(wrapper.classes()).not.toContain('e-field--focused')
      await wrapper.vm.$emit('focus')
      expect(wrapper.classes()).toContain('e-field--focused')
    })
    it('handles blur event', async () => {
      expect(wrapper.classes()).not.toContain('e-field--focused')
      await wrapper.vm.$emit('focus')
      expect(wrapper.classes()).toContain('e-field--focused')
      await wrapper.vm.$emit('blur')
      expect(wrapper.classes()).not.toContain('e-field--focused')
    })
  })
})
