import Vue from 'vue'
import { createLocalVue, mount } from '@vue/test-utils'
import { ValidationProvider, extend } from 'vee-validate'
import { required, min } from 'vee-validate/dist/rules.umd.js'
import FieldSwitch from '@/components/elements/fields/FieldSwitch.vue'

describe('FieldSwitch', () => {
  const localVue = createLocalVue()
  extend('required', required)
  extend('min', min)
  localVue.component('ValidationProvider', ValidationProvider)

  const options = [{
    value: 'one',
    caption: 'Option 1',
    description: 'This is the first Option',
    icon: 'external-link-alt',
  }, {
    value: 'two',
    caption: 'Option 2',
    description: 'This is the second Option',
    icon: 'external-link-alt',
  }]

  it('renders', () => {
    const wrapper = mount(FieldSwitch, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        options,
        rules: '',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    const html = wrapper.html()
    expect(html).toContain('input')
    expect(html).toContain('e-field e-field--switch')
    expect(html).toContain('myValue')
    expect(html).toContain('My Value Field')
    expect(html).toContain('type="radio"')
    for (const option of options) {
      expect(html).toContain(`value="${option.value}"`)
      expect(html).toContain(option.caption)
      expect(html).toContain(option.icon)
    }
  })
  it('emits change event on click', async () => {
    const wrapper = mount(FieldSwitch, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        options,
        rules: '',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.findAll('input').at(0).setChecked(true)
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    expect(wrapper.emitted('change', 'one')).toBeTruthy()
    wrapper.findAll('input').at(1).setChecked(true)
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    expect(wrapper.emitted('change', 'two')).toBeTruthy()
  })
  it('does not emit event for empty value', () => {
    const wrapper = mount(FieldSwitch, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: 'one',
        options,
        rules: '',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.setProps({
      value: '',
    })
    expect(wrapper.emitted('change')).toBeFalsy()
  })
  it('updates cache on value change from parent', async () => {
    const wrapper = mount(FieldSwitch, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        options,
        rules: '',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.setProps({
      value: 'testytest',
    })
    await Vue.nextTick()
    expect(wrapper.vm.cache).toEqual('testytest')
  })
})
