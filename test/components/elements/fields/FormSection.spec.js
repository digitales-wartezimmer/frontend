import { mount } from '@vue/test-utils'
import FormSection from '@/components/elements/fields/FormSection.vue'

describe('FormSection', () => {
  it('renders the default slot', () => {
    const wrapper = mount(FormSection, {
      slots: {
        default: '<p>Slot Content</p>',
      },
      propsData: {
        caption: 'This is my Caption',
      },
    })
    const html = wrapper.html()
    expect(html).toContain('<p>Slot Content</p>')
  })
  it('renders a caption', () => {
    const wrapper = mount(FormSection, {
      slots: {
        default: '<p>Slot Content</p>',
      },
      propsData: {
        caption: 'This is my Caption',
      },
    })
    const html = wrapper.html()
    expect(html).toContain('This is my Caption')
  })
  it('does not render a description', () => {
    const wrapper = mount(FormSection, {
      slots: {
        default: '<p>Slot Content</p>',
      },
      propsData: {
        caption: 'This is my Caption',
        description: 'My Description, which is longer',
      },
    })
    const html = wrapper.html()
    expect(html).not.toContain('My Description, which is longer')
  })
})
