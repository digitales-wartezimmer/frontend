import Vue from 'vue'
import { createLocalVue, mount } from '@vue/test-utils'
import { ValidationProvider, extend } from 'vee-validate'
import { required, min } from 'vee-validate/dist/rules.umd.js'
import FieldInput from '@/components/elements/fields/FieldInput.vue'
import Field from '@/components/elements/fields/Field.vue'

describe('FieldInput', () => {
  const localVue = createLocalVue()
  extend('required', required)
  extend('min', min)
  localVue.component('ValidationProvider', ValidationProvider)

  it('renders', () => {
    const wrapper = mount(FieldInput, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        type: 'text',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    const html = wrapper.html()
    expect(html).toContain('input')
    expect(html).toContain('e-field')
    expect(html).toContain('myValue')
    expect(html).toContain('My Value Field')
    expect(html).toContain('type="text"')
  })
  it('sets the correct type', () => {
    const wrapper = mount(FieldInput, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        type: 'email',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    const html = wrapper.html()

    expect(html).toContain('type="email"')
  })
  it('passes the rules', () => {
    const wrapper = mount(FieldInput, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        type: 'text',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    expect(wrapper.findComponent(ValidationProvider).props('rules')).toEqual('required|min:4')
  })
  it('displays valid state on change', async () => {
    const wrapper = mount(FieldInput, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        type: 'text',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.find('input').setValue('testytest')
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    await Vue.nextTick()
    expect(wrapper.find('.e-field__label').classes('valid')).toBeTruthy()
    expect(wrapper.find('input').classes('valid')).toBeTruthy()
    expect(wrapper.html()).toContain('icon="check-circle"')
  })
  it('displays error on change', async () => {
    const wrapper = mount(FieldInput, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        type: 'text',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.find('input').setValue('')
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    await Vue.nextTick()
    expect(wrapper.find('.e-field__label').classes('invalid')).toBeTruthy()
    expect(wrapper.find('input').classes('invalid')).toBeTruthy()
    expect(wrapper.html()).toContain('My Value Field is not valid.')
  })
  it('emits change event on blur', async () => {
    const wrapper = mount(FieldInput, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        type: 'text',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.find('input').setValue('testytest')
    wrapper.find('input').trigger('blur')
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    expect(wrapper.emitted('change', 'testytest')).toBeTruthy()
  })

  it('emits empty change event on blur with invalid input', async () => {
    const wrapper = mount(FieldInput, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        type: 'text',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.find('input').setValue('wr')
    wrapper.find('input').trigger('blur')
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    expect(wrapper.emitted('change')).toBeFalsy()
  })
  it('updates cache on value change from parent', async () => {
    const wrapper = mount(FieldInput, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        type: 'text',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.setProps({
      value: 'testytest',
    })
    await Vue.nextTick()
    expect(wrapper.vm.cache).toEqual('testytest')
  })
  it('passes focus event on to Field component', async () => {
    const wrapper = mount(FieldInput, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        type: 'text',
        value: '',
        rules: 'required|min:4',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    expect(wrapper.findComponent(Field).emitted('focus')).toBeFalsy()
    await wrapper.find('input').trigger('focus')
    expect(wrapper.findComponent(Field).emitted('focus')).toBeTruthy()
  })
})
