import Vue from 'vue'
import dayjs from 'dayjs'
import { createLocalVue, mount } from '@vue/test-utils'
import { ValidationProvider, extend } from 'vee-validate'
import { required } from 'vee-validate/dist/rules.umd.js'
import VueDatePicker from '@mathieustan/vue-datepicker'
import FieldDatepicker from '@/components/elements/fields/FieldDatepicker.vue'
import Field from '@/components/elements/fields/Field.vue'

describe('FieldDatepicker', () => {
  const localVue = createLocalVue()
  extend('required', required)
  localVue.component('ValidationProvider', ValidationProvider)
  localVue.component('VueDatePicker', VueDatePicker)

  function mountCmp (props = {}) {
    return mount(FieldDatepicker, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        type: 'date',
        value: '',
        rules: 'required',
        storeKey: 'myForm',
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
        VueDatePicker: true,
      },
      mocks: {
        $t: msg => msg,
        $i18n: {
          locale: 'de',
        },
        $store: {
          state: {
            myForm: {},
          },
        },
      },
      localVue,
    })
  }
  it('renders', () => {
    const wrapper = mountCmp()
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    expect(html).toContain('vuedatepicker-stub')
    expect(html).toContain('e-field')
    expect(html).toContain('myValue')
    expect(html).toContain('My Value Field')
  })
  it('passes the rules', () => {
    const wrapper = mountCmp()
    expect(wrapper.findComponent(ValidationProvider).props('rules')).toEqual('required')
  })
  it('emits change event on blur', async () => {
    let wrapper = mountCmp()
    wrapper.vm.cacheDate = '2020-01-01'
    wrapper.vm.handleBlur('2020-01-01')
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    expect(wrapper.emitted('change', '01.01.2020')).toBeTruthy()

    wrapper = mountCmp()
    wrapper.vm.cacheDate = ''
    wrapper.vm.handleBlur('')
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    expect(wrapper.emitted('change')).toBeFalsy()
  })
  it('updates cache on value change from parent', async () => {
    const wrapper = mountCmp()
    wrapper.setProps({
      value: '01.01.2019',
    })
    await Vue.nextTick()
    expect('2018-12-31 2019-01-01').toContain(dayjs(wrapper.vm.cacheDate).format('YYYY-MM-DD')) // Contain check to allow timezone diff
    wrapper.setProps({
      value: '2016-03-03',
    })
    await Vue.nextTick()
    expect('2016-03-02 2016-03-03').toContain(dayjs(wrapper.vm.cacheDate).format('YYYY-MM-DD')) // Contain check to allow timezone diff
    wrapper.setProps({
      value: 'wrong',
    })
    await Vue.nextTick()
    expect(wrapper.vm.cacheDate).toEqual(undefined)
    wrapper.setProps({
      value: '',
    })
    await Vue.nextTick()
    expect(wrapper.vm.cacheDate).toEqual(undefined)
  })
  it('passes focus event on to Field component', async () => {
    const wrapper = mountCmp()
    expect(wrapper.findComponent(Field).emitted('focus')).toBeFalsy()
    await wrapper.find('vuedatepicker-stub').vm.$emit('onOpen')
    expect(wrapper.findComponent(Field).emitted('focus')).toBeTruthy()
  })
  it('handles change from datepicker', async () => {
    let wrapper = mountCmp()
    wrapper.vm.cacheDate = '2020-01-01'
    await wrapper.find('vuedatepicker-stub').vm.$emit('onChange')
    expect(wrapper.emitted('input', '01.01.2020')).toBeTruthy()
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    await Vue.nextTick()
    expect(wrapper.emitted('change', '01.01.2020')).toBeTruthy()

    wrapper = mountCmp()
    wrapper.vm.cacheDate = undefined
    await wrapper.find('vuedatepicker-stub').vm.$emit('onChange')
    expect(wrapper.emitted('input')).toBeFalsy()
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    await Vue.nextTick()
    expect(wrapper.emitted('change')).toBeFalsy()
  })
  it('is a birthday picker', async () => {
    const wrapper = mountCmp({ isBirthdayPicker: true })
    wrapper.vm.$refs.datepicker = { $refs: { agenda: { mode: 'day' } }, test: 'foo' }
    await wrapper.find('vuedatepicker-stub').vm.$emit('onOpen')
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    await Vue.nextTick()
    expect(wrapper.vm.$refs.datepicker.$refs.agenda.mode).toEqual('year')
    expect(wrapper.find('vuedatepicker-stub').attributes('visible-years-number')).toEqual('100')
  })
  it('provides function to parse text input', () => {
    const wrapper = mountCmp()
    expect(wrapper.vm.getDateForInput('01.01.2020')).toEqual('2020-01-01')
  })
  describe('it can set a min year', () => {
    it('does not set anything if undefined', () => {
      const wrapper = mountCmp({ minDate: undefined })
      expect(wrapper.find('vuedatepicker-stub').attributes('min-date')).toEqual(undefined)
    })
    it('does set to specific date if given as string', async () => {
      const wrapper = mountCmp({ minDate: '2019-01-01' })
      await Vue.nextTick()
      expect(wrapper.find('vuedatepicker-stub').attributes('min-date')).toEqual('2019-01-01')
    })
    it('does set to todays date if given as shortcut string', () => {
      const wrapper = mountCmp({ minDate: 'today' })
      expect(wrapper.find('vuedatepicker-stub').attributes('min-date')).toEqual(dayjs().format('YYYY-MM-DD'))
    })
    it('does set to date if given as function', async () => {
      const wrapper = mountCmp({ minDate: () => '2018-12-12' })
      await Vue.nextTick()
      expect(wrapper.find('vuedatepicker-stub').attributes('min-date')).toEqual('2018-12-12')
    })
  })
  describe('it can set a max year', () => {
    it('does not set anything if undefined', () => {
      const wrapper = mountCmp({ maxDate: undefined })
      expect(wrapper.find('vuedatepicker-stub').attributes('max-date')).toEqual(undefined)
    })
    it('does set to specific date if given as string', async () => {
      const wrapper = mountCmp({ maxDate: '2019-01-01' })
      await Vue.nextTick()
      expect(wrapper.find('vuedatepicker-stub').attributes('max-date')).toEqual('2019-01-01')
    })
    it('does set to todays date if given as shortcut string', () => {
      const wrapper = mountCmp({ maxDate: 'today' })
      expect(wrapper.find('vuedatepicker-stub').attributes('max-date')).toEqual(dayjs().format('YYYY-MM-DD'))
    })
    it('does set to date if given as function', async () => {
      const wrapper = mountCmp({ maxDate: () => '2018-12-12' })
      await Vue.nextTick()
      expect(wrapper.find('vuedatepicker-stub').attributes('max-date')).toEqual('2018-12-12')
    })
  })
  it('calcualtes classes for the input field', () => {
    const wrapper = mountCmp()
    expect(wrapper.vm.inputClasses([], false)).toEqual({ invalid: false, empty: true, valid: false })
    expect(wrapper.vm.inputClasses([{}], false)).toEqual({ invalid: true, empty: true, valid: false })
    expect(wrapper.vm.inputClasses([{}], true)).toEqual({ invalid: true, empty: true, valid: false })
    expect(wrapper.vm.inputClasses([], true)).toEqual({ invalid: false, empty: true, valid: true })
    wrapper.vm.cache = '2020-01-01'
    expect(wrapper.vm.inputClasses([], true)).toEqual({ invalid: false, empty: false, valid: true })
  })
})
