import Vue from 'vue'
import { createLocalVue, mount } from '@vue/test-utils'
import { ValidationProvider, extend } from 'vee-validate'
import { required, min } from 'vee-validate/dist/rules.umd.js'
import FieldRadio from '@/components/elements/fields/FieldRadio.vue'

describe('FieldRadio', () => {
  const localVue = createLocalVue()
  extend('required', required)
  extend('min', min)
  localVue.component('ValidationProvider', ValidationProvider)

  const options = [{
    value: 'one',
    caption: 'Option 1',
    description: 'This is the first Option',
  }, {
    value: 'two',
    caption: 'Option 2',
    description: 'This is the second Option',
  }]

  it('renders', () => {
    const wrapper = mount(FieldRadio, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        options,
        rules: '',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    const html = wrapper.html()
    expect(html).toContain('input')
    expect(html).toContain('e-field e-field--radio')
    expect(html).toContain('myValue')
    expect(html).toContain('My Value Field')
    expect(html).toContain('type="radio"')
    for (const option of options) {
      expect(html).toContain(`value="${option.value}"`)
      expect(html).toContain(option.caption)
      expect(html).toContain(option.description)
    }
  })
  it('emits change event on click', async () => {
    const wrapper = mount(FieldRadio, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        options,
        rules: '',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.findAll('input').at(0).setChecked(true)
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    expect(wrapper.emitted('change', 'one')).toBeTruthy()
    wrapper.findAll('input').at(1).setChecked(true)
    await new Promise(resolve => setTimeout(resolve, 21)) // Flush Validation Promise vee-validate#2752
    expect(wrapper.emitted('change', 'two')).toBeTruthy()
  })
  it('updates cache on value change from parent', async () => {
    const wrapper = mount(FieldRadio, {
      propsData: {
        alias: 'myValue',
        name: 'My Value Field',
        value: '',
        options,
        rules: '',
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
      localVue,
    })
    wrapper.setProps({
      value: 'testytest',
    })
    await Vue.nextTick()
    expect(wrapper.vm.cache).toEqual('testytest')
  })
})
