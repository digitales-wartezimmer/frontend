import { mount } from '@vue/test-utils'
import Container from '@/components/elements/Container.vue'

describe('Container', () => {
  it('renders a div with a default slot', () => {
    const wrapper = mount(Container, {
      slots: {
        default: '<p>Slot Content</p>',
      },
    })
    const html = wrapper.html()
    expect(html).toContain('<p>Slot Content</p>')
  })
  it('renders a div with padding', () => {
    const wrapper = mount(Container, {
      propsData: {
        padding: 'large',
      },
    })
    const html = wrapper.html()
    expect(html).toContain('e-container--padding-large')
  })
  it('renders a div with width', () => {
    const wrapper = mount(Container, {
      propsData: {
        width: 'wide',
      },
    })
    const html = wrapper.html()
    expect(html).toContain('e-container--width-wide')
  })
})
