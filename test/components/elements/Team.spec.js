import { mount } from '@vue/test-utils'
import Team from '@/components/elements/Team.vue'

const members = {
  till: {
    key: 'till',
    name: 'Till Sanders',
    position: 'Frontend & Design',
    link: {
      href: 'https://www.linkedin.com/in/tillsanders/',
      icon: ['fab', 'linkedin'],
    },
  },
  berni: {
    key: 'berni',
    name: 'Bernhard Wittmann',
    position: 'Frontend',
    link: {
      href: 'https://www.linkedin.com/in/bernhard-wittmann/',
      icon: ['fab', 'linkedin'],
    },
  },
}

describe('Team', () => {
  it('renders', () => {
    const wrapper = mount(Team, {
      propsData: {
        members,
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
      mocks: {
        $config: {
          baseUrl: 'http://base.de',
        },
      },
    })
    const html = wrapper.html()
    expect(html).toContain('e-team')
    for (const member of Object.values(members)) {
      expect(html).toContain(member.name)
      expect(html).toContain(member.position.replace('&', '&amp;'))
      expect(html).toContain(member.link.href)
    }
  })
  it('returns the jsonld of the team members', () => {
    const wrapper = mount(Team, {
      propsData: {
        members,
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
      mocks: {
        $config: {
          baseUrl: 'http://base.de',
        },
      },
    })
    expect(wrapper.vm.$options.jsonld.apply(wrapper.vm)).toEqual({
      '@context': 'http://schema.org',
      '@type': 'Organization',
      name: 'Digitales Wartezimmer',
      employee: Object.values(members).map(member => ({
        '@type': 'Person',
        image: `http://base.de/assets/team/${member.key}.jpg`,
        jobTitle: member.position,
        name: member.name,
        sameAs: member.link.href,
      })),
    })
  })
  it('handles empty team members object in jsonld', () => {
    const wrapper = mount(Team, {
      propsData: {
        members: {},
      },
      stubs: {
        'font-awesome-icon': true,
        'nuxt-link': {
          template: '<a :href="to"><slot /></a>',
          props: ['to'],
        },
      },
      mocks: {
        $config: {
          baseUrl: 'http://base.de',
        },
      },
    })
    expect(wrapper.vm.$options.jsonld.apply(wrapper.vm)).toEqual({
      '@context': 'http://schema.org',
      '@type': 'Organization',
      name: 'Digitales Wartezimmer',
      employee: [],
    })
  })
})
