import { shallowMount } from '@vue/test-utils'
import SegmentedCard from '@/components/elements/SegmentedCard.vue'

describe('SegmentedCard', () => {
  function mountCmp (props = {}) {
    return shallowMount(SegmentedCard, {
      propsData: {
        segments: [{
          caption: 'Segment 1',
          text: 'Segment 1 Text',
        }, {
          caption: 'Segment 2',
          text: 'Segment 2 Text',
        }, {
          caption: 'Segment 3',
          text: 'Segment 3 Text',
        }],
        ...props,
      },
      stubs: {
        'font-awesome-icon': true,
      },
      mocks: {
        $t: msg => msg,
      },
    })
  }
  it('renders', () => {
    const wrapper = mountCmp()
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
    expect(wrapper.findAll('.m-segmented-card__segment')).toHaveLength(3)
  })
  it('renders with no segments', () => {
    const wrapper = mountCmp({
      segments: undefined,
    })
    const html = wrapper.html()
    expect(html).toMatchSnapshot()
  })
  it('can have a different Width', async () => {
    const wrapper = mountCmp({
      width: 'normal',
    })
    expect(wrapper.find('container-stub').props('width')).toEqual('normal')
    await wrapper.setProps({
      width: 'slim',
    })
    expect(wrapper.find('container-stub').props('width')).toEqual('slim')
    await wrapper.setProps({
      width: 'narrow',
    })
    expect(wrapper.find('container-stub').props('width')).toEqual('narrow')
    await wrapper.setProps({
      width: 'wide',
    })
    expect(wrapper.find('container-stub').props('width')).toEqual('wide')
  })
  it('can have a different vertical Spacing', async () => {
    const wrapper = mountCmp({
      marginY: 'normal',
    })
    expect(wrapper.find('container-stub').props('marginY')).toEqual('normal')
    await wrapper.setProps({
      marginY: 'none',
    })
    expect(wrapper.find('container-stub').props('marginY')).toEqual('none')
    await wrapper.setProps({
      marginY: 'large',
    })
    expect(wrapper.find('container-stub').props('marginY')).toEqual('large')
    await wrapper.setProps({
      marginY: 'larger',
    })
    expect(wrapper.find('container-stub').props('marginY')).toEqual('larger')
  })
})
