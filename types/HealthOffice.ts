export interface HealthOffice {
  _id: string
  name: string
  department: string
  code: string
  transmissionType: string
  pdfServiceEnabled: boolean
  address: {
    street: string
    postCode: number
    place: string
  }
  contact: {
    phone: string
    fax: string
    mail: string
  }
  jurisdiction: Array<number>
  __v?: number
}
