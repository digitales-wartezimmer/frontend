import { PropOptions, PropType } from 'vue'

export default function EnumProp<T extends string> (defaultValue: T | undefined, typeEnum: Record<T, any>): PropOptions<T> {
  return {
    type: String as unknown as PropType<T>,
    default: defaultValue,
    validator: (v: T) => typeEnum[v] !== undefined,
  } as unknown as PropOptions<T>
}
