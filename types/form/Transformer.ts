import dayjs from 'dayjs'
import { PhoneNumberUtil, PhoneNumberFormat } from 'google-libphonenumber'
import { Store } from '~/types/form/Store'
const phoneUtil = PhoneNumberUtil.getInstance()

interface BaseDTO {
  idempotencyToken: string
  timestamp: string
}

export abstract class Transformer<FIELD_KEY extends string, STEP_ID extends string, DTO extends Record<string, any>> {
  abstract getData (store: Store<FIELD_KEY, STEP_ID>): DTO
  getBaseData (idempotencyToken: string): BaseDTO {
    return {
      idempotencyToken,
      timestamp: (new Date()).toISOString(),
    }
  }

  getRequestData (store: Store<FIELD_KEY, STEP_ID>, idempotencyToken: string): DTO & BaseDTO {
    return {
      ...this.getBaseData(idempotencyToken),
      ...this.getData(store),
    }
  }

  dateToISOString (value: any): string | undefined {
    const germanDate = dayjs(value, 'DD.MM.YYYY')
    if (germanDate.isValid()) {
      return germanDate.format('YYYY-MM-DDTHH:mm:ss')
    }
    const date = dayjs(value)
    if (date.isValid()) {
      return date.format('YYYY-MM-DDTHH:mm:ss')
    }
    return undefined
  }

  confirmRadioToBoolean (value: string | undefined): boolean {
    return value === 'yes'
  }

  transformPhoneNumber (value: string): string {
    let number
    try {
      number = phoneUtil.parseAndKeepRawInput(value, 'DE')
    } catch (e) {
      return value
    }
    if (!number || !phoneUtil.isValidNumber(number)) {
      return value
    }
    return phoneUtil.format(number, PhoneNumberFormat.INTERNATIONAL)
  }
}
