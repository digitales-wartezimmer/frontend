import { EmailTemplate } from '~/types/EmailTemplate'

export type SharedStore<STEP_ID extends string> = {
  currentStep: STEP_ID | undefined
  responseData: any,
  quarantineStartDate: EmailTemplate | undefined,
  zip: string | undefined
}

export type Store<FIELD_KEY extends string, STEP_ID extends string> = SharedStore<STEP_ID> & {
  data: {} & Record<FIELD_KEY, any>
}
