import { Field, Text } from '~/types/form/Field'

export interface Section<STEP_ID extends string, SECTION_KEY extends string, PROGRESS_STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> {
  fields: ReadonlyArray<Field<STEP_ID, FIELD_KEY, DTO> | ReadonlyArray<Field<STEP_ID, FIELD_KEY, DTO>> | Text<STEP_ID, FIELD_KEY>>
  key: SECTION_KEY,
  progressKey: PROGRESS_STEP_ID
}
