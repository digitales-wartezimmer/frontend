import { Section } from '~/types/form/Section'
import { Store } from '~/types/form/Store'
import { FORM_IDS } from '~/content/forms'

interface StepInterface<STEP_ID extends string, SECTION_KEY extends string, PROGRESS_STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> {
  section: Section<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>,
  key: STEP_ID
  noBackButton?: boolean
  noNextButton?: boolean
  nextButtonText?: string
  backButtonText?: string
  changeForm?: boolean
}

type NextFunction<STEP_ID extends string, FIELD_KEY extends string> =
  ((store: Store<FIELD_KEY, STEP_ID>) => STEP_ID)
  | STEP_ID

type ChangeFormFunction<STEP_ID extends string, FIELD_KEY extends string> =
  ((store: Store<FIELD_KEY, STEP_ID>) => FORM_IDS | null)
  | FORM_IDS

interface NormalStepInterface<STEP_ID extends string, SECTION_KEY extends string, PROGRESS_STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> extends StepInterface<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO> {
  changeForm?: false
}
interface StartStep<STEP_ID extends string, SECTION_KEY extends string, PROGRESS_STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> extends NormalStepInterface<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO> {
  start: true
  final?: false
  next: NextFunction<STEP_ID, FIELD_KEY>
  previous?: undefined
}

interface MidStep<STEP_ID extends string, SECTION_KEY extends string, PROGRESS_STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> extends NormalStepInterface<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO> {
  final?: false
  next: NextFunction<STEP_ID, FIELD_KEY>
  start?: false
  previous: NextFunction<STEP_ID, FIELD_KEY>
}

interface EndStep<STEP_ID extends string, SECTION_KEY extends string, PROGRESS_STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> extends NormalStepInterface<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO> {
  final: true
  next?: undefined
  start?: false
  previous: NextFunction<STEP_ID, FIELD_KEY>
}

interface StartEndStep<STEP_ID extends string, SECTION_KEY extends string, PROGRESS_STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> extends NormalStepInterface<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO> {
  final: true
  next?: undefined
  start?: true
  previous?: undefined
}

interface ChangeFormStep<STEP_ID extends string, SECTION_KEY extends string, PROGRESS_STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> extends StepInterface<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO> {
  final: true
  next?: undefined
  start?: false
  previous: NextFunction<STEP_ID, FIELD_KEY>
  form: ChangeFormFunction<FIELD_KEY, STEP_ID>
  key: STEP_ID
  changeForm: true
  noBackButton: true
  noNextButton: true
}

export type Step<STEP_ID extends string, SECTION_KEY extends string, PROGRESS_STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> =
  StartStep<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  | MidStep<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  | EndStep<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  | StartEndStep<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  | ChangeFormStep<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>

export interface ProgressStep<PROGRESS_STEP_ID extends string> {
  key: PROGRESS_STEP_ID
  title: string
  titleVisible?: boolean
}
export default interface Form<ID extends string, STEP_ID extends string, SECTION_KEY extends string, PROGRESS_STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> {
  steps: Record<STEP_ID, Step<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>>
  progress: Record<PROGRESS_STEP_ID, ProgressStep<PROGRESS_STEP_ID>>,
  storeKey: string
  id: ID
}
