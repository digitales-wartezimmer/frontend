import { Store } from '~/types/form/Store'
import { Transformer } from '~/types/form/Transformer'

type FieldTypes = 'component' | 'text' | 'textarea' | 'date' | 'radio' | 'switch' | 'checkbox'
export type ComponentNames = 'fetch-health-office' | 'form-reviewer' | 'form-submitter' | 'email-display' | 'quarantine-date' | 'loading' | 'start-over-button' | 'contacts' | 'success'

type TypeOrCalculationFunction<T, STEP_ID extends string, FIELD_KEY extends string> =
((store: Store<FIELD_KEY, STEP_ID>) => T)
| T
type OptionalTypeOrCalculationFunction<T, STEP_ID extends string, FIELD_KEY extends string> =
((store: Store<FIELD_KEY, STEP_ID>) => T | undefined)
| T | undefined

interface FieldOptions {
  disabled?: boolean
}
interface FieldInterface<STEP_ID extends string, FIELD_KEY extends string> {
  alias: string
  key: FIELD_KEY
  rules?: TypeOrCalculationFunction<string, STEP_ID, FIELD_KEY>
  type: FieldTypes
  hidden?: TypeOrCalculationFunction<boolean, STEP_ID, FIELD_KEY>
  hiddenInReview?: TypeOrCalculationFunction<boolean, STEP_ID, FIELD_KEY>
  onChange?: (newValue: any) => void
  options?: FieldOptions
}

interface BaseComponent<STEP_ID extends string, FIELD_KEY extends string> extends FieldInterface<STEP_ID, FIELD_KEY> {
  type: 'component'
  componentName: ComponentNames
}

export interface Component<STEP_ID extends string, FIELD_KEY extends string> extends FieldInterface<STEP_ID, FIELD_KEY> {
  type: 'component'
  componentName: Exclude<ComponentNames, 'form-submitter' | 'form-reviewer'>
}

export interface SubmitComponent<STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> extends BaseComponent<STEP_ID, FIELD_KEY> {
  componentName: 'form-submitter'
  apiUrl: string
  transformer: Transformer<FIELD_KEY, STEP_ID, DTO>
}

export interface ReviewComponent<STEP_ID extends string, FIELD_KEY extends string> extends BaseComponent<STEP_ID, FIELD_KEY> {
  componentName: 'form-reviewer'
  additionalFields?: (store: Store<FIELD_KEY, STEP_ID>) => Array<{ title: string, value: string, type: 'artificial' }>
}

export interface InputField<STEP_ID extends string, FIELD_KEY extends string> extends FieldInterface<STEP_ID, FIELD_KEY> {
  type: 'text' | 'textarea'
}

export interface DateField<STEP_ID extends string, FIELD_KEY extends string> extends FieldInterface<STEP_ID, FIELD_KEY> {
  type: 'date'
  allowedDates?: (date: string) => boolean
  minDate?: OptionalTypeOrCalculationFunction<string, STEP_ID, FIELD_KEY>
  maxDate?: OptionalTypeOrCalculationFunction<string, STEP_ID, FIELD_KEY>
  isBirthdayPicker?: boolean
}

export interface SelectOption {
  value: string,
  alias: string,
  icon?: string
}

export interface SelectField<STEP_ID extends string, FIELD_KEY extends string> extends FieldInterface<STEP_ID, FIELD_KEY> {
  type: 'radio' | 'switch' | 'checkbox'
  choices: ReadonlyArray<SelectOption>
}

export interface Text<STEP_ID extends string, FIELD_KEY extends string> {
  type: 'paragraph' | 'title',
  color?: 'default' | 'primary',
  content: string,
  hidden?: TypeOrCalculationFunction<boolean, STEP_ID, FIELD_KEY>
}

export type Field<STEP_ID extends string, FIELD_KEY extends string, DTO extends Record<string, any>> =
  Component<STEP_ID, FIELD_KEY>
  | SubmitComponent<STEP_ID, FIELD_KEY, DTO>
  | ReviewComponent<STEP_ID, FIELD_KEY>
  | InputField<STEP_ID, FIELD_KEY>
  | SelectField<STEP_ID, FIELD_KEY>
  | DateField<STEP_ID, FIELD_KEY>
