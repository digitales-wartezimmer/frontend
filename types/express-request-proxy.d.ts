declare module 'express-request-proxy' {
    const requestProxy: (params: any) => ((req: any, res: any, next: any) => {})
    export default requestProxy
}
  