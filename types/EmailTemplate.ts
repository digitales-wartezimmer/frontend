export interface EmailTemplate {
  userEmail: string
  healthOfficeEmail: string
  subject: string
  body: string
}
