declare module '@mathieustan/vue-datepicker' {
    import { PluginFunction } from 'vue'

  const VueDatePicker: PluginFunction<any>
  export default VueDatePicker
}
