declare module 'vuex-map-fields' {
  export function mapFields (namespace: string, fields: Array<string>): any
}
