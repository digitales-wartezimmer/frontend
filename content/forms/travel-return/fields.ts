import dayjs from 'dayjs'
import { STEP_ID } from './form'
import { DTO, Transformer } from './transformer'
import { Field } from '~/types/form/Field'
import { storeZipAndRetrieveCity } from '~/content/forms/shared'

export enum FIELD_KEY {
  'FIRST_NAME' = 'firstName',
  'LAST_NAME' = 'lastName',
  'ZIP' = 'zip',
  'FETCH_HEALTH_OFFICE' = 'fetchHealthOffice',
  'BIRTHDAY' = 'birthday',
  'GENDER' = 'gender',
  'STREET' = 'street',
  'CITY' = 'city',
  'EMAIL' = 'email',
  'PHONE' = 'phone',
  'TRAVEL_COUNTRY' = 'travelCountry',
  'TRAVEL_REGION' = 'travelRegion',
  'STAY_DURATION' = 'stayDuration',
  'DATE_OF_ENTRY' = 'dateOfEntry',
  'MODE_OF_ENTRY' = 'modeOfEntry',
  'HAS_SYMPTOMS' = 'hasSymptoms',
  'SYMPTOMS' = 'symptoms',
  'BEGIN_OF_SYMPTOMS' = 'beginOfSymptoms',
  'HAS_TESTED' = 'hasTested',
  'DATE_OF_TEST' = 'dateOfTest',
  'TEST_COUNTRY' = 'testCountry',
  'TEST_RESULT' = 'testResult',
  'IMMUNE_DISEASE' = 'immuneDisease',
  'HEART_DISEASE' = 'heartDisease',
  'DISEASE_EXPLANATION' = 'diseaseExplanation',
  'PREGNANCY' = 'pregnancy',
  'PREGNANCY_WEEK' = 'pregnancyWeek',
  'PERSONAL_NOTE' = 'personalNote',
  'CONSENT_GIVEN' = 'consentGiven',
  'FORM_REVIEW' = 'formReview',
  'FORM_SUBMIT' = 'formSubmit',
  'QUARANTINE_DISPLAY' = 'quarantineDisplay',
  'EMAIL_DISPLAY' = 'emailDisplay'
}

export const FieldMap: Record<FIELD_KEY, Field<STEP_ID, FIELD_KEY, DTO>> = {
  [FIELD_KEY.FIRST_NAME]: {
    type: 'text',
    key: FIELD_KEY.FIRST_NAME,
    alias: 'first-name',
    rules: 'required|max:64',
  },
  [FIELD_KEY.LAST_NAME]: {
    type: 'text',
    key: FIELD_KEY.LAST_NAME,
    alias: 'last-name',
    rules: 'required|max:64',
  },
  [FIELD_KEY.ZIP]: {
    type: 'text',
    key: FIELD_KEY.ZIP,
    alias: 'zip',
    rules: 'required|min:5|max:5|numeric|regex:[0-9]{5}',
    onChange: storeZipAndRetrieveCity,
  },
  [FIELD_KEY.FETCH_HEALTH_OFFICE]: {
    type: 'component',
    key: FIELD_KEY.FETCH_HEALTH_OFFICE,
    alias: 'fetch-health-office',
    componentName: 'fetch-health-office',
  },
  [FIELD_KEY.BIRTHDAY]: {
    type: 'date',
    key: FIELD_KEY.BIRTHDAY,
    alias: 'birthday',
    rules: 'required|date|not_future|alive',
    maxDate: 'today',
    isBirthdayPicker: true,
  },
  [FIELD_KEY.GENDER]: {
    type: 'switch',
    key: FIELD_KEY.GENDER,
    alias: 'gender',
    rules: 'required',
    choices: [{
      value: 'M',
      alias: 'male',
      icon: 'mars',
    },
    {
      value: 'F',
      alias: 'female',
      icon: 'venus',
    },
    {
      value: 'D',
      alias: 'diverse',
      icon: 'transgender-alt',
    }],
  },
  [FIELD_KEY.STREET]: {
    type: 'text',
    key: FIELD_KEY.STREET,
    alias: 'street',
    rules: 'required|max:64',
  },
  [FIELD_KEY.CITY]: {
    type: 'text',
    key: FIELD_KEY.CITY,
    alias: 'city',
    rules: 'required|max:64',
  },
  [FIELD_KEY.EMAIL]: {
    type: 'text',
    key: FIELD_KEY.EMAIL,
    alias: 'email',
    rules: 'email|max:64',
  },
  [FIELD_KEY.PHONE]: {
    type: 'text',
    key: FIELD_KEY.PHONE,
    alias: 'phone',
    rules: 'required|max:25|phone',
  },
  [FIELD_KEY.TRAVEL_COUNTRY]: {
    type: 'text',
    key: FIELD_KEY.TRAVEL_COUNTRY,
    alias: 'travel-country',
    rules: 'required|max:64',
  },

  [FIELD_KEY.TRAVEL_REGION]: {
    type: 'text',
    key: FIELD_KEY.TRAVEL_REGION,
    alias: 'travel-region',
    rules: 'required|max:64',
  },
  [FIELD_KEY.STAY_DURATION]: {
    type: 'text',
    key: FIELD_KEY.STAY_DURATION,
    alias: 'stay-duration',
    rules: 'required|numeric|min_value:1',
  },
  [FIELD_KEY.DATE_OF_ENTRY]: {
    type: 'date',
    key: FIELD_KEY.DATE_OF_ENTRY,
    alias: 'date-of-entry',
    rules: 'required|date|not_future|during_corona',
    maxDate: 'today',
    onChange (newValue) {
      (this as any).$store.commit((this as any).form.storeKey + '/updateField', {
        path: 'quarantineStartDate',
        value: newValue,
      })
    },
  },
  [FIELD_KEY.MODE_OF_ENTRY]: {
    type: 'checkbox',
    key: FIELD_KEY.MODE_OF_ENTRY,
    alias: 'mode-of-entry',
    rules: 'min_array_length:1',
    choices: [{
      alias: 'car',
      value: 'car',
    }, {
      alias: 'plane',
      value: 'plane',
    }, {
      alias: 'ship',
      value: 'ship',
    }, {
      alias: 'bus',
      value: 'bus',
    }, {
      alias: 'train',
      value: 'train',
    }],
  },
  [FIELD_KEY.HAS_SYMPTOMS]: {
    type: 'radio',
    key: FIELD_KEY.HAS_SYMPTOMS,
    alias: 'has-symptoms',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }, {
      alias: 'no',
      value: 'no',
    }],
  },
  [FIELD_KEY.SYMPTOMS]: {
    type: 'textarea',
    key: FIELD_KEY.SYMPTOMS,
    alias: 'symptoms',
    rules: 'required|max:500',
    hidden: (store) => {
      return store.data[FIELD_KEY.HAS_SYMPTOMS] !== 'yes'
    },
  },
  [FIELD_KEY.BEGIN_OF_SYMPTOMS]: {
    type: 'date',
    key: FIELD_KEY.BEGIN_OF_SYMPTOMS,
    alias: 'begin-of-symptoms',
    rules: 'required|date|during_corona|not_future',
    maxDate: 'today',
    hidden: (store) => {
      return store.data[FIELD_KEY.HAS_SYMPTOMS] !== 'yes'
    },
  },
  [FIELD_KEY.HAS_TESTED]: {
    type: 'radio',
    key: FIELD_KEY.HAS_TESTED,
    alias: 'has-tested',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }, {
      alias: 'no',
      value: 'no',
    }],
  },
  [FIELD_KEY.DATE_OF_TEST]: {
    type: 'date',
    key: FIELD_KEY.DATE_OF_TEST,
    alias: 'date-of-test',
    rules (this: any, store) {
      const result = 'required|date|during_corona|not_future|'
      return `${result}min_days_after:${store.data[FIELD_KEY.DATE_OF_ENTRY]},5`
    },
    maxDate: 'today',
    minDate (this: any, store) {
      if (!store.data[FIELD_KEY.DATE_OF_ENTRY]) {
        return undefined
      }
      const date = dayjs(store.data[FIELD_KEY.DATE_OF_ENTRY], 'DD.MM.YYYY')
      if (!date.isValid()) {
        return undefined
      }
      return date.add(5, 'day').format('YYYY-MM-DD')
    },
    hidden: (store) => {
      return store.data[FIELD_KEY.HAS_TESTED] !== 'yes'
    },
  },
  [FIELD_KEY.TEST_COUNTRY]: {
    type: 'text',
    key: FIELD_KEY.TEST_COUNTRY,
    alias: 'test-country',
    rules: 'required|max:64',
    hidden: (store) => {
      return store.data[FIELD_KEY.HAS_TESTED] !== 'yes'
    },
  },
  [FIELD_KEY.TEST_RESULT]: {
    type: 'radio',
    key: FIELD_KEY.TEST_RESULT,
    alias: 'test-result',
    rules: 'required',
    choices: [{
      alias: 'positive',
      value: 'positive',
    }, {
      alias: 'negative',
      value: 'negative',
    }, {
      alias: 'unknown',
      value: 'unknown',
    }],
    hidden: (store) => {
      return store.data[FIELD_KEY.HAS_TESTED] !== 'yes'
    },
  },
  [FIELD_KEY.IMMUNE_DISEASE]: {
    type: 'radio',
    key: FIELD_KEY.IMMUNE_DISEASE,
    alias: 'immune-disease',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }, {
      alias: 'no',
      value: 'no',
    }],
  },
  [FIELD_KEY.HEART_DISEASE]: {
    type: 'radio',
    key: FIELD_KEY.HEART_DISEASE,
    alias: 'heart-disease',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }, {
      alias: 'no',
      value: 'no',
    }],
  },
  [FIELD_KEY.DISEASE_EXPLANATION]: {
    type: 'text',
    key: FIELD_KEY.DISEASE_EXPLANATION,
    alias: 'disease-explanation',
    rules: 'required|max:200',
    hidden: (store) => {
      return store.data[FIELD_KEY.IMMUNE_DISEASE] !== 'yes' && store.data[FIELD_KEY.HEART_DISEASE] !== 'yes'
    },
  },
  [FIELD_KEY.PREGNANCY]: {
    type: 'radio',
    key: FIELD_KEY.PREGNANCY,
    alias: 'pregnancy',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }, {
      alias: 'no',
      value: 'no',
    }],
    hidden: (store) => {
      return !['D', 'F'].includes(store.data[FIELD_KEY.GENDER])
    },
  },
  [FIELD_KEY.PREGNANCY_WEEK]: {
    type: 'text',
    key: FIELD_KEY.PREGNANCY_WEEK,
    alias: 'pregnancy-week',
    rules: 'required|numeric|min_value:0|max:2',
    hidden: (store) => {
      return store.data[FIELD_KEY.PREGNANCY] !== 'yes'
    },
  },
  [FIELD_KEY.PERSONAL_NOTE]: {
    type: 'textarea',
    key: FIELD_KEY.PERSONAL_NOTE,
    alias: 'personal-note',
    rules: 'max:500',
  },
  [FIELD_KEY.CONSENT_GIVEN]: {
    type: 'checkbox',
    key: FIELD_KEY.CONSENT_GIVEN,
    alias: 'consent-given',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }],
    hiddenInReview: true,
  },
  [FIELD_KEY.FORM_REVIEW]: {
    type: 'component',
    key: FIELD_KEY.FORM_REVIEW,
    alias: 'form-review',
    componentName: 'form-reviewer',
  },
  [FIELD_KEY.FORM_SUBMIT]: {
    type: 'component',
    key: FIELD_KEY.FORM_SUBMIT,
    alias: 'form-submit',
    componentName: 'form-submitter',
    apiUrl: 'travelReturn',
    transformer: new Transformer(),
    onChange (newValue) {
      if (!newValue) {
        return
      }
      (this as any).$store.commit((this as any).form.storeKey + '/updateField', {
        path: 'responseData',
        value: newValue,
      })
    },
  },
  [FIELD_KEY.QUARANTINE_DISPLAY]: {
    type: 'component',
    key: FIELD_KEY.QUARANTINE_DISPLAY,
    alias: 'quarantine-display',
    componentName: 'quarantine-date',
  },
  [FIELD_KEY.EMAIL_DISPLAY]: {
    type: 'component',
    key: FIELD_KEY.EMAIL_DISPLAY,
    alias: 'email-display',
    componentName: 'email-display',
  },
}
