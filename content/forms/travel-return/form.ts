import { PROGRESS_STEP_ID, SECTION_KEY, SectionMap } from './sections'
import { FIELD_KEY } from './fields'

import AbstractForm, { Step as AbstractStep } from '~/types/form/Form'
import { Section as AbstractSection } from '~/types/form/Section'
import { Field as AbstractField } from '~/types/form/Field'
import { DTO } from '~/content/forms/travel-return/transformer'

export enum STEP_ID {
  'HEALTH_OFFICE' = 'health_office',
  'USER_DATA' = 'user_data',
  'TRAVEL' = 'travel',
  'SYMPTOMS' = 'symptoms',
  'TEST' = 'test',
  'PRECONDITIONS' = 'preconditions',
  'PERSONAL_NOTE' = 'personal_note',
  'REVIEW' = 'review',
  'SUBMIT' = 'submit',
  'RESULT' = 'result'
}

export { SECTION_KEY as SECTION_KEYS }
export { PROGRESS_STEP_ID as PROGRESS_STEP_IDS }
export { FIELD_KEY as FIELD_KEYS }
export { STEP_ID as STEP_IDS }

export namespace TravelReturnForm {
  export const ID = 'travel-return'
  export type ID = 'travel-return'
  export type Form = AbstractForm<'travel-return', STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Step = AbstractStep<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Section = AbstractSection<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Field = AbstractField<STEP_ID, FIELD_KEY, DTO>

  export const form: TravelReturnForm.Form = {
    id: TravelReturnForm.ID,
    storeKey: 'travelReturn',
    steps: {
      [STEP_ID.HEALTH_OFFICE]: {
        key: STEP_ID.HEALTH_OFFICE,
        section: SectionMap[SECTION_KEY.HEALTH_OFFICE],
        start: true,
        next: STEP_ID.USER_DATA,
      },
      [STEP_ID.USER_DATA]: {
        key: STEP_ID.USER_DATA,
        section: SectionMap[SECTION_KEY.USER_DATA],
        next: STEP_ID.TRAVEL,
        previous: STEP_ID.HEALTH_OFFICE,
      },
      [STEP_ID.TRAVEL]: {
        key: STEP_ID.TRAVEL,
        section: SectionMap[SECTION_KEY.TRAVEL],
        next: STEP_ID.SYMPTOMS,
        previous: STEP_ID.USER_DATA,
      },
      [STEP_ID.SYMPTOMS]: {
        key: STEP_ID.SYMPTOMS,
        section: SectionMap[SECTION_KEY.SYMPTOMS],
        next: STEP_ID.TEST,
        previous: STEP_ID.TRAVEL,
      },
      [STEP_ID.TEST]: {
        key: STEP_ID.TEST,
        section: SectionMap[SECTION_KEY.TEST],
        next: STEP_ID.PRECONDITIONS,
        previous: STEP_ID.SYMPTOMS,
      },
      [STEP_ID.PRECONDITIONS]: {
        key: STEP_ID.PRECONDITIONS,
        section: SectionMap[SECTION_KEY.PRECONDITIONS],
        next: STEP_ID.PERSONAL_NOTE,
        previous: STEP_ID.TEST,
      },
      [STEP_ID.PERSONAL_NOTE]: {
        key: STEP_ID.PERSONAL_NOTE,
        section: SectionMap[SECTION_KEY.PERSONAL_NOTE],
        next: STEP_ID.REVIEW,
        previous: STEP_ID.PRECONDITIONS,
      },
      [STEP_ID.REVIEW]: {
        key: STEP_ID.REVIEW,
        section: SectionMap[SECTION_KEY.REVIEW],
        final: false,
        next: STEP_ID.SUBMIT,
        previous: STEP_ID.PERSONAL_NOTE,
        nextButtonText: 'pages.travel-return.form-reviewer.complete',
      },
      [STEP_ID.SUBMIT]: {
        key: STEP_ID.SUBMIT,
        section: SectionMap[SECTION_KEY.SUBMIT],
        final: false,
        noBackButton: true,
        noNextButton: true,
        next: STEP_ID.RESULT,
        previous: STEP_ID.REVIEW,
      },
      [STEP_ID.RESULT]: {
        key: STEP_ID.RESULT,
        section: SectionMap[SECTION_KEY.RESULT],
        final: true,
        noBackButton: true,
        previous: STEP_ID.REVIEW,
      },
    },
    progress: {
      [PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE]: {
        key: PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE,
        title: 'pages.travel-return.progress.health_office',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.USER_DATA]: {
        key: PROGRESS_STEP_ID.USER_DATA,
        title: 'pages.travel-return.progress.user_data',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.TRAVEL]: {
        key: PROGRESS_STEP_ID.TRAVEL,
        title: 'pages.travel-return.progress.source_of_infection',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.SYMPTOMS]: {
        key: PROGRESS_STEP_ID.SYMPTOMS,
        title: 'pages.travel-return.progress.symptoms',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.TEST]: {
        key: PROGRESS_STEP_ID.TEST,
        title: 'pages.travel-return.progress.infection_data',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.PRECONDITIONS]: {
        key: PROGRESS_STEP_ID.PRECONDITIONS,
        title: 'pages.travel-return.progress.preconditions',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.PERSONAL_NOTE]: {
        key: PROGRESS_STEP_ID.PERSONAL_NOTE,
        title: 'pages.travel-return.progress.personal_note',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.REVIEW]: {
        key: PROGRESS_STEP_ID.REVIEW,
        title: 'pages.travel-return.progress.review',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.SUBMIT]: {
        key: PROGRESS_STEP_ID.SUBMIT,
        title: 'pages.travel-return.progress.submit',
        titleVisible: true,
      },
    },
  }
}

export default TravelReturnForm
