import { FIELD_KEY, FieldMap } from './fields'
import { STEP_ID } from './form'
import { Section } from '~/types/form/Section'
import { DTO } from '~/content/forms/travel-return/transformer'

export enum SECTION_KEY {
  'HEALTH_OFFICE' = 'health_office',
  'USER_DATA' = 'user_data',
  'TRAVEL' = 'travel',
  'SYMPTOMS' = 'symptoms',
  'TEST' = 'test',
  'PRECONDITIONS' = 'preconditions',
  'PERSONAL_NOTE' = 'personal_note',
  'REVIEW' = 'review',
  'SUBMIT' = 'submit',
  'RESULT' = 'result'
}

export enum PROGRESS_STEP_ID {
  'FETCH_HEALTH_OFFICE' = 'fetch_health_office',
  'USER_DATA' = 'user_data',
  'TRAVEL' = 'travel',
  'SYMPTOMS' = 'symptoms',
  'TEST' = 'test',
  'PRECONDITIONS' = 'preconditions',
  'PERSONAL_NOTE' = 'personal_note',
  'REVIEW' = 'review',
  'SUBMIT' = 'submit'
}

export const SectionMap: Record<SECTION_KEY, Section<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>> = {
  [SECTION_KEY.HEALTH_OFFICE]: {
    key: SECTION_KEY.HEALTH_OFFICE,
    progressKey: PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE,
    fields: [
      {
        type: 'title',
        content: 'pages.travel-return.health-office.title',
      },
      {
        type: 'paragraph',
        content: 'pages.travel-return.health-office.description',
      },
      FieldMap[FIELD_KEY.ZIP],
      {
        type: 'paragraph',
        content: 'pages.travel-return.health-office.note',
      },
      FieldMap[FIELD_KEY.FETCH_HEALTH_OFFICE],
    ],
  },
  [SECTION_KEY.USER_DATA]: {
    key: SECTION_KEY.USER_DATA,
    progressKey: PROGRESS_STEP_ID.USER_DATA,
    fields: [
      {
        type: 'title',
        content: 'pages.travel-return.user-data.title',
      },
      [
        FieldMap[FIELD_KEY.FIRST_NAME],
        FieldMap[FIELD_KEY.LAST_NAME],
      ],
      FieldMap[FIELD_KEY.BIRTHDAY],
      FieldMap[FIELD_KEY.GENDER],
      {
        type: 'title',
        content: 'pages.travel-return.contact.title',
      },
      FieldMap[FIELD_KEY.EMAIL],
      FieldMap[FIELD_KEY.PHONE],
      FieldMap[FIELD_KEY.STREET],
      [
        {
          ...FieldMap[FIELD_KEY.ZIP],
          options: {
            ...FieldMap[FIELD_KEY.ZIP].options,
            disabled: true,
          },
        },
        FieldMap[FIELD_KEY.CITY],
      ],
    ],
  },
  [SECTION_KEY.TRAVEL]: {
    key: SECTION_KEY.TRAVEL,
    progressKey: PROGRESS_STEP_ID.TRAVEL,
    fields: [
      {
        type: 'title',
        content: 'pages.travel-return.travel.title',
      },
      FieldMap[FIELD_KEY.TRAVEL_COUNTRY],
      FieldMap[FIELD_KEY.TRAVEL_REGION],
      FieldMap[FIELD_KEY.STAY_DURATION],
      FieldMap[FIELD_KEY.DATE_OF_ENTRY],
      FieldMap[FIELD_KEY.MODE_OF_ENTRY],
    ],
  },
  [SECTION_KEY.SYMPTOMS]: {
    key: SECTION_KEY.SYMPTOMS,
    progressKey: PROGRESS_STEP_ID.SYMPTOMS,
    fields: [
      {
        type: 'title',
        content: 'pages.travel-return.symptoms.title',
      },
      FieldMap[FIELD_KEY.HAS_SYMPTOMS],
      FieldMap[FIELD_KEY.SYMPTOMS],
      FieldMap[FIELD_KEY.BEGIN_OF_SYMPTOMS],
    ],
  },
  [SECTION_KEY.TEST]: {
    key: SECTION_KEY.TEST,
    progressKey: PROGRESS_STEP_ID.TEST,
    fields: [
      {
        type: 'title',
        content: 'pages.travel-return.test.title',
      },
      FieldMap[FIELD_KEY.HAS_TESTED],
      FieldMap[FIELD_KEY.DATE_OF_TEST],
      FieldMap[FIELD_KEY.TEST_COUNTRY],
      FieldMap[FIELD_KEY.TEST_RESULT],
      {
        type: 'paragraph',
        content: 'pages.travel-return.test.note_attachment',
        color: 'primary',
        hidden: (store) => {
          return store.data[FIELD_KEY.HAS_TESTED] !== 'yes'
        },
      },
    ],
  },
  [SECTION_KEY.PRECONDITIONS]: {
    key: SECTION_KEY.PRECONDITIONS,
    progressKey: PROGRESS_STEP_ID.PRECONDITIONS,
    fields: [
      {
        type: 'title',
        content: 'pages.travel-return.preconditions.title',
      },
      FieldMap[FIELD_KEY.IMMUNE_DISEASE],
      FieldMap[FIELD_KEY.HEART_DISEASE],
      FieldMap[FIELD_KEY.DISEASE_EXPLANATION],
      FieldMap[FIELD_KEY.PREGNANCY],
      FieldMap[FIELD_KEY.PREGNANCY_WEEK],
    ],
  },
  [SECTION_KEY.PERSONAL_NOTE]: {
    key: SECTION_KEY.PERSONAL_NOTE,
    progressKey: PROGRESS_STEP_ID.PERSONAL_NOTE,
    fields: [
      {
        type: 'title',
        content: 'pages.travel-return.personal-note.title',
      },
      FieldMap[FIELD_KEY.PERSONAL_NOTE],
    ],
  },
  [SECTION_KEY.REVIEW]: {
    key: SECTION_KEY.REVIEW,
    progressKey: PROGRESS_STEP_ID.REVIEW,
    fields: [
      {
        type: 'title',
        content: 'pages.travel-return.form-reviewer.heading',
      },
      {
        type: 'paragraph',
        content: 'pages.travel-return.form-reviewer.description',
      },
      FieldMap[FIELD_KEY.FORM_REVIEW],
      FieldMap[FIELD_KEY.CONSENT_GIVEN],
    ],
  },
  [SECTION_KEY.SUBMIT]: {
    key: SECTION_KEY.SUBMIT,
    progressKey: PROGRESS_STEP_ID.SUBMIT,
    fields: [
      FieldMap[FIELD_KEY.FORM_SUBMIT],
    ],
  },
  [SECTION_KEY.RESULT]: {
    key: SECTION_KEY.RESULT,
    progressKey: PROGRESS_STEP_ID.SUBMIT,
    fields: [
      {
        type: 'title',
        content: 'pages.travel-return.result-display.heading',
      },
      {
        type: 'paragraph',
        content: 'pages.travel-return.result-display.description1',
      },
      {
        type: 'paragraph',
        content: 'pages.travel-return.result-display.description2',
        color: 'primary',
        hidden: (store) => {
          return store.data[FIELD_KEY.HAS_TESTED] !== 'yes'
        },
      },
      {
        type: 'paragraph',
        content: 'pages.travel-return.result-display.description3',
      },
      FieldMap[FIELD_KEY.EMAIL_DISPLAY],
    ],
  },
}
