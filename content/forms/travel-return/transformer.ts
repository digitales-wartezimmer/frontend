import { STEP_ID } from './form'
import { FIELD_KEY } from './fields'
import { Transformer as BaseTransformer } from '~/types/form/Transformer'
import { Store } from '~/types/form/Store'

export interface DTO {
  contact: {
    firstName: string
    lastName: string
    birthday: string
    gender: string
    address: {
      street: string
      zip: string
      city: string
    },
    email?: string
    phone: string
  },
  travel: {
    country: string
    region: string
    duration: number
    dateOfEntry: string
    modeOfEntry: Array<string>
  },
  test: {
    hasTested: boolean
    date?: string
    country?: string
    result?: string
  },
  condition: {
    hasSymptoms: boolean
    symptoms?: string
    beginOfSymptoms?: string
    preconditions: {
      immuneDisease: boolean
      heartDisease: boolean
      diseaseExplanation?: string
    },
    pregnancy: boolean
    pregnancyWeek?: number
    personalNote?: string
  }
}

export class Transformer extends BaseTransformer<FIELD_KEY, STEP_ID, DTO> {
  getData (store: Store<FIELD_KEY, STEP_ID>): DTO {
    const email = store.data[FIELD_KEY.EMAIL]
    return {
      contact: {
        firstName: store.data[FIELD_KEY.FIRST_NAME],
        lastName: store.data[FIELD_KEY.LAST_NAME],
        birthday: this.dateToISOString(store.data[FIELD_KEY.BIRTHDAY])!,
        gender: store.data[FIELD_KEY.GENDER] || 'D',
        address: {
          street: store.data[FIELD_KEY.STREET],
          zip: store.data[FIELD_KEY.ZIP],
          city: store.data[FIELD_KEY.CITY],
        },
        email: (email && email.trim().length > 0) ? email : undefined,
        phone: this.transformPhoneNumber(store.data[FIELD_KEY.PHONE]),
      },
      travel: {
        country: store.data[FIELD_KEY.TRAVEL_COUNTRY],
        region: store.data[FIELD_KEY.TRAVEL_REGION],
        duration: parseInt(store.data[FIELD_KEY.STAY_DURATION], 10),
        dateOfEntry: this.dateToISOString(store.data[FIELD_KEY.DATE_OF_ENTRY])!,
        modeOfEntry: store.data[FIELD_KEY.MODE_OF_ENTRY],
      },
      test: {
        hasTested: this.confirmRadioToBoolean(store.data[FIELD_KEY.HAS_TESTED]),
        date: this.dateToISOString(store.data[FIELD_KEY.DATE_OF_TEST]),
        country: store.data[FIELD_KEY.TEST_COUNTRY],
        result: store.data[FIELD_KEY.TEST_RESULT],
      },
      condition: {
        hasSymptoms: this.confirmRadioToBoolean(store.data[FIELD_KEY.HAS_SYMPTOMS]),
        symptoms: store.data[FIELD_KEY.SYMPTOMS],
        beginOfSymptoms: this.dateToISOString(store.data[FIELD_KEY.BEGIN_OF_SYMPTOMS])!,
        preconditions: {
          immuneDisease: this.confirmRadioToBoolean(store.data[FIELD_KEY.IMMUNE_DISEASE]),
          heartDisease: this.confirmRadioToBoolean(store.data[FIELD_KEY.HEART_DISEASE]),
          diseaseExplanation: store.data[FIELD_KEY.DISEASE_EXPLANATION],
        },
        pregnancy: this.confirmRadioToBoolean(store.data[FIELD_KEY.PREGNANCY]),
        pregnancyWeek: parseInt(store.data[FIELD_KEY.PREGNANCY_WEEK], 10),
        personalNote: store.data[FIELD_KEY.PERSONAL_NOTE],
      },
    }
  }
}
