export const formIDs = ['formA', 'formB']
export const fields = {
  changeForm: {
    key: 'changeForm',
    changeForm: true,
    form: 'otherForm',
    previous: 'stepA',
  },
  changeFormWithFn: {
    key: 'changeFormWithFn',
    changeForm: true,
    form: () => 'anotherForm',
    previous: 'stepA',
  },
  changeFormWithoutForm: {
    key: 'changeFormWithoutForm',
    changeForm: true,
    previous: 'stepA',
  },
  changeFormBack: {
    key: 'changeFormBack',
    fields: [{
      key: 'aSingleField',
      alias: 'a-single-field',
      type: 'text',
    }],
    start: true,
  },
  singleField: {
    key: 'singleField',
    section: {
      fields: [{
        key: 'aSingleField',
        alias: 'a-single-field',
        type: 'text',
      }],
    },
  },
  sectionMoreFields: {
    key: 'sectionMoreFields',
    section: {
      fields: [{
        key: 'aSingleField',
        alias: 'a-single-field',
        type: 'text',
      }, {
        key: 'anotherSingleField',
        alias: 'another-single-field',
        type: 'text',
      }],
    },
  },
  hiddenField: {
    key: 'hiddenField',
    section: {
      fields: [{
        key: 'hiddenField',
        alias: 'a-hidden-field',
        type: 'text',
        hidden: true,
      }],
    },
  },
  hiddenWithFnField: {
    key: 'hiddenWithFnField',
    section: {
      fields: [{
        key: 'hiddenField',
        alias: 'a-hidden-field',
        type: 'text',
        hidden: () => true,
      }],
    },
  },
  multiField: {
    key: 'multiField',
    section: {
      fields: [[{
        key: 'aMultiField',
        alias: 'a-multi-field',
        type: 'text',
      }, {
        key: 'bMultiField',
        alias: 'a-multi-field',
        type: 'text',
      }]],
    },
  },
  fetchHealthOffice: {
    key: 'fetchHealthOffice',
    section: {
      fields: [{
        key: 'fetchHealthOffice',
        alias: 'fetch-health-office',
        type: 'component',
        componentName: 'fetch-health-office',
      }],
    },
  },
  formReviewer: {
    key: 'formReviewer',
    section: {
      fields: [{
        key: 'formReviewer',
        alias: 'form-reviewer',
        type: 'component',
        componentName: 'form-reviewer',
      }],
    },
  },
  formSubmitter: {
    key: 'formSubmitter',
    section: {
      fields: [{
        key: 'formSubmitter',
        alias: 'form-submitter',
        type: 'component',
        componentName: 'form-submitter',
        apiUrl: '/test/endpoint',
        transformer: {
          getRequestData: () => ({ foo: 'bar' }),
        },
      }],
    },
  },
  emailDisplay: {
    key: 'emailDisplay',
    section: {
      fields: [{
        key: 'emailDisplay',
        alias: 'email-display',
        type: 'component',
        componentName: 'email-display',
      }],
    },
  },
  success: {
    key: 'success',
    section: {
      fields: [{
        key: 'success',
        alias: 'success',
        type: 'component',
        componentName: 'success',
      }],
    },
  },
  startOverButton: {
    key: 'startOverButton',
    section: {
      fields: [{
        key: 'startOverButton',
        alias: 'start-over-button',
        type: 'component',
        componentName: 'start-over-button',
      }],
    },
  },
  quarantineDate: {
    key: 'quarantineDate',
    section: {
      fields: [{
        key: 'quarantineDate',
        alias: 'quarantine-date',
        type: 'component',
        componentName: 'quarantine-date',
      }],
    },
  },
  loading: {
    key: 'loading',
    section: {
      fields: [{
        key: 'loading',
        alias: 'loading',
        type: 'component',
        componentName: 'loading',
      }],
    },
  },
  fieldWithOnChange: {
    key: 'fieldWithOnChange',
    section: {
      fields: [{
        key: 'aSingleFieldWithOnChange',
        alias: 'a-single-field-with-on-change',
        type: 'text',
        onChange (val: string) {
          // @ts-ignore
          this.$store.commit('test', val)
        },
      }],
    },
  },
  fieldWithNextPrevious: {
    key: 'fieldWithNextPrevious',
    section: {
      fields: [{
        key: 'fieldWithnextPrevious',
        alias: 'field',
        hidden: true,
        type: 'text',
      }],
    },
    next: 'NEXT',
    previous: 'PREV',
  },
  fieldWithNextPreviousFn: {
    key: 'fieldWithNextPreviousFn',
    section: {
      fields: [{
        key: 'fieldWithNextPreviousFn',
        alias: 'field',
        hidden: true,
        type: 'text',
      }],
    },
    next: () => 'NEXT',
    previous: () => 'PREV',
  },
  fieldWithDisabledNextPrevious: {
    key: 'fieldWithDisabledNextPrevious',
    section: {
      fields: [{
        key: 'fieldWithDisabledNextPrevious',
        alias: 'field',
        hidden: true,
        type: 'text',
      }],
    },
    noNextButton: true,
    noBackButton: true,
  },
  fieldWithEmptyNextPrevious: {
    key: 'fieldWithEmptyNextPrevious',
    section: {
      fields: [{
        key: 'fieldWithEmptyNextPrevious',
        alias: 'field',
        hidden: true,
        type: 'text',
      }],
    },
    next: '',
    previous: '',
  },
}

export const formMap = {
  formA: {
    form: {
      id: 'formA',
      storeKey: 'formAStore',
      steps: {
        A: {
          key: 'A',
          previous: 'previousA',
          next: 'previousB',
        },
        ...fields,
      },
    },
  },
  formB: {
    form: {
      id: 'formB',
      storeKey: 'formBStore',
      steps: {
        B: {
          key: 'B',
        },
      },
    },
  },
}
