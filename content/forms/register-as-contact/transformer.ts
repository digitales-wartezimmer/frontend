import dayjs from 'dayjs'
import { Transformer as BaseTransformer } from '~/types/form/Transformer'
import { FIELD_KEY } from '~/content/forms/register-as-contact/fields'
import { STEP_ID } from '~/content/forms/register-as-contact/form'
import { Store } from '~/types/form/Store'

export interface DTO {
  contact: {
    firstName: string
    lastName: string
    birthday: string
    gender: string
    address: {
      street: string
      zip: string
      city: string
    },
    email?: string
    phone: string
  },
  infection: {
    sourceOfInfection: string
    contactData?: {
      firstName?: string
      lastName?: string
      phone?: string
      email?: string
    },
    locality?: string
    lastContact?: string
    countRiskEncounters: string
    dateRiskEncounter: string
    category: number
    reason: string
    hasSymptoms: boolean
    symptoms: string
    beginOfSymptoms: string
    preconditions: {
      immuneDisease: boolean,
      heartDisease: boolean,
      diseaseExplanation?: string
    },
    pregnancy: boolean,
    pregnancyWeek?: number,
    personalNote?: string
  }
}

export class Transformer extends BaseTransformer<FIELD_KEY, STEP_ID, DTO> {
  getData (store: Store<FIELD_KEY, STEP_ID>): DTO {
    const email = store.data[FIELD_KEY.EMAIL]
    return {
      contact: {
        firstName: store.data[FIELD_KEY.FIRST_NAME],
        lastName: store.data[FIELD_KEY.LAST_NAME],
        birthday: this.dateToISOString(store.data[FIELD_KEY.BIRTHDAY])!,
        gender: store.data[FIELD_KEY.GENDER] || 'D',
        address: {
          street: store.data[FIELD_KEY.STREET],
          zip: store.data[FIELD_KEY.ZIP],
          city: store.data[FIELD_KEY.CITY],
        },
        email: (email && email.trim().length > 0) ? email : undefined,
        phone: this.transformPhoneNumber(store.data[FIELD_KEY.PHONE]),
      },
      infection: {
        sourceOfInfection: store.data[FIELD_KEY.SOURCE_OF_INFECTION],
        contactData: {
          firstName: store.data[FIELD_KEY.CONTACT_FIRST_NAME],
          lastName: store.data[FIELD_KEY.CONTACT_LAST_NAME],
          phone: (store.data[FIELD_KEY.CONTACT_PHONE] && store.data[FIELD_KEY.CONTACT_PHONE].trim().length > 0) ? this.transformPhoneNumber(store.data[FIELD_KEY.CONTACT_PHONE]) : undefined,
          email: store.data[FIELD_KEY.CONTACT_EMAIL],
        },
        locality: store.data[FIELD_KEY.LOCALITY],
        lastContact: this.dateToISOString(store.data[FIELD_KEY.LAST_CONTACT])!,
        countRiskEncounters: store.data[FIELD_KEY.COUNT_RISK_ENCOUNTERS],
        dateRiskEncounter: this.dateToISOString(dayjs().subtract(parseInt(store.data[FIELD_KEY.DAYS_SINCE_RISK_ENCOUNTER], 10), 'day'))!,
        category: ['first', 'second', 'third'].indexOf(store.data[FIELD_KEY.CATEGORY]) + 1,
        reason: store.data[FIELD_KEY.REASON],
        hasSymptoms: this.confirmRadioToBoolean(store.data[FIELD_KEY.HAS_SYMPTOMS]),
        symptoms: store.data[FIELD_KEY.SYMPTOMS],
        beginOfSymptoms: this.dateToISOString(store.data[FIELD_KEY.BEGIN_OF_SYMPTOMS])!,
        preconditions: {
          immuneDisease: this.confirmRadioToBoolean(store.data[FIELD_KEY.IMMUNE_DISEASE]),
          heartDisease: this.confirmRadioToBoolean(store.data[FIELD_KEY.HEART_DISEASE]),
          diseaseExplanation: store.data[FIELD_KEY.DISEASE_EXPLANATION],
        },
        pregnancy: this.confirmRadioToBoolean(store.data[FIELD_KEY.PREGNANCY]),
        pregnancyWeek: parseInt(store.data[FIELD_KEY.PREGNANCY_WEEK], 10),
        personalNote: store.data[FIELD_KEY.PERSONAL_NOTE],
      },
    }
  }
}
