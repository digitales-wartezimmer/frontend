import { PROGRESS_STEP_ID, SECTION_KEY, SectionMap } from './sections'
import { DTO } from './transformer'

import { FIELD_KEY } from './fields'
import AbstractForm, { Step as AbstractStep } from '~/types/form/Form'
import { Section as AbstractSection } from '~/types/form/Section'
import { Field as AbstractField } from '~/types/form/Field'

export enum STEP_ID {
  'FETCH_HEALTH_OFFICE' = 'fetch_health_office',
  'USER_DATA' = 'user_data',
  'SOURCE_OF_INFECTION' = 'source_of_infection',
  'INFECTION_PERSONAL' = 'infection_personal',
  'INFECTION_CWA' = 'infection_cwa',
  'INFECTION_ESTABLISHMENT' = 'infection_establishment',
  'CONTACT_PERSONAL' = 'contact_personal',
  'SYMPTOMS' = 'symptoms',
  'PRECONDITIONS' = 'preconditions',
  'PERSONAL_NOTE' = 'personal_note',
  'REVIEW' = 'review',
  'SUBMIT' = 'submit',
  'RESULT' = 'result'
}
export { SECTION_KEY as SECTION_KEYS }
export { PROGRESS_STEP_ID as PROGRESS_STEP_IDS }
export { FIELD_KEY as FIELD_KEYS }
export { STEP_ID as STEP_IDS }

export namespace RegisterAsContactForm {
  export const ID = 'registration-as-contact'
  export type ID = 'registration-as-contact'
  export type Form = AbstractForm<'registration-as-contact', STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Step = AbstractStep<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Section = AbstractSection<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Field = AbstractField<STEP_ID, FIELD_KEY, DTO>

  export const form: RegisterAsContactForm.Form = {
    id: RegisterAsContactForm.ID,
    storeKey: 'registrationAsContact',
    steps: {
      [STEP_ID.FETCH_HEALTH_OFFICE]: {
        key: STEP_ID.FETCH_HEALTH_OFFICE,
        section: SectionMap[SECTION_KEY.HEALTH_OFFICE],
        start: true,
        next: STEP_ID.USER_DATA,
      },
      [STEP_ID.USER_DATA]: {
        key: STEP_ID.USER_DATA,
        section: SectionMap[SECTION_KEY.USER_DATA],
        next: STEP_ID.SOURCE_OF_INFECTION,
        previous: STEP_ID.FETCH_HEALTH_OFFICE,
      },
      [STEP_ID.SOURCE_OF_INFECTION]: {
        key: STEP_ID.SOURCE_OF_INFECTION,
        section: SectionMap[SECTION_KEY.SOURCE_OF_INFECTION],
        next: (store) => {
          switch (store.data[FIELD_KEY.SOURCE_OF_INFECTION]) {
            case 'personal':
              return STEP_ID.INFECTION_PERSONAL
            case 'cwa':
              return STEP_ID.INFECTION_CWA
            case 'establishment':
              return STEP_ID.INFECTION_ESTABLISHMENT
            default:
              return STEP_ID.SYMPTOMS
          }
        },
        previous: STEP_ID.USER_DATA,
      },
      [STEP_ID.INFECTION_PERSONAL]: {
        key: STEP_ID.INFECTION_PERSONAL,
        section: SectionMap[SECTION_KEY.INFECTION_PERSONAL],
        next: STEP_ID.CONTACT_PERSONAL,
        previous: STEP_ID.SOURCE_OF_INFECTION,
      },
      [STEP_ID.INFECTION_CWA]: {
        key: STEP_ID.INFECTION_CWA,
        section: SectionMap[SECTION_KEY.INFECTION_CWA],
        next: STEP_ID.SYMPTOMS,
        previous: STEP_ID.SOURCE_OF_INFECTION,
      },
      [STEP_ID.INFECTION_ESTABLISHMENT]: {
        key: STEP_ID.INFECTION_ESTABLISHMENT,
        section: SectionMap[SECTION_KEY.INFECTION_ESTABLISHMENT],
        next: STEP_ID.SYMPTOMS,
        previous: STEP_ID.SOURCE_OF_INFECTION,
      },
      [STEP_ID.CONTACT_PERSONAL]: {
        key: STEP_ID.CONTACT_PERSONAL,
        section: SectionMap[SECTION_KEY.CONTACT_PERSONAL],
        next: STEP_ID.SYMPTOMS,
        previous: STEP_ID.INFECTION_PERSONAL,
      },
      [STEP_ID.SYMPTOMS]: {
        key: STEP_ID.SYMPTOMS,
        section: SectionMap[SECTION_KEY.SYMPTOMS],
        next: STEP_ID.PRECONDITIONS,
        previous: (store) => {
          switch (store.data[FIELD_KEY.SOURCE_OF_INFECTION]) {
            case 'personal':
              return STEP_ID.CONTACT_PERSONAL
            case 'cwa':
              return STEP_ID.INFECTION_CWA
            case 'establishment':
              return STEP_ID.INFECTION_ESTABLISHMENT
            default:
              return STEP_ID.SOURCE_OF_INFECTION
          }
        },
      },
      [STEP_ID.PRECONDITIONS]: {
        key: STEP_ID.PRECONDITIONS,
        section: SectionMap[SECTION_KEY.PRECONDITIONS],
        next: STEP_ID.PERSONAL_NOTE,
        previous: STEP_ID.SYMPTOMS,
      },
      [STEP_ID.PERSONAL_NOTE]: {
        key: STEP_ID.PERSONAL_NOTE,
        section: SectionMap[SECTION_KEY.PERSONAL_NOTE],
        next: STEP_ID.REVIEW,
        previous: STEP_ID.PRECONDITIONS,
      },
      [STEP_ID.REVIEW]: {
        key: STEP_ID.REVIEW,
        section: SectionMap[SECTION_KEY.REVIEW],
        next: STEP_ID.SUBMIT,
        previous: STEP_ID.PERSONAL_NOTE,
        nextButtonText: 'pages.registration-as-contact.form-reviewer.complete',
      },
      [STEP_ID.SUBMIT]: {
        key: STEP_ID.SUBMIT,
        section: SectionMap[SECTION_KEY.SUBMIT],
        final: false,
        noBackButton: true,
        noNextButton: true,
        next: STEP_ID.RESULT,
        previous: STEP_ID.REVIEW,
      },
      [STEP_ID.RESULT]: {
        key: STEP_ID.RESULT,
        section: SectionMap[SECTION_KEY.RESULT],
        final: true,
        noBackButton: true,
        previous: STEP_ID.REVIEW,
      },
    },
    progress: {
      [PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE]: {
        key: PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE,
        title: 'pages.registration-as-contact.progress.health_office',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.USER_DATA]: {
        key: PROGRESS_STEP_ID.USER_DATA,
        title: 'pages.registration-as-contact.progress.user_data',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.SOURCE_OF_INFECTION]: {
        key: PROGRESS_STEP_ID.SOURCE_OF_INFECTION,
        title: 'pages.registration-as-contact.progress.source_of_infection',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.INFECTION_DATA]: {
        key: PROGRESS_STEP_ID.INFECTION_DATA,
        title: 'pages.registration-as-contact.progress.infection_data',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.CONTACT_DATA]: {
        key: PROGRESS_STEP_ID.CONTACT_DATA,
        title: 'pages.registration-as-contact.progress.personal_contact',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.SYMPTOMS]: {
        key: PROGRESS_STEP_ID.SYMPTOMS,
        title: 'pages.registration-as-contact.progress.symptoms',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.PRECONDITIONS]: {
        key: PROGRESS_STEP_ID.PRECONDITIONS,
        title: 'pages.registration-as-contact.progress.preconditions',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.PERSONAL_NOTE]: {
        key: PROGRESS_STEP_ID.PERSONAL_NOTE,
        title: 'pages.registration-as-contact.progress.personal_note',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.REVIEW]: {
        key: PROGRESS_STEP_ID.REVIEW,
        title: 'pages.registration-as-contact.progress.review',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.SUBMIT]: {
        key: PROGRESS_STEP_ID.SUBMIT,
        title: 'pages.registration-as-contact.progress.submit',
        titleVisible: true,
      },
    },
  }
}

export default RegisterAsContactForm
