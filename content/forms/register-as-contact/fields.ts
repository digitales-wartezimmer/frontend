import { STEP_ID } from './form'
import { DTO, Transformer } from './transformer'
import { Field } from '~/types/form/Field'
import { storeZipAndRetrieveCity } from '~/content/forms/shared'

export enum FIELD_KEY {
  'FIRST_NAME' = 'firstName',
  'LAST_NAME' = 'lastName',
  'ZIP' = 'zip',
  'FETCH_HEALTH_OFFICE' = 'fetchHealthOffice',
  'BIRTHDAY' = 'birthday',
  'GENDER' = 'gender',
  'STREET' = 'street',
  'CITY' = 'city',
  'EMAIL' = 'email',
  'PHONE' = 'phone',
  'SOURCE_OF_INFECTION' = 'sourceOfInfection',
  'CONTACT_FIRST_NAME' = 'contactFirstName',
  'CONTACT_LAST_NAME' = 'contactLastName',
  'CONTACT_PHONE' = 'contactPhone',
  'CONTACT_EMAIL' = 'contactEmail',
  'LOCALITY' = 'locality',
  'LAST_CONTACT' = 'lastContact',
  'COUNT_RISK_ENCOUNTERS' = 'countRiskEncounters',
  'DAYS_SINCE_RISK_ENCOUNTER' = 'daysSinceRiskEncounter',
  'CATEGORY' = 'category',
  'REASON' = 'reason',
  'HAS_SYMPTOMS' = 'hasSymptoms',
  'SYMPTOMS' = 'symptoms',
  'BEGIN_OF_SYMPTOMS' = 'beginOfSymptoms',
  'IMMUNE_DISEASE' = 'immuneDisease',
  'HEART_DISEASE' = 'heartDisease',
  'DISEASE_EXPLANATION' = 'diseaseExplanation',
  'PREGNANCY' = 'pregnancy',
  'PREGNANCY_WEEK' = 'pregnancyWeek',
  'PERSONAL_NOTE' = 'personalNote',
  'CONSENT_GIVEN' = 'consentGiven',
  'FORM_REVIEW' = 'formReview',
  'FORM_SUBMIT' = 'formSubmit',
  'EMAIL_DISPLAY' = 'emailDisplay'
}

export const FieldMap: Record<FIELD_KEY, Field<STEP_ID, FIELD_KEY, DTO>> = {
  [FIELD_KEY.FIRST_NAME]: {
    type: 'text',
    key: FIELD_KEY.FIRST_NAME,
    alias: 'first-name',
    rules: 'required|max:64',
  },
  [FIELD_KEY.LAST_NAME]: {
    type: 'text',
    key: FIELD_KEY.LAST_NAME,
    alias: 'last-name',
    rules: 'required|max:64',
  },
  [FIELD_KEY.ZIP]: {
    type: 'text',
    key: FIELD_KEY.ZIP,
    alias: 'zip',
    rules: 'required|min:5|max:5|numeric|regex:[0-9]{5}',
    onChange: storeZipAndRetrieveCity,
  },
  [FIELD_KEY.FETCH_HEALTH_OFFICE]: {
    type: 'component',
    key: FIELD_KEY.FETCH_HEALTH_OFFICE,
    alias: 'fetch-health-office',
    componentName: 'fetch-health-office',
  },
  [FIELD_KEY.BIRTHDAY]: {
    type: 'date',
    key: FIELD_KEY.BIRTHDAY,
    alias: 'birthday',
    rules: 'required|date|not_future|alive',
    maxDate: 'today',
    isBirthdayPicker: true,
  },
  [FIELD_KEY.GENDER]: {
    type: 'switch',
    key: FIELD_KEY.GENDER,
    alias: 'gender',
    rules: 'required',
    choices: [{
      value: 'M',
      alias: 'male',
      icon: 'mars',
    },
    {
      value: 'F',
      alias: 'female',
      icon: 'venus',
    },
    {
      value: 'D',
      alias: 'diverse',
      icon: 'transgender-alt',
    }],
  },
  [FIELD_KEY.STREET]: {
    type: 'text',
    key: FIELD_KEY.STREET,
    alias: 'street',
    rules: 'required|max:64',
  },
  [FIELD_KEY.CITY]: {
    type: 'text',
    key: FIELD_KEY.CITY,
    alias: 'city',
    rules: 'required|max:64',
  },
  [FIELD_KEY.EMAIL]: {
    type: 'text',
    key: FIELD_KEY.EMAIL,
    alias: 'email',
    rules: 'email|max:64',
  },
  [FIELD_KEY.PHONE]: {
    type: 'text',
    key: FIELD_KEY.PHONE,
    alias: 'phone',
    rules: 'required|max:25|phone',
  },
  [FIELD_KEY.SOURCE_OF_INFECTION]: {
    type: 'radio',
    key: FIELD_KEY.SOURCE_OF_INFECTION,
    alias: 'source-of-infection',
    rules: 'required',
    choices: [{
      value: 'personal',
      alias: 'personal',
    },
    {
      value: 'cwa',
      alias: 'cwa',
    },
    {
      value: 'establishment',
      alias: 'establishment',
    }],
  },
  [FIELD_KEY.CONTACT_FIRST_NAME]: {
    type: 'text',
    key: FIELD_KEY.CONTACT_FIRST_NAME,
    alias: 'contact-first-name',
    rules: 'max:64',
    hidden: (store) => {
      return store.data[FIELD_KEY.SOURCE_OF_INFECTION] !== 'personal'
    },
  },
  [FIELD_KEY.CONTACT_LAST_NAME]: {
    type: 'text',
    key: FIELD_KEY.CONTACT_LAST_NAME,
    alias: 'contact-last-name',
    rules: 'max:64',
    hidden: (store) => {
      return store.data[FIELD_KEY.SOURCE_OF_INFECTION] !== 'personal'
    },
  },
  [FIELD_KEY.CONTACT_PHONE]: {
    type: 'text',
    key: FIELD_KEY.CONTACT_PHONE,
    alias: 'contact-phone',
    rules: 'max:25|phone',
    hidden: (store) => {
      return store.data[FIELD_KEY.SOURCE_OF_INFECTION] !== 'personal'
    },
  },
  [FIELD_KEY.CONTACT_EMAIL]: {
    type: 'text',
    key: FIELD_KEY.CONTACT_EMAIL,
    alias: 'contact-email',
    rules: 'email|max:64',
    hidden: (store) => {
      return store.data[FIELD_KEY.SOURCE_OF_INFECTION] !== 'personal'
    },
  },
  [FIELD_KEY.LOCALITY]: {
    type: 'text',
    key: FIELD_KEY.LOCALITY,
    alias: 'locality',
    rules: 'max:200',
    hidden: (store) => {
      return !['personal', 'establishment'].includes(store.data[FIELD_KEY.SOURCE_OF_INFECTION])
    },
  },
  [FIELD_KEY.LAST_CONTACT]: {
    type: 'date',
    key: FIELD_KEY.LAST_CONTACT,
    alias: 'last-contact',
    rules: 'required|date|not_future|during_corona',
    maxDate: 'today',
    hidden: (store) => {
      return !['personal', 'establishment'].includes(store.data[FIELD_KEY.SOURCE_OF_INFECTION])
    },
  },
  [FIELD_KEY.COUNT_RISK_ENCOUNTERS]: {
    type: 'text',
    key: FIELD_KEY.COUNT_RISK_ENCOUNTERS,
    alias: 'count-risk-encounters',
    rules: 'required|numeric|min_value:1|max:2',
    hidden: (store) => {
      return store.data[FIELD_KEY.SOURCE_OF_INFECTION] !== 'cwa'
    },
  },
  [FIELD_KEY.DAYS_SINCE_RISK_ENCOUNTER]: {
    type: 'text',
    key: FIELD_KEY.DAYS_SINCE_RISK_ENCOUNTER,
    alias: 'days-since-risk-encounter',
    rules: 'required|numeric|min_value:0|max:2',
    hidden: (store) => {
      return store.data[FIELD_KEY.SOURCE_OF_INFECTION] !== 'cwa'
    },
  },
  [FIELD_KEY.CATEGORY]: {
    type: 'radio',
    key: FIELD_KEY.CATEGORY,
    alias: 'category',
    rules: 'required',
    choices: [
      {
        value: 'first',
        alias: 'first',
      },
      {
        value: 'second',
        alias: 'second',
      },
      {
        value: 'third',
        alias: 'third',
      },
    ],
    hidden: (store) => {
      return store.data[FIELD_KEY.SOURCE_OF_INFECTION] !== 'personal'
    },
  },
  [FIELD_KEY.REASON]: {
    type: 'textarea',
    key: FIELD_KEY.REASON,
    alias: 'reason',
    rules: 'required|max:500',
    hidden: (store) => {
      return store.data[FIELD_KEY.SOURCE_OF_INFECTION] !== 'personal'
    },
  },
  [FIELD_KEY.HAS_SYMPTOMS]: {
    type: 'radio',
    key: FIELD_KEY.HAS_SYMPTOMS,
    alias: 'has-symptoms',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }, {
      alias: 'no',
      value: 'no',
    }],
  },
  [FIELD_KEY.SYMPTOMS]: {
    type: 'textarea',
    key: FIELD_KEY.SYMPTOMS,
    alias: 'symptoms',
    rules: 'required|max:500',
    hidden: (store) => {
      return store.data[FIELD_KEY.HAS_SYMPTOMS] !== 'yes'
    },
  },
  [FIELD_KEY.BEGIN_OF_SYMPTOMS]: {
    type: 'date',
    key: FIELD_KEY.BEGIN_OF_SYMPTOMS,
    alias: 'begin-of-symptoms',
    rules: 'required|date|during_corona|not_future',
    maxDate: 'today',
    hidden: (store) => {
      return store.data[FIELD_KEY.HAS_SYMPTOMS] !== 'yes'
    },
  },
  [FIELD_KEY.IMMUNE_DISEASE]: {
    type: 'radio',
    key: FIELD_KEY.IMMUNE_DISEASE,
    alias: 'immune-disease',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }, {
      alias: 'no',
      value: 'no',
    }],
  },
  [FIELD_KEY.HEART_DISEASE]: {
    type: 'radio',
    key: FIELD_KEY.HEART_DISEASE,
    alias: 'heart-disease',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }, {
      alias: 'no',
      value: 'no',
    }],
  },
  [FIELD_KEY.DISEASE_EXPLANATION]: {
    type: 'text',
    key: FIELD_KEY.DISEASE_EXPLANATION,
    alias: 'disease-explanation',
    rules: 'required|max:200',
    hidden: (store) => {
      return store.data[FIELD_KEY.IMMUNE_DISEASE] !== 'yes' && store.data[FIELD_KEY.HEART_DISEASE] !== 'yes'
    },
  },
  [FIELD_KEY.PREGNANCY]: {
    type: 'radio',
    key: FIELD_KEY.PREGNANCY,
    alias: 'pregnancy',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }, {
      alias: 'no',
      value: 'no',
    }],
    hidden: (store) => {
      return !['D', 'F'].includes(store.data[FIELD_KEY.GENDER])
    },
  },
  [FIELD_KEY.PREGNANCY_WEEK]: {
    type: 'text',
    key: FIELD_KEY.PREGNANCY_WEEK,
    alias: 'pregnancy-week',
    rules: 'required|numeric|min_value:0|max:2',
    hidden: (store) => {
      return store.data[FIELD_KEY.PREGNANCY] !== 'yes'
    },
  },
  [FIELD_KEY.PERSONAL_NOTE]: {
    type: 'textarea',
    key: FIELD_KEY.PERSONAL_NOTE,
    alias: 'personal-note',
    rules: 'max:500',
  },
  [FIELD_KEY.CONSENT_GIVEN]: {
    type: 'checkbox',
    key: FIELD_KEY.CONSENT_GIVEN,
    alias: 'consent-given',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }],
    hiddenInReview: true,
  },
  [FIELD_KEY.FORM_REVIEW]: {
    type: 'component',
    key: FIELD_KEY.FORM_REVIEW,
    alias: 'form-review',
    componentName: 'form-reviewer',
  },
  [FIELD_KEY.FORM_SUBMIT]: {
    type: 'component',
    key: FIELD_KEY.FORM_SUBMIT,
    alias: 'form-submit',
    componentName: 'form-submitter',
    apiUrl: '/api/patientSubmission',
    transformer: new Transformer(),
    onChange (newValue) {
      if (!newValue) {
        return
      }
      (this as any).$store.commit((this as any).form.storeKey + '/updateField', {
        path: 'responseData',
        value: newValue,
      })
    },
  },
  [FIELD_KEY.EMAIL_DISPLAY]: {
    type: 'component',
    key: FIELD_KEY.EMAIL_DISPLAY,
    alias: 'email-display',
    componentName: 'email-display',
  },
}
