import { Section } from '~/types/form/Section'
import { FIELD_KEY, FieldMap } from '~/content/forms/register-as-contact/fields'
import { STEP_ID } from '~/content/forms/register-as-contact/form'
import { DTO } from '~/content/forms/register-as-contact/transformer'

export enum SECTION_KEY {
  'HEALTH_OFFICE' = 'health_office',
  'USER_DATA' = 'user_data',
  'SOURCE_OF_INFECTION' = 'source_of_infection',
  'INFECTION_PERSONAL' = 'infection_personal',
  'INFECTION_CWA' = 'infection_cwa',
  'INFECTION_ESTABLISHMENT' = 'infection_establishment',
  'CONTACT_PERSONAL' = 'contact_personal',
  'SYMPTOMS' = 'symptoms',
  'PRECONDITIONS' = 'preconditions',
  'PERSONAL_NOTE' = 'personal_note',
  'REVIEW' = 'review',
  'SUBMIT' = 'submit',
  'RESULT' = 'result'
}

export enum PROGRESS_STEP_ID {
  'FETCH_HEALTH_OFFICE' = 'fetch_health_office',
  'USER_DATA' = 'user_data',
  'SOURCE_OF_INFECTION' = 'source_of_infection',
  'INFECTION_DATA' = 'infection_data',
  'CONTACT_DATA' = 'contact_data',
  'SYMPTOMS' = 'symptoms',
  'PRECONDITIONS' = 'preconditions',
  'PERSONAL_NOTE' = 'personal_note',
  'REVIEW' = 'review',
  'SUBMIT' = 'submit'
}

export const SectionMap: Record<SECTION_KEY, Section<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>> = {
  [SECTION_KEY.HEALTH_OFFICE]: {
    key: SECTION_KEY.HEALTH_OFFICE,
    progressKey: PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE,
    fields: [
      {
        type: 'title',
        content: 'pages.registration-as-contact.health-office.title',
      },
      {
        type: 'paragraph',
        content: 'pages.travel-return.health-office.description',
      },
      FieldMap[FIELD_KEY.ZIP],
      {
        type: 'paragraph',
        content: 'pages.travel-return.health-office.note',
      },
      FieldMap[FIELD_KEY.FETCH_HEALTH_OFFICE],
    ],
  },
  [SECTION_KEY.USER_DATA]: {
    key: SECTION_KEY.USER_DATA,
    progressKey: PROGRESS_STEP_ID.USER_DATA,
    fields: [
      {
        type: 'title',
        content: 'pages.registration-as-contact.user-data.title',
      },
      [
        FieldMap[FIELD_KEY.FIRST_NAME],
        FieldMap[FIELD_KEY.LAST_NAME],
      ],
      FieldMap[FIELD_KEY.BIRTHDAY],
      FieldMap[FIELD_KEY.GENDER],
      {
        type: 'title',
        content: 'pages.registration-as-contact.contact.title',
      },
      FieldMap[FIELD_KEY.EMAIL],
      FieldMap[FIELD_KEY.PHONE],
      FieldMap[FIELD_KEY.STREET],
      [
        {
          ...FieldMap[FIELD_KEY.ZIP],
          options: {
            ...FieldMap[FIELD_KEY.ZIP].options,
            disabled: true,
          },
        },
        FieldMap[FIELD_KEY.CITY],
      ],
    ],
  },
  [SECTION_KEY.SOURCE_OF_INFECTION]: {
    key: SECTION_KEY.SOURCE_OF_INFECTION,
    progressKey: PROGRESS_STEP_ID.SOURCE_OF_INFECTION,
    fields: [
      FieldMap[FIELD_KEY.SOURCE_OF_INFECTION],
    ],
  },
  [SECTION_KEY.INFECTION_PERSONAL]: {
    key: SECTION_KEY.INFECTION_PERSONAL,
    progressKey: PROGRESS_STEP_ID.INFECTION_DATA,
    fields: [
      {
        type: 'title',
        content: 'pages.registration-as-contact.infection-personal.title',
      },
      [
        FieldMap[FIELD_KEY.CONTACT_FIRST_NAME],
        FieldMap[FIELD_KEY.CONTACT_LAST_NAME],
      ],
      FieldMap[FIELD_KEY.CONTACT_PHONE],
      FieldMap[FIELD_KEY.CONTACT_EMAIL],
      FieldMap[FIELD_KEY.LOCALITY],
      FieldMap[FIELD_KEY.LAST_CONTACT],
    ],
  },
  [SECTION_KEY.INFECTION_CWA]: {
    key: SECTION_KEY.INFECTION_CWA,
    progressKey: PROGRESS_STEP_ID.INFECTION_DATA,
    fields: [
      {
        type: 'title',
        content: 'pages.registration-as-contact.infection-cwa.title',
      },
      FieldMap[FIELD_KEY.COUNT_RISK_ENCOUNTERS],
      FieldMap[FIELD_KEY.DAYS_SINCE_RISK_ENCOUNTER],
    ],
  },
  [SECTION_KEY.INFECTION_ESTABLISHMENT]: {
    key: SECTION_KEY.INFECTION_ESTABLISHMENT,
    progressKey: PROGRESS_STEP_ID.INFECTION_DATA,
    fields: [
      {
        type: 'title',
        content: 'pages.registration-as-contact.infection-establishment.title',
      },
      FieldMap[FIELD_KEY.LOCALITY],
      FieldMap[FIELD_KEY.LAST_CONTACT],
    ],
  },
  [SECTION_KEY.CONTACT_PERSONAL]: {
    key: SECTION_KEY.CONTACT_PERSONAL,
    progressKey: PROGRESS_STEP_ID.CONTACT_DATA,
    fields: [
      FieldMap[FIELD_KEY.CATEGORY],
      FieldMap[FIELD_KEY.REASON],
    ],
  },
  [SECTION_KEY.SYMPTOMS]: {
    key: SECTION_KEY.SYMPTOMS,
    progressKey: PROGRESS_STEP_ID.SYMPTOMS,
    fields: [
      {
        type: 'title',
        content: 'pages.registration-as-contact.symptoms.title',
      },
      FieldMap[FIELD_KEY.HAS_SYMPTOMS],
      FieldMap[FIELD_KEY.SYMPTOMS],
      FieldMap[FIELD_KEY.BEGIN_OF_SYMPTOMS],
    ],
  },
  [SECTION_KEY.PRECONDITIONS]: {
    key: SECTION_KEY.PRECONDITIONS,
    progressKey: PROGRESS_STEP_ID.PRECONDITIONS,
    fields: [
      {
        type: 'title',
        content: 'pages.registration-as-contact.preconditions.title',
      },
      FieldMap[FIELD_KEY.IMMUNE_DISEASE],
      FieldMap[FIELD_KEY.HEART_DISEASE],
      FieldMap[FIELD_KEY.DISEASE_EXPLANATION],
      FieldMap[FIELD_KEY.PREGNANCY],
      FieldMap[FIELD_KEY.PREGNANCY_WEEK],
    ],
  },
  [SECTION_KEY.PERSONAL_NOTE]: {
    key: SECTION_KEY.PERSONAL_NOTE,
    progressKey: PROGRESS_STEP_ID.PERSONAL_NOTE,
    fields: [
      {
        type: 'title',
        content: 'pages.registration-as-contact.personal-note.title',
      },
      FieldMap[FIELD_KEY.PERSONAL_NOTE],
    ],
  },
  [SECTION_KEY.REVIEW]: {
    key: SECTION_KEY.REVIEW,
    progressKey: PROGRESS_STEP_ID.REVIEW,
    fields: [
      {
        type: 'title',
        content: 'pages.registration-as-contact.form-reviewer.heading',
      },
      {
        type: 'paragraph',
        content: 'pages.registration-as-contact.form-reviewer.description',
      },
      FieldMap[FIELD_KEY.FORM_REVIEW],
      FieldMap[FIELD_KEY.CONSENT_GIVEN],
    ],
  },
  [SECTION_KEY.SUBMIT]: {
    key: SECTION_KEY.SUBMIT,
    progressKey: PROGRESS_STEP_ID.SUBMIT,
    fields: [
      FieldMap[FIELD_KEY.FORM_SUBMIT],
    ],
  },
  [SECTION_KEY.RESULT]: {
    key: SECTION_KEY.RESULT,
    progressKey: PROGRESS_STEP_ID.SUBMIT,
    fields: [
      {
        type: 'title',
        content: 'pages.registration-as-contact.result-display.heading',
      },
      {
        type: 'paragraph',
        content: 'pages.registration-as-contact.result-display.description',
      },
      FieldMap[FIELD_KEY.EMAIL_DISPLAY],
    ],
  },
}
