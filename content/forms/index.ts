/* eslint-disable no-redeclare */
import * as RegisterAsContactForm from './register-as-contact/form'
import * as TravelReturnForm from './travel-return/form'
import * as IndexCaseForm from './index-case/form'
import * as MasterForm from './master/form'

export const formIDs = [MasterForm.default.ID, RegisterAsContactForm.default.ID, TravelReturnForm.default.ID, IndexCaseForm.default.ID]
export type FORM_IDS = RegisterAsContactForm.default.ID | TravelReturnForm.default.ID | MasterForm.default.ID | IndexCaseForm.default.ID
export type Form = RegisterAsContactForm.default.Form | TravelReturnForm.default.Form | MasterForm.default.Form | IndexCaseForm.default.Form
export type Step = RegisterAsContactForm.default.Step | TravelReturnForm.default.Step | MasterForm.default.Step | IndexCaseForm.default.Step

export const FIELD_KEYS = { ...RegisterAsContactForm.FIELD_KEYS, ...TravelReturnForm.FIELD_KEYS, ...MasterForm.FIELD_KEYS, ...IndexCaseForm.FIELD_KEYS }
export type FIELD_KEYS = typeof FIELD_KEYS
export const STEP_IDS = { ...RegisterAsContactForm.STEP_IDS, ...TravelReturnForm.STEP_IDS, ...MasterForm.STEP_IDS, ...IndexCaseForm.STEP_IDS }
export type STEP_IDS = typeof STEP_IDS
export const PROGRESS_STEP_IDS = { ...RegisterAsContactForm.PROGRESS_STEP_IDS, ...TravelReturnForm.PROGRESS_STEP_IDS, ...MasterForm.PROGRESS_STEP_IDS, ...IndexCaseForm.PROGRESS_STEP_IDS }
export type PROGRESS_STEP_IDS = typeof PROGRESS_STEP_IDS
export const SECTION_KEYS = { ...RegisterAsContactForm.SECTION_KEYS, ...TravelReturnForm.SECTION_KEYS, ...MasterForm.SECTION_KEYS, ...IndexCaseForm.SECTION_KEYS }
export type SECTION_KEYS = typeof SECTION_KEYS

export const formMap = {
  [RegisterAsContactForm.default.ID]: RegisterAsContactForm.default,
  [TravelReturnForm.default.ID]: TravelReturnForm.default,
  [MasterForm.default.ID]: MasterForm.default,
  [IndexCaseForm.default.ID]: IndexCaseForm.default,
}
