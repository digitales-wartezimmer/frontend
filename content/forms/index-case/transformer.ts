import { STEP_ID } from './form'
import { FIELD_KEY } from './fields'
import { Transformer as BaseTransformer } from '~/types/form/Transformer'
import { Store } from '~/types/form/Store'

interface Contact {
  firstName: string
  lastName: string
  phone?: string
  email?: string
  address: {
    street?: string
    houseNumber?: string
    zip?: string
    city?: string
  }
}

export interface DTO {
  contact: {
    firstName: string
    lastName: string
    caseId?: string
  }
  contacts: Array<Contact>
  zip: string
  personalNote?: string
}

export class Transformer extends BaseTransformer<FIELD_KEY, STEP_ID, DTO> {
  getData (store: Store<FIELD_KEY, STEP_ID>): DTO {
    return {
      contact: {
        firstName: store.data[FIELD_KEY.FIRST_NAME],
        lastName: store.data[FIELD_KEY.LAST_NAME],
        caseId: store.data[FIELD_KEY.CASE_ID],
      },
      zip: store.data[FIELD_KEY.ZIP],
      personalNote: store.data[FIELD_KEY.PERSONAL_NOTE],
      contacts: store.data[FIELD_KEY.CONTACTS].map((contact: Contact) => ({
        firstName: contact.firstName,
        lastName: contact.lastName,
        phone: (contact.phone && contact.phone.trim().length > 0) ? this.transformPhoneNumber(contact.phone) : undefined,
        email: (contact.email && contact.email.trim().length > 0) ? contact.email : undefined,
        address: {
          street: (contact.address.street && contact.address.street.trim().length > 0) ? contact.address.street : undefined,
          houseNumber: (contact.address.houseNumber && contact.address.houseNumber.trim().length > 0) ? contact.address.houseNumber : undefined,
          zip: (contact.address.zip && contact.address.zip.trim().length > 0) ? contact.address.zip : undefined,
          city: (contact.address.city && contact.address.city.trim().length > 0) ? contact.address.city : undefined,
        },
      })),
    }
  }
}
