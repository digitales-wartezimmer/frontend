import { PROGRESS_STEP_ID, SECTION_KEY, SectionMap } from './sections'
import { FIELD_KEY } from './fields'

import AbstractForm, { Step as AbstractStep } from '~/types/form/Form'
import { Section as AbstractSection } from '~/types/form/Section'
import { Field as AbstractField } from '~/types/form/Field'
import { DTO } from '~/content/forms/index-case/transformer'

export enum STEP_ID {
  'HEALTH_OFFICE' = 'health_office',
  'USER_DATA' = 'user_data',
  'CONTACTS' = 'contacts',
  'PERSONAL_NOTE' = 'personal_note',
  'REVIEW' = 'review',
  'SUBMIT' = 'submit',
  'RESULT' = 'result',
  'SUCCESS' = 'success'
}

export { SECTION_KEY as SECTION_KEYS }
export { PROGRESS_STEP_ID as PROGRESS_STEP_IDS }
export { FIELD_KEY as FIELD_KEYS }
export { STEP_ID as STEP_IDS }

export namespace IndexCaseForm {
  export const ID = 'index-case'
  export type ID = 'index-case'
  export type Form = AbstractForm<'index-case', STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Step = AbstractStep<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Section = AbstractSection<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Field = AbstractField<STEP_ID, FIELD_KEY, DTO>

  export const form: IndexCaseForm.Form = {
    id: IndexCaseForm.ID,
    storeKey: 'indexCase',
    steps: {
      [STEP_ID.HEALTH_OFFICE]: {
        key: STEP_ID.HEALTH_OFFICE,
        section: SectionMap[SECTION_KEY.HEALTH_OFFICE],
        start: true,
        next: STEP_ID.USER_DATA,
      },
      [STEP_ID.USER_DATA]: {
        key: STEP_ID.USER_DATA,
        section: SectionMap[SECTION_KEY.USER_DATA],
        next: STEP_ID.CONTACTS,
        previous: STEP_ID.HEALTH_OFFICE,
      },
      [STEP_ID.CONTACTS]: {
        key: STEP_ID.CONTACTS,
        section: SectionMap[SECTION_KEY.CONTACTS],
        next: STEP_ID.PERSONAL_NOTE,
        previous: STEP_ID.USER_DATA,
      },
      [STEP_ID.PERSONAL_NOTE]: {
        key: STEP_ID.PERSONAL_NOTE,
        section: SectionMap[SECTION_KEY.PERSONAL_NOTE],
        next: STEP_ID.REVIEW,
        previous: STEP_ID.CONTACTS,
      },
      [STEP_ID.REVIEW]: {
        key: STEP_ID.REVIEW,
        section: SectionMap[SECTION_KEY.REVIEW],
        final: false,
        next: STEP_ID.SUBMIT,
        previous: STEP_ID.PERSONAL_NOTE,
        nextButtonText: 'pages.index-case.form-reviewer.complete',
      },
      [STEP_ID.SUBMIT]: {
        key: STEP_ID.SUBMIT,
        section: SectionMap[SECTION_KEY.SUBMIT],
        final: false,
        noBackButton: true,
        noNextButton: true,
        next: (store) => {
          const data = store.responseData
          if (data && data.workflow === 'sormas' && data.submitted) {
            return STEP_ID.SUCCESS
          }
          return STEP_ID.RESULT
        },
        previous: STEP_ID.REVIEW,
      },
      [STEP_ID.RESULT]: {
        key: STEP_ID.RESULT,
        section: SectionMap[SECTION_KEY.RESULT],
        final: true,
        noBackButton: true,
        previous: STEP_ID.REVIEW,
      },
      [STEP_ID.SUCCESS]: {
        key: STEP_ID.SUCCESS,
        section: SectionMap[SECTION_KEY.SUCCESS],
        final: true,
        noBackButton: true,
        previous: STEP_ID.REVIEW,
      },
    },
    progress: {
      [PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE]: {
        key: PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE,
        title: 'pages.index-case.progress.health_office',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.USER_DATA]: {
        key: PROGRESS_STEP_ID.USER_DATA,
        title: 'pages.index-case.progress.user_data',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.CONTACTS]: {
        key: PROGRESS_STEP_ID.CONTACTS,
        title: 'pages.index-case.progress.source_of_infection',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.PERSONAL_NOTE]: {
        key: PROGRESS_STEP_ID.PERSONAL_NOTE,
        title: 'pages.index-case.progress.personal_note',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.REVIEW]: {
        key: PROGRESS_STEP_ID.REVIEW,
        title: 'pages.index-case.progress.review',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.SUBMIT]: {
        key: PROGRESS_STEP_ID.SUBMIT,
        title: 'pages.index-case.progress.submit',
        titleVisible: true,
      },
    },
  }
}

export default IndexCaseForm
