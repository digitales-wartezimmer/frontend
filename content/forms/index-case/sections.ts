import { FIELD_KEY, FieldMap } from './fields'
import { STEP_ID } from './form'
import { Section } from '~/types/form/Section'
import { DTO } from '~/content/forms/index-case/transformer'

export enum SECTION_KEY {
  'HEALTH_OFFICE' = 'health_office',
  'USER_DATA' = 'user_data',
  'CONTACTS' = 'contacts',
  'PERSONAL_NOTE' = 'personal_note',
  'REVIEW' = 'review',
  'SUBMIT' = 'submit',
  'RESULT' = 'result',
  'SUCCESS' = 'success'
}

export enum PROGRESS_STEP_ID {
  'FETCH_HEALTH_OFFICE' = 'fetch_health_office',
  'USER_DATA' = 'user_data',
  'CONTACTS' = 'contacts',
  'PERSONAL_NOTE' = 'personal_note',
  'REVIEW' = 'review',
  'SUBMIT' = 'submit'
}

export const SectionMap: Record<SECTION_KEY, Section<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>> = {
  [SECTION_KEY.HEALTH_OFFICE]: {
    key: SECTION_KEY.HEALTH_OFFICE,
    progressKey: PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE,
    fields: [
      {
        type: 'title',
        content: 'pages.index-case.health-office.title',
      },
      {
        type: 'paragraph',
        content: 'pages.index-case.health-office.description',
      },
      FieldMap[FIELD_KEY.ZIP],
      {
        type: 'paragraph',
        content: 'pages.index-case.health-office.note',
      },
      FieldMap[FIELD_KEY.FETCH_HEALTH_OFFICE],
    ],
  },
  [SECTION_KEY.USER_DATA]: {
    key: SECTION_KEY.USER_DATA,
    progressKey: PROGRESS_STEP_ID.USER_DATA,
    fields: [
      {
        type: 'title',
        content: 'pages.index-case.user-data.title',
      },
      [
        FieldMap[FIELD_KEY.FIRST_NAME],
        FieldMap[FIELD_KEY.LAST_NAME],
      ],
      FieldMap[FIELD_KEY.CASE_ID],
      {
        type: 'paragraph',
        content: 'pages.index-case.user-data.case-id-note',
      },
    ],
  },
  [SECTION_KEY.CONTACTS]: {
    key: SECTION_KEY.CONTACTS,
    progressKey: PROGRESS_STEP_ID.CONTACTS,
    fields: [
      {
        type: 'title',
        content: 'pages.index-case.contacts.title',
      },
      {
        type: 'paragraph',
        content: 'pages.index-case.contacts.description',
      },
      {
        type: 'paragraph',
        content: 'pages.index-case.contacts.attachment_note',
        color: 'primary',
      },
      FieldMap[FIELD_KEY.CONTACTS],
    ],
  },
  [SECTION_KEY.PERSONAL_NOTE]: {
    key: SECTION_KEY.PERSONAL_NOTE,
    progressKey: PROGRESS_STEP_ID.PERSONAL_NOTE,
    fields: [
      {
        type: 'title',
        content: 'pages.index-case.personal_note.title',
      },
      FieldMap[FIELD_KEY.PERSONAL_NOTE],
    ],
  },
  [SECTION_KEY.REVIEW]: {
    key: SECTION_KEY.REVIEW,
    progressKey: PROGRESS_STEP_ID.REVIEW,
    fields: [
      {
        type: 'title',
        content: 'pages.index-case.form-reviewer.heading',
      },
      {
        type: 'paragraph',
        content: 'pages.index-case.form-reviewer.description',
      },
      FieldMap[FIELD_KEY.FORM_REVIEW],
      FieldMap[FIELD_KEY.CONSENT_GIVEN],
    ],
  },
  [SECTION_KEY.SUBMIT]: {
    key: SECTION_KEY.SUBMIT,
    progressKey: PROGRESS_STEP_ID.SUBMIT,
    fields: [
      FieldMap[FIELD_KEY.FORM_SUBMIT],
    ],
  },
  [SECTION_KEY.RESULT]: {
    key: SECTION_KEY.RESULT,
    progressKey: PROGRESS_STEP_ID.SUBMIT,
    fields: [
      {
        type: 'title',
        content: 'pages.index-case.result-display.heading',
      },
      {
        type: 'paragraph',
        content: 'pages.index-case.result-display.description',
      },
      {
        type: 'paragraph',
        content: 'pages.index-case.result-display.attachment_note',
        color: 'primary',
      },
      FieldMap[FIELD_KEY.EMAIL_DISPLAY],
    ],
  },
  [SECTION_KEY.SUCCESS]: {
    key: SECTION_KEY.SUCCESS,
    progressKey: PROGRESS_STEP_ID.SUBMIT,
    fields: [
      FieldMap[FIELD_KEY.SUCCESS],
    ],
  },
}
