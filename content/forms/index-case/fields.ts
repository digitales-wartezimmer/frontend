import { STEP_ID } from './form'
import { DTO, Transformer } from './transformer'
import { Field } from '~/types/form/Field'
import { storeZipAndRetrieveCity } from '~/content/forms/shared'

export enum FIELD_KEY {
  'ZIP' = 'zip',
  'FETCH_HEALTH_OFFICE' = 'fetchHealthOffice',
  'FIRST_NAME' = 'firstName',
  'LAST_NAME' = 'lastName',
  'CASE_ID' = 'caseId',
  'CONTACTS' = 'contacts',
  'PERSONAL_NOTE' = 'personalNote',
  'CONSENT_GIVEN' = 'consentGiven',
  'FORM_REVIEW' = 'formReview',
  'FORM_SUBMIT' = 'formSubmit',
  'EMAIL_DISPLAY' = 'emailDisplay',
  'SUCCESS' = 'success'
}

export const FieldMap: Record<FIELD_KEY, Field<STEP_ID, FIELD_KEY, DTO>> = {
  [FIELD_KEY.FIRST_NAME]: {
    type: 'text',
    key: FIELD_KEY.FIRST_NAME,
    alias: 'first-name',
    rules: 'required|max:64',
  },
  [FIELD_KEY.LAST_NAME]: {
    type: 'text',
    key: FIELD_KEY.LAST_NAME,
    alias: 'last-name',
    rules: 'required|max:64',
  },
  [FIELD_KEY.ZIP]: {
    type: 'text',
    key: FIELD_KEY.ZIP,
    alias: 'zip',
    rules: 'required|min:5|max:5|numeric|regex:[0-9]{5}',
    onChange: storeZipAndRetrieveCity,
  },
  [FIELD_KEY.FETCH_HEALTH_OFFICE]: {
    type: 'component',
    key: FIELD_KEY.FETCH_HEALTH_OFFICE,
    alias: 'fetch-health-office',
    componentName: 'fetch-health-office',
  },
  [FIELD_KEY.CASE_ID]: {
    type: 'text',
    key: FIELD_KEY.CASE_ID,
    alias: 'case-id',
    rules: 'min:1',
  },
  [FIELD_KEY.CONTACTS]: {
    type: 'component',
    key: FIELD_KEY.CONTACTS,
    alias: 'contacts',
    componentName: 'contacts',
  },
  [FIELD_KEY.PERSONAL_NOTE]: {
    type: 'textarea',
    key: FIELD_KEY.PERSONAL_NOTE,
    alias: 'personal-note',
    rules: 'max:500',
  },
  [FIELD_KEY.CONSENT_GIVEN]: {
    type: 'checkbox',
    key: FIELD_KEY.CONSENT_GIVEN,
    alias: 'consent-given',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }],
    hiddenInReview: true,
  },
  [FIELD_KEY.FORM_REVIEW]: {
    type: 'component',
    key: FIELD_KEY.FORM_REVIEW,
    alias: 'form-review',
    componentName: 'form-reviewer',
    additionalFields (store) {
      return store.data[FIELD_KEY.CONTACTS].map((contact: any, index: number) => ({
        title: (this as any).$t('pages.index-case.contacts.numbering', { number: index + 1 }),
        value: Object.values(contact).map((value: any) => (Object.prototype.toString.call(value) === '[object Object]') ? Object.values(value).join(' ') : value).join(' '),
        type: 'artificial',
      }))
    },
  },
  [FIELD_KEY.FORM_SUBMIT]: {
    type: 'component',
    key: FIELD_KEY.FORM_SUBMIT,
    alias: 'form-submit',
    componentName: 'form-submitter',
    apiUrl: '/api/indexCase',
    transformer: new Transformer(),
    onChange (newValue) {
      if (!newValue) {
        return
      }
      (this as any).$store.commit((this as any).form.storeKey + '/updateField', {
        path: 'responseData',
        value: newValue,
      })
    },
  },
  [FIELD_KEY.EMAIL_DISPLAY]: {
    type: 'component',
    key: FIELD_KEY.EMAIL_DISPLAY,
    alias: 'email-display',
    componentName: 'email-display',
  },
  [FIELD_KEY.SUCCESS]: {
    type: 'component',
    key: FIELD_KEY.SUCCESS,
    alias: 'success',
    componentName: 'success',
  },
}
