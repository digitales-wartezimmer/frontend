import { FIELD_KEY } from '~/content/forms/travel-return/fields'

export function storeZip (this: any, newValue?: string) {
  (this as any).$store.commit((this as any).form.storeKey + '/updateField', {
    path: 'zip',
    value: newValue,
  })
}

export function getCityBasedOnZip (this: any, newValue?: string) {
  if (!newValue || newValue.trim().length <= 4) {
    return
  }
  (this as any).$axios.get('/api/plzInfo', { params: { postCode: newValue.trim() } }).then((response: any) => {
    if (response.status !== 200) {
      return
    }
    let result = ''
    if (response.data && response.data.length > 0) {
      result = response.data[0].plzName
    }
    (this as any).$store.commit((this as any).form.storeKey + '/updateField', {
      path: 'data.' + FIELD_KEY.CITY,
      value: result,
    })
  })
}

export function storeZipAndRetrieveCity (this: any, newValue?: string) {
  storeZip.apply(this, [newValue])
  getCityBasedOnZip.apply(this, [newValue])
}
