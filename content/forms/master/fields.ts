import { STEP_ID } from './form'
import { DTO } from './transformer'
import { Field } from '~/types/form/Field'

export enum FIELD_KEY {
  'CONTACT' = 'contact',
  'INFECTION' = 'infection',
  'LOADING' = 'loading',
  'START_OVER_BUTTON' = 'start_over_button'
}

export const FieldMap: Record<FIELD_KEY, Field<STEP_ID, FIELD_KEY, DTO>> = {
  [FIELD_KEY.CONTACT]: {
    type: 'radio',
    key: FIELD_KEY.CONTACT,
    alias: 'contact',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }, {
      alias: 'no',
      value: 'no',
    }],
  },
  [FIELD_KEY.INFECTION]: {
    type: 'radio',
    key: FIELD_KEY.INFECTION,
    alias: 'infection',
    rules: 'required',
    choices: [{
      alias: 'yes',
      value: 'yes',
    }, {
      alias: 'no',
      value: 'no',
    }],
  },
  [FIELD_KEY.LOADING]: {
    type: 'component',
    key: FIELD_KEY.LOADING,
    alias: 'loading',
    componentName: 'loading',
  },
  [FIELD_KEY.START_OVER_BUTTON]: {
    type: 'component',
    key: FIELD_KEY.START_OVER_BUTTON,
    alias: 'start-over-button',
    componentName: 'start-over-button',
  },
}
