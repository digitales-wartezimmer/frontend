import { PROGRESS_STEP_ID, SECTION_KEY, SectionMap } from './sections'
import { DTO } from './transformer'

import { FIELD_KEY } from './fields'
import IndexCaseForm from '~/content/forms/index-case/form'
import RegisterAsContactForm from '~/content/forms/register-as-contact/form'
import AbstractForm, { Step as AbstractStep } from '~/types/form/Form'
import { Section as AbstractSection } from '~/types/form/Section'
import { Field as AbstractField } from '~/types/form/Field'

export enum STEP_ID {
  'CONTACT' = 'contact',
  'INFECTION' = 'infection',
  'CHANGE_FORM' = 'change_form',
  'NO_ACTION_NECESSARY' = 'no_action_necessary'
}
export { SECTION_KEY as SECTION_KEYS }
export { PROGRESS_STEP_ID as PROGRESS_STEP_IDS }
export { FIELD_KEY as FIELD_KEYS }
export { STEP_ID as STEP_IDS }

namespace MasterForm {
  export const ID = 'master'
  export type ID = 'master'
  export type Form = AbstractForm<'master', STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Step = AbstractStep<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Section = AbstractSection<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>
  export type Field = AbstractField<STEP_ID, FIELD_KEY, DTO>

  export const form: MasterForm.Form = {
    id: MasterForm.ID,
    storeKey: 'masterForm',
    steps: {
      [STEP_ID.CONTACT]: {
        start: true,
        key: STEP_ID.CONTACT,
        section: SectionMap[SECTION_KEY.CONTACT],
        next: STEP_ID.INFECTION,
      },
      [STEP_ID.INFECTION]: {
        key: STEP_ID.INFECTION,
        section: SectionMap[SECTION_KEY.INFECTION],
        next: (store) => {
          if (store.data[FIELD_KEY.INFECTION] === 'no' && store.data[FIELD_KEY.CONTACT] === 'no') {
            return STEP_ID.NO_ACTION_NECESSARY
          }
          return STEP_ID.CHANGE_FORM
        },
        previous: STEP_ID.CONTACT,
      },
      [STEP_ID.CHANGE_FORM]: {
        key: STEP_ID.CHANGE_FORM,
        final: true,
        previous: STEP_ID.INFECTION,
        section: SectionMap[SECTION_KEY.LOADING],
        noBackButton: true,
        noNextButton: true,
        changeForm: true,
        form: (store) => {
          if (store.data[FIELD_KEY.CONTACT] === 'yes') {
            return RegisterAsContactForm.ID
          }
          if (store.data[FIELD_KEY.INFECTION] === 'yes') {
            return IndexCaseForm.ID
          }
          return null
        },
      },
      [STEP_ID.NO_ACTION_NECESSARY]: {
        key: STEP_ID.NO_ACTION_NECESSARY,
        final: true,
        previous: STEP_ID.INFECTION,
        section: SectionMap[SECTION_KEY.NO_ACTION_NECESSARY],
        noBackButton: true,
        noNextButton: true,
      },
    },
    progress: {
      [PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE]: {
        key: PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE,
        title: 'pages.master.progress.health_office',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.USER_DATA]: {
        key: PROGRESS_STEP_ID.USER_DATA,
        title: 'pages.master.progress.user_data',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.SOURCE_OF_INFECTION]: {
        key: PROGRESS_STEP_ID.SOURCE_OF_INFECTION,
        title: 'pages.master.progress.source_of_infection',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.INFECTION_DATA]: {
        key: PROGRESS_STEP_ID.INFECTION_DATA,
        title: 'pages.master.progress.infection_data',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.CONTACT_DATA]: {
        key: PROGRESS_STEP_ID.CONTACT_DATA,
        title: 'pages.master.progress.personal_contact',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.SYMPTOMS]: {
        key: PROGRESS_STEP_ID.SYMPTOMS,
        title: 'pages.master.progress.symptoms',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.PRECONDITIONS]: {
        key: PROGRESS_STEP_ID.PRECONDITIONS,
        title: 'pages.master.progress.preconditions',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.PERSONAL_NOTE]: {
        key: PROGRESS_STEP_ID.PERSONAL_NOTE,
        title: 'pages.master.progress.personal_note',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.CONSENT]: {
        key: PROGRESS_STEP_ID.CONSENT,
        title: 'pages.master.progress.consent',
        titleVisible: false,
      },
      [PROGRESS_STEP_ID.REVIEW]: {
        key: PROGRESS_STEP_ID.REVIEW,
        title: 'pages.master.progress.review',
        titleVisible: true,
      },
      [PROGRESS_STEP_ID.SUBMIT]: {
        key: PROGRESS_STEP_ID.SUBMIT,
        title: 'pages.master.progress.submit',
        titleVisible: true,
      },
    },
  }
}

export default MasterForm
