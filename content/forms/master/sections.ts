import { FIELD_KEY, FieldMap } from './fields'
import { STEP_ID } from './form'
import { DTO } from './transformer'
import { Section } from '~/types/form/Section'

export enum SECTION_KEY {
  'CONTACT' = 'contact',
  'INFECTION' = 'infection',
  'LOADING' = 'loading',
  'NO_ACTION_NECESSARY' = 'no_action_necessary'
}

export enum PROGRESS_STEP_ID {
  'FETCH_HEALTH_OFFICE' = 'fetch_health_office',
  'USER_DATA' = 'user_data',
  'SOURCE_OF_INFECTION' = 'source_of_infection',
  'INFECTION_DATA' = 'infection_data',
  'CONTACT_DATA' = 'contact_data',
  'SYMPTOMS' = 'symptoms',
  'PRECONDITIONS' = 'preconditions',
  'PERSONAL_NOTE' = 'personal_note',
  'CONSENT' = 'consent',
  'REVIEW' = 'review',
  'SUBMIT' = 'submit'
}

export const SectionMap: Record<SECTION_KEY, Section<STEP_ID, SECTION_KEY, PROGRESS_STEP_ID, FIELD_KEY, DTO>> = {
  [SECTION_KEY.CONTACT]: {
    key: SECTION_KEY.CONTACT,
    progressKey: PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE,
    fields: [
      FieldMap[FIELD_KEY.CONTACT],
    ],
  },
  [SECTION_KEY.INFECTION]: {
    key: SECTION_KEY.INFECTION,
    progressKey: PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE,
    fields: [
      FieldMap[FIELD_KEY.INFECTION],
    ],
  },
  [SECTION_KEY.LOADING]: {
    key: SECTION_KEY.LOADING,
    progressKey: PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE,
    fields: [
      {
        type: 'paragraph',
        content: 'pages.master.loading.description',
      },
      FieldMap[FIELD_KEY.LOADING],
    ],
  },
  [SECTION_KEY.NO_ACTION_NECESSARY]: {
    key: SECTION_KEY.NO_ACTION_NECESSARY,
    progressKey: PROGRESS_STEP_ID.FETCH_HEALTH_OFFICE,
    fields: [
      {
        type: 'title',
        content: 'pages.master.no-action-necessary.title',
      }, {
        type: 'paragraph',
        content: 'pages.master.no-action-necessary.paragraph-1',
      }, {
        type: 'paragraph',
        content: 'pages.master.no-action-necessary.paragraph-2',
      }, {
        type: 'paragraph',
        content: 'pages.master.no-action-necessary.paragraph-3',
      },
      FieldMap[FIELD_KEY.START_OVER_BUTTON],
    ],
  },
}
