import { FIELD_KEY } from './fields'
import { STEP_ID } from './form'
import { Transformer as BaseTransformer } from '~/types/form/Transformer'
import { Store } from '~/types/form/Store'

export interface DTO {
}

export class Transformer extends BaseTransformer<FIELD_KEY, STEP_ID, DTO> {
  getData (_store: Store<FIELD_KEY, STEP_ID>): DTO {
    return {}
  }
}
