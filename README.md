<p align="center" class="text-center">
  <a href="https://digitales-wartezimmer.org/" target="blank"><img src="https://i.imgur.com/z4zeQQO.png" width="320" alt="Digitales Wartezimmer Logo" /></a>
</p>
  
<p align="center" class="text-center">A digital solution for simple and efficient exchange of information between health authorities and citizens for rapid containment of infectious diseases.</p>
<p align="center" class="text-center">
<a href="https://gitlab.com/digitales-wartezimmer/frontend/-/pipelines" target="_blank"><img alt="Gitlab pipeline status Production" src="https://img.shields.io/gitlab/pipeline/digitales-wartezimmer/frontend/main?label=build%20production"></a>
<a href="https://gitlab.com/digitales-wartezimmer/frontend/-/pipelines" target="_blank"><img alt="Gitlab pipeline status Staging" src="https://img.shields.io/gitlab/pipeline/digitales-wartezimmer/gateway/staging?label=build%20staging"></a>
<a href="https://status.digitales-wartezimmer.org" target="_blank"><img alt="Uptime Robot status Production" src="https://img.shields.io/uptimerobot/status/m786427079-170aecc04d13e1f85353818b?label=status%20production"></a>
<a href="https://status.digitales-wartezimmer.org" target="_blank"><img alt="Uptime Robot status Staging" src="https://img.shields.io/uptimerobot/status/m786427077-89c10f1f5ab73a6ba0a5dbe4?label=status%20staging"></a>
<a href="https://gitlab.com/digitales-wartezimmer/frontend/-/pipelines" target="_blank"><img alt="Gitlab code coverage" src="https://img.shields.io/gitlab/coverage/digitales-wartezimmer/frontend/main"></a>
<a href="https://www.gnu.org/licenses/gpl-3.0" target="_blank"><img src="https://img.shields.io/badge/License-GPLv3-brightgreen.svg" alt="License GPLv3"></a>
</p>

> :warning: This project is sadly not active anymore and unmaintained. For more info visit https://digitales-wartezimmer.org. Should you have questions regarding the codebase feel free to send an [E-mail](mailto:digitales-wartezimmer@bernhardwittmann.com).

# Digitales Wartezimmer - Frontend

#### :rocket: [https://digitales-wartezimmer.org](https://digitales-wartezimmer.org) 

With our digital solution, all relevant information of COVID-19 contact persons will be digitally captured and transferred to the systems used by the responsible health authorities to track contact persons.

By digitizing the transmission of COVID-19 contact person data, we want to help health authorities break the chain of infection quickly. At the same time, the COVID-19 contact person solution should provide a fast and user-friendly way to contact his/her health authority. 

This repository contains the Frontend codebase of the Digitales Wartezimmer.

## Installation

You should have [node](https://nodejs.org/en/) and npm or [yarn](https://yarnpkg.com) installed.

```bash
$ npm install
```

## Running the app

For more information see [Nuxt.js Docs](https://nuxtjs.org).

```bash
# development
$ npm run dev


# production mode
$ npm run build
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run e2e

# linting
$ npm run lint
```

## Support

Digitales Wartezimmer is a GPLv3 open source project. It is powered by our awesome [Team](https://digitales-wartezimmer.org/project). If you'd like to join us, please contact [info@digitales-wartezimmer.org](mailto:digitales-wartezimmer.org)

## Stay in touch

- Website - [https://digitales-wartezimmer.org](https://digitales-wartezimmer.org)
- Twitter - [@Dig_Wartezimmer](https://twitter.com/Dig_Wartezimmer)
- Facebook - [DigitalesWartezimmer](https://www.facebook.com/DigitalesWartezimmer/)
- LinkedIn - [digitales-wartezimmer](https://www.linkedin.com/company/digitales-wartezimmer/)

## License

  Digitales Wartezimmer is [GPLv3 licensed](LICENSE).
