import { GetterTree, MutationTree } from 'vuex'
// @ts-ignore
import { getField, updateField } from 'vuex-map-fields'
import { RootState } from '~/store'
import { Store as AbstractStore } from '~/types/form/Store'
import { STEP_IDS, FIELD_KEYS } from '~/content/forms/index-case/form'

export interface Store extends AbstractStore<FIELD_KEYS, STEP_IDS> {
  data: {
    [FIELD_KEYS.FIRST_NAME]: string,
    [FIELD_KEYS.LAST_NAME]: string,
    [FIELD_KEYS.ZIP]: string,
    [FIELD_KEYS.FETCH_HEALTH_OFFICE]: undefined,
    [FIELD_KEYS.CASE_ID]: string,
    [FIELD_KEYS.CONTACTS]: Array<{ firstName: string, lastName: string, phone?: string, email?: string, address: { street?: string, houseNumber?: string, zip?: string, city?: string }}>,
    [FIELD_KEYS.PERSONAL_NOTE]: string,
    [FIELD_KEYS.CONSENT_GIVEN]: Array<string>,
    [FIELD_KEYS.FORM_REVIEW]: undefined,
    [FIELD_KEYS.FORM_SUBMIT]: undefined,
    [FIELD_KEYS.EMAIL_DISPLAY]: undefined,
    [FIELD_KEYS.SUCCESS]: undefined
  }
}

const getDefaultState: () => Store = () => ({
  currentStep: undefined,
  responseData: undefined,
  quarantineStartDate: undefined,
  zip: undefined,
  data: {
    [FIELD_KEYS.FIRST_NAME]: '',
    [FIELD_KEYS.LAST_NAME]: '',
    [FIELD_KEYS.ZIP]: '',
    [FIELD_KEYS.CASE_ID]: '',
    [FIELD_KEYS.FETCH_HEALTH_OFFICE]: undefined,
    [FIELD_KEYS.CONTACTS]: [],
    [FIELD_KEYS.PERSONAL_NOTE]: '',
    [FIELD_KEYS.CONSENT_GIVEN]: [],
    [FIELD_KEYS.FORM_REVIEW]: undefined,
    [FIELD_KEYS.FORM_SUBMIT]: undefined,
    [FIELD_KEYS.EMAIL_DISPLAY]: undefined,
    [FIELD_KEYS.SUCCESS]: undefined,
  },
})

export const state = getDefaultState

export type TravelReturnState = ReturnType<typeof state>

export const getters: GetterTree<TravelReturnState, RootState> = {
  getField,
}

export const mutations: MutationTree<TravelReturnState> = {
  updateField,
  reset (state) {
    Object.assign(state, getDefaultState())
  },
}
