import { MutationTree } from 'vuex'
import { v4 as uuid } from 'uuid'

export const state = () => ({
  idempotencyToken: undefined as (string | undefined),
})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  generateIdempotencyToken (state) {
    state.idempotencyToken = uuid()
  },
}
