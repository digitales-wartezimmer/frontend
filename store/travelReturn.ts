import { GetterTree, MutationTree } from 'vuex'
// @ts-ignore
import { getField, updateField } from 'vuex-map-fields'
import { RootState } from '~/store'
import { Store as AbstractStore } from '~/types/form/Store'
import { STEP_IDS, FIELD_KEYS } from '~/content/forms/travel-return/form'

export interface Store extends AbstractStore<FIELD_KEYS, STEP_IDS> {
  data: {
    [FIELD_KEYS.FIRST_NAME]: string,
    [FIELD_KEYS.LAST_NAME]: string,
    [FIELD_KEYS.ZIP]: string,
    [FIELD_KEYS.FETCH_HEALTH_OFFICE]: undefined,
    [FIELD_KEYS.BIRTHDAY]: string,
    [FIELD_KEYS.GENDER]: string,
    [FIELD_KEYS.STREET]: string,
    [FIELD_KEYS.CITY]: string,
    [FIELD_KEYS.EMAIL]: string,
    [FIELD_KEYS.PHONE]: string,
    [FIELD_KEYS.TRAVEL_COUNTRY]: string,
    [FIELD_KEYS.TRAVEL_REGION]: string,
    [FIELD_KEYS.STAY_DURATION]: string,
    [FIELD_KEYS.DATE_OF_ENTRY]: string,
    [FIELD_KEYS.MODE_OF_ENTRY]: Array<string>,
    [FIELD_KEYS.HAS_SYMPTOMS]: string | undefined,
    [FIELD_KEYS.SYMPTOMS]: string,
    [FIELD_KEYS.BEGIN_OF_SYMPTOMS]: string,
    [FIELD_KEYS.HAS_TESTED]: string,
    [FIELD_KEYS.DATE_OF_TEST]: string,
    [FIELD_KEYS.TEST_COUNTRY]: string,
    [FIELD_KEYS.TEST_RESULT]: string | undefined,
    [FIELD_KEYS.IMMUNE_DISEASE]: string | undefined,
    [FIELD_KEYS.HEART_DISEASE]: string | undefined,
    [FIELD_KEYS.DISEASE_EXPLANATION]: string,
    [FIELD_KEYS.PREGNANCY]: string | undefined,
    [FIELD_KEYS.PREGNANCY_WEEK]: string,
    [FIELD_KEYS.PERSONAL_NOTE]: string,
    [FIELD_KEYS.CONSENT_GIVEN]: Array<string>,
    [FIELD_KEYS.FORM_REVIEW]: undefined,
    [FIELD_KEYS.FORM_SUBMIT]: undefined,
    [FIELD_KEYS.EMAIL_DISPLAY]: undefined,
    [FIELD_KEYS.QUARANTINE_DISPLAY]: undefined
  }
}

const getDefaultState: () => Store = () => ({
  currentStep: undefined,
  responseData: undefined,
  quarantineStartDate: undefined,
  zip: undefined,
  data: {
    [FIELD_KEYS.FIRST_NAME]: '',
    [FIELD_KEYS.LAST_NAME]: '',
    [FIELD_KEYS.ZIP]: '',
    [FIELD_KEYS.FETCH_HEALTH_OFFICE]: undefined,
    [FIELD_KEYS.BIRTHDAY]: '',
    [FIELD_KEYS.GENDER]: '',
    [FIELD_KEYS.STREET]: '',
    [FIELD_KEYS.CITY]: '',
    [FIELD_KEYS.EMAIL]: '',
    [FIELD_KEYS.PHONE]: '',
    [FIELD_KEYS.TRAVEL_COUNTRY]: '',
    [FIELD_KEYS.TRAVEL_REGION]: '',
    [FIELD_KEYS.STAY_DURATION]: '',
    [FIELD_KEYS.DATE_OF_ENTRY]: '',
    [FIELD_KEYS.MODE_OF_ENTRY]: [],
    [FIELD_KEYS.HAS_SYMPTOMS]: '',
    [FIELD_KEYS.HAS_SYMPTOMS]: undefined,
    [FIELD_KEYS.SYMPTOMS]: '',
    [FIELD_KEYS.BEGIN_OF_SYMPTOMS]: '',
    [FIELD_KEYS.HAS_TESTED]: '',
    [FIELD_KEYS.DATE_OF_TEST]: '',
    [FIELD_KEYS.TEST_COUNTRY]: '',
    [FIELD_KEYS.TEST_RESULT]: undefined,
    [FIELD_KEYS.IMMUNE_DISEASE]: undefined,
    [FIELD_KEYS.HEART_DISEASE]: undefined,
    [FIELD_KEYS.DISEASE_EXPLANATION]: '',
    [FIELD_KEYS.PREGNANCY]: undefined,
    [FIELD_KEYS.PREGNANCY_WEEK]: '',
    [FIELD_KEYS.PERSONAL_NOTE]: '',
    [FIELD_KEYS.CONSENT_GIVEN]: [],
    [FIELD_KEYS.FORM_REVIEW]: undefined,
    [FIELD_KEYS.FORM_SUBMIT]: undefined,
    [FIELD_KEYS.EMAIL_DISPLAY]: undefined,
    [FIELD_KEYS.QUARANTINE_DISPLAY]: undefined,
  },
})

export const state = getDefaultState

export type TravelReturnState = ReturnType<typeof state>

export const getters: GetterTree<TravelReturnState, RootState> = {
  getField,
}

export const mutations: MutationTree<TravelReturnState> = {
  updateField,
  reset (state) {
    Object.assign(state, getDefaultState())
  },
}
