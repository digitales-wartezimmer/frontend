import { GetterTree, MutationTree } from 'vuex'
// @ts-ignore
import { getField, updateField } from 'vuex-map-fields'
import { RootState } from '~/store'
import { Store as AbstractStore } from '~/types/form/Store'
import { STEP_IDS, FIELD_KEYS } from '~/content/forms/master/form'

export interface Store extends AbstractStore<FIELD_KEYS, STEP_IDS> {
  data: {
    [FIELD_KEYS.CONTACT]: string | undefined,
    [FIELD_KEYS.INFECTION]: string | undefined,
    [FIELD_KEYS.LOADING]: undefined,
    [FIELD_KEYS.START_OVER_BUTTON]: undefined
  }
}

const getDefaultState: () => Store = () => ({
  currentStep: undefined,
  responseData: undefined,
  quarantineStartDate: undefined,
  zip: undefined,
  data: {
    [FIELD_KEYS.CONTACT]: undefined,
    [FIELD_KEYS.INFECTION]: undefined,
    [FIELD_KEYS.LOADING]: undefined,
    [FIELD_KEYS.START_OVER_BUTTON]: undefined,
  },
})

export const state = getDefaultState

export type MasterFormState = ReturnType<typeof state>

export const getters: GetterTree<MasterFormState, RootState> = {
  getField,
}

export const mutations: MutationTree<MasterFormState> = {
  updateField,
  reset (state) {
    Object.assign(state, getDefaultState())
  },
}
