import { GetterTree, MutationTree } from 'vuex'
// @ts-ignore
import { getField, updateField } from 'vuex-map-fields'
import { RootState } from '~/store'
import { Store as AbstractStore } from '~/types/form/Store'
import { STEP_IDS, FIELD_KEYS } from '~/content/forms/register-as-contact/form'

export interface Store extends AbstractStore<FIELD_KEYS, STEP_IDS> {
  data: {
    [FIELD_KEYS.FIRST_NAME]: string,
    [FIELD_KEYS.LAST_NAME]: string,
    [FIELD_KEYS.ZIP]: string,
    [FIELD_KEYS.FETCH_HEALTH_OFFICE]: undefined,
    [FIELD_KEYS.BIRTHDAY]: string,
    [FIELD_KEYS.GENDER]: string,
    [FIELD_KEYS.STREET]: string,
    [FIELD_KEYS.CITY]: string,
    [FIELD_KEYS.EMAIL]: string,
    [FIELD_KEYS.PHONE]: string,
    [FIELD_KEYS.SOURCE_OF_INFECTION]: string,
    [FIELD_KEYS.CONTACT_FIRST_NAME]: string,
    [FIELD_KEYS.CONTACT_LAST_NAME]: string,
    [FIELD_KEYS.CONTACT_PHONE]: string,
    [FIELD_KEYS.CONTACT_EMAIL]: string,
    [FIELD_KEYS.LOCALITY]: string,
    [FIELD_KEYS.LAST_CONTACT]: string,
    [FIELD_KEYS.COUNT_RISK_ENCOUNTERS]: string,
    [FIELD_KEYS.DAYS_SINCE_RISK_ENCOUNTER]: string,
    [FIELD_KEYS.CATEGORY]: string,
    [FIELD_KEYS.REASON]: string,
    [FIELD_KEYS.HAS_SYMPTOMS]: boolean | undefined,
    [FIELD_KEYS.SYMPTOMS]: string,
    [FIELD_KEYS.BEGIN_OF_SYMPTOMS]: string,
    [FIELD_KEYS.IMMUNE_DISEASE]: boolean | undefined,
    [FIELD_KEYS.HEART_DISEASE]: boolean | undefined,
    [FIELD_KEYS.DISEASE_EXPLANATION]: string,
    [FIELD_KEYS.PREGNANCY]: boolean | undefined,
    [FIELD_KEYS.PREGNANCY_WEEK]: string,
    [FIELD_KEYS.PERSONAL_NOTE]: string,
    [FIELD_KEYS.CONSENT_GIVEN]: Array<string>,
    [FIELD_KEYS.FORM_REVIEW]: undefined,
    [FIELD_KEYS.FORM_SUBMIT]: undefined,
    [FIELD_KEYS.EMAIL_DISPLAY]: undefined
  }
}

const getDefaultState: () => Store = () => ({
  currentStep: undefined,
  responseData: undefined,
  quarantineStartDate: undefined,
  zip: undefined,
  data: {
    [FIELD_KEYS.FIRST_NAME]: '',
    [FIELD_KEYS.LAST_NAME]: '',
    [FIELD_KEYS.ZIP]: '',
    [FIELD_KEYS.FETCH_HEALTH_OFFICE]: undefined,
    [FIELD_KEYS.BIRTHDAY]: '',
    [FIELD_KEYS.GENDER]: '',
    [FIELD_KEYS.STREET]: '',
    [FIELD_KEYS.CITY]: '',
    [FIELD_KEYS.EMAIL]: '',
    [FIELD_KEYS.PHONE]: '',
    [FIELD_KEYS.SOURCE_OF_INFECTION]: '',
    [FIELD_KEYS.CONTACT_FIRST_NAME]: '',
    [FIELD_KEYS.CONTACT_LAST_NAME]: '',
    [FIELD_KEYS.CONTACT_PHONE]: '',
    [FIELD_KEYS.CONTACT_EMAIL]: '',
    [FIELD_KEYS.LOCALITY]: '',
    [FIELD_KEYS.LAST_CONTACT]: '',
    [FIELD_KEYS.COUNT_RISK_ENCOUNTERS]: '',
    [FIELD_KEYS.DAYS_SINCE_RISK_ENCOUNTER]: '',
    [FIELD_KEYS.CATEGORY]: '',
    [FIELD_KEYS.REASON]: '',
    [FIELD_KEYS.HAS_SYMPTOMS]: undefined,
    [FIELD_KEYS.SYMPTOMS]: '',
    [FIELD_KEYS.BEGIN_OF_SYMPTOMS]: '',
    [FIELD_KEYS.IMMUNE_DISEASE]: undefined,
    [FIELD_KEYS.HEART_DISEASE]: undefined,
    [FIELD_KEYS.DISEASE_EXPLANATION]: '',
    [FIELD_KEYS.PREGNANCY]: undefined,
    [FIELD_KEYS.PREGNANCY_WEEK]: '',
    [FIELD_KEYS.PERSONAL_NOTE]: '',
    [FIELD_KEYS.CONSENT_GIVEN]: [],
    [FIELD_KEYS.FORM_REVIEW]: undefined,
    [FIELD_KEYS.FORM_SUBMIT]: undefined,
    [FIELD_KEYS.EMAIL_DISPLAY]: undefined,
  },
})

export const state = getDefaultState

export type RegistrationAsContactState = ReturnType<typeof state>

export const getters: GetterTree<RegistrationAsContactState, RootState> = {
  getField,
}

export const mutations: MutationTree<RegistrationAsContactState> = {
  updateField,
  reset (state) {
    Object.assign(state, getDefaultState())
  },
}
