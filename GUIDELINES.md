# Development Guidelines

## Git

1. The main branch holds production-ready releases.
2. Feature branches merge directly to the main branch using merge requests.
3. Feature branches are named: `DIWA-<Issue ID>`.

## Styling

1. We use the BEM naming convention.
2. There are 5 types of styling:
  1. Base Styling (under `assets/styles/*`)
    - no classes
  2. Element Styling
    - Vue.js components for small interface elements, like buttons
    - BEM classes are prefixed with an `e`, like this: `e-button`
    - Styles are defined in the `<style>` block of the Vue.js component
    - the Vue.js component lives in `components/elements/*`
  3. Module Styling
    - Vue.js components for composed interface elements, like a navigation bar
    - BEM classes are prefixed with a `m`, like this: `m-navigation`
    - Styles are defined in the `<style>` block of the Vue.js component
    - the Vue.js component lives in `components/modules/*`
  4. Page Styling
    - Vue.js components for pages and layouts, like a the home page
    - BEM classes are prefixed with a `p`, like this: `p-home`
    - Styles are defined in the `<style>` block of the Vue.js component
    - the Vue.js component lives in `pages/*`
  5. Utility classes
    - single purpose style definitions
    - used sparingly
    - classes are prefixed with an `u`, like this: `u-sr-only`
3. Definitions are indented as to (roughly) reflect the HTML structure