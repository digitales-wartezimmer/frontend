import Vue from 'vue'
export default Vue.extend({
  computed: {
    max (): number | undefined {
      // @ts-ignore
      const rules = this.rules as any
      if (typeof rules === 'string') {
        const max = rules.match(/max:(\d*)/)
        if (max && typeof max[1] === 'string') {
          return parseInt(max[1], 10)
        }
      }
      if (
        typeof rules === 'object' &&
        typeof rules.max === 'number'
      ) {
        return rules.max
      }
      if (
        typeof rules === 'object' &&
        typeof rules.max === 'object' &&
        typeof rules.max.length === 'number'
      ) {
        return rules.max
      }
      return undefined
    },
  },
})
