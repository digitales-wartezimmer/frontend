# COMPONENTS

The components directory contains all Vue.js components, that are not pages. Pages reside under
`/pages`. The components are split into two categories:

## Elements

Elements are small interface elements, like special buttons, icons or input elements. You can find
them in the [`elements/`](./elements) directory.

## Modules

Modules are composed interface elements, making use of other [Elements](#elements). They can be many
things, like navigation, a map, a form, etc. You can find them in the
[`modules/`](./modules) directory.
