import { Middleware } from '@nuxt/types'

const apiAuthenticateMiddleware: Middleware = async ({ $axios, $config, req, res }) => {
  if (process.server && !process.env.DISABLE_AUTH) {
    const authResponse = await $axios.post(`${$config.API_BASE_URL}/auth/authenticate`, { token: $config.API_TOKEN, host: req.headers.host, referer: req.headers.referer })
    const cookies = authResponse.headers['set-cookie']
    if (cookies && cookies.length > 0) {
      res.setHeader('Set-Cookie', cookies)
    }
  }
}

export default apiAuthenticateMiddleware
