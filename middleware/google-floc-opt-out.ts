import { Middleware } from '@nuxt/types'

const googleFlocOptOut: Middleware = ({ res }) => {
  if (res) {
    res.setHeader(
      'Permissions-Policy',
      'geolocation=(), interest-cohort=()',
    )
  }
}

export default googleFlocOptOut
