import { Middleware } from '@nuxt/types'

const idempotencyMiddleware: Middleware = (context) => {
  context.store.commit('generateIdempotencyToken')
}

export default idempotencyMiddleware
