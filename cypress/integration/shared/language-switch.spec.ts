describe('Shared', () => {
  describe('Language Switch', () => {
    describe('basic behavior', () => {
      beforeEach(() => {
        cy.acceptCookies()
        cy.setI18nCookie('de')
        cy.visit('/')
        cy.location('pathname').should('eq', '/')
      })

      it('displays an text to read in another language', () => {
        cy.get('.m-footer-section__items').should('contain', 'Read in English')
        cy.setI18nCookie('en')
        cy.visit('/en/')
        cy.get('.m-footer-section__items').should('contain', 'Auf Deutsch lesen')
      })

      it('can change the language', () => {
        cy.get('h2').contains('Covid-19 Selbstregistrierung')
        cy.get('.m-footer-section__items').find('a[href="/en"]').should('exist').click()
        cy.location('pathname').should('eq', '/en')
        cy.get('h2').contains('Covid-19 self-registration')
      })
    })
  })
})
