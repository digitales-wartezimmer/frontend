describe('Shared', () => {
  describe('Menu', () => {
    before(() => {
      cy.acceptCookies()
      cy.setI18nCookie('de')
      cy.visit('/')
      cy.location('pathname').should('eq', '/')
    })
    describe('renders on desktop', () => {
      beforeEach(() => {
        cy.viewport(Cypress.config('viewportWidth'), Cypress.config('viewportHeight'))
      })

      it('renders the links to the main pages', () => {
        cy.get('.e-responsive-menu').find('a[href="/"]').should('contain', 'Home')
        cy.get('.e-responsive-menu').find('a[href="/project"]').should('contain', 'Das Projekt')
      })
    })

    describe('renders on mobile', () => {
      beforeEach(() => {
        cy.viewport('iphone-6')
      })
      it('renders in collapsed state', () => {
        cy.get('.e-responsive-menu').find('.e-responsive-menu__toggle').should('exist')
        cy.get('.e-responsive-menu').find('a').should('not.exist')
      })
      it('shows the menu links on click', () => {
        cy.get('.e-responsive-menu').find('.e-responsive-menu__toggle').click()
        cy.get('.e-responsive-menu').find('a[href="/"]').should('exist')
        cy.get('.e-responsive-menu').find('a[href="/project"]').should('exist')
        cy.get('.e-responsive-menu').find('.e-responsive-menu__toggle').click()
      })
      it('can close the menu again', () => {
        cy.get('.e-responsive-menu').find('.e-responsive-menu__toggle').click()
        cy.get('.e-responsive-menu').find('a').should('exist')
        cy.get('.e-responsive-menu').find('.e-responsive-menu__toggle').click()
        cy.get('.e-responsive-menu').find('a').should('not.exist')
      })
    })
  })
})
