describe('Shared', () => {
  describe('Footer', () => {
    before(() => {
      cy.acceptCookies()
      cy.setI18nCookie('de')
      cy.visit('/')
      cy.location('pathname').should('eq', '/')

      cy.get('footer').scrollIntoView()
    })
    it('includes all links', () => {
      cy.get('footer').find('a[href="/project"]').should('exist').and('contain', 'Das Projekt')
      cy.get('footer').find('a[href="/privacy"]').should('exist').and('contain', 'Datenschutz')
      cy.get('footer').find('a[href="/about"]').should('exist').and('contain', 'Impressum')
    })
    it('includes language switch links', () => {
      cy.get('footer').find('a[href="/en"]').should('exist').and('contain', 'Read in English')
    })
  })
})
