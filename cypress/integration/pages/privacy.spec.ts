describe('Pages', () => {
  describe('Privacy', () => {
    before(() => {
      cy.acceptCookies()
      cy.setI18nCookie('de')
      cy.visit('/privacy')
      cy.location('pathname').should('eq', '/privacy')
    })
    it('renders the page', () => {
      cy.contains('Datenschutzerklärung')
      cy.screenshot()
    })

    it('displays an email', () => {
      cy.contains('datenschutz@digitales-wartezimmer.org')
    })

    it('displays the legal information', () => {
      cy.contains('Digitales Wartezimmer GbR')
      cy.contains('Rostockerstraße 41')
      cy.contains('10553 Berlin')
    })
  })
})
