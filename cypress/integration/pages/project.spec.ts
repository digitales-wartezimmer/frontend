describe('Pages', () => {
  describe('Project', () => {
    before(() => {
      cy.acceptCookies()
      cy.setI18nCookie('de')
      cy.visit('/project')
      cy.location('pathname').should('eq', '/project')
    })
    it('renders the page', () => {
      cy.contains('Das Projekt')
      cy.screenshot()
    })

    it('renders the team members', () => {
      cy.get('.e-team__member').should('have.length', 12).each(($el) => {
        cy.wrap($el).find('a').should('have.attr', 'href').and('include', 'https://www.linkedin.com/in/')
        cy.wrap($el).find('img').should('exist')
      })
    })

    it('renders a contact button', () => {
      cy.get('.e-big-link[href="mailto:info@digitales-wartezimmer.org"]').should('contain', 'Kontakt')
    })

    it('renders a link to GitLab', () => {
      cy.get('.e-big-link[href="https://gitlab.com/digitales-wartezimmer"]').should('contain', 'GitLab')
    })
  })
})
