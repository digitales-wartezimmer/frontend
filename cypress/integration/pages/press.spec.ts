describe('Pages', () => {
  describe('Press', () => {
    before(() => {
      cy.acceptCookies()
      cy.setI18nCookie('de')
      cy.visit('/press')
      cy.location('pathname').should('contain', '/press')
    })
    it('renders the page', () => {
      cy.contains('Presse')
      cy.screenshot()
    })

    it('displays press articles', () => {
      cy.get('.e-article-card').within(() => {
        cy.get('a').should('have.attr', 'href').and('include', 'https://')
      })
    })

    it('can show more press articles', () => {
      cy.get('.e-article-card').should('have.length', 6)
      cy.get('.press-articles__load-button').click()
      cy.get('.e-article-card').its('length').should('be.gt', 6)
    })

    it('displays a contact link', () => {
      cy.get('.e-big-link[href="mailto:info@digitales-wartezimmer.org"]').should('contain', 'Kontakt')
    })
    it('displays a press material download link', () => {
      cy.get('.e-big-link[href="/press/material.zip"]').should('contain', 'Pressematerial')
    })
  })
})
