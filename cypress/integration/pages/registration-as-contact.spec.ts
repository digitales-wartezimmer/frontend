describe('Pages', () => {
  describe('Registration as Contact', () => {
    before(() => {
      cy.acceptCookies()
      cy.setI18nCookie('de')
      cy.visit('/registration-as-contact')
      cy.location('pathname').should('eq', '/registration-as-contact')
    })
    it('renders the page', () => {
      cy.contains('Digitales Wartezimmer')
      cy.screenshot()
    })

    it('renders the title', () => {
      cy.get('h2').contains('Covid-19 Kontaktpersonenformular')
    })

    it('renders the sponsored by bmbf logo', () => {
      cy.get('img[alt="Bundesministerium für Bildung und Forschung"]').should('exist')
    })

    it('renders a link to the forms', () => {
      cy.get('.e-button[href="/forms/registration-as-contact?startOver=true"]').should('contain', 'Formular ausfüllen')
    })
  })
})
