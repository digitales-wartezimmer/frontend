describe('Pages', () => {
  describe('Index', () => {
    before(() => {
      cy.acceptCookies()
      cy.setI18nCookie('de')
      cy.visit('/')
      cy.location('pathname').should('eq', '/')
    })
    it('renders the page', () => {
      cy.contains('Digitales Wartezimmer')
      cy.screenshot()
    })

    it('renders the title', () => {
      cy.get('h2').contains('Covid-19 Selbstregistrierung')
    })

    it('renders the sponsored by bmbf logo', () => {
      cy.get('img[alt="Bundesministerium für Bildung und Forschung"]').should('exist')
    })

    it('renders a link to the forms', () => {
      cy.get('.e-button[href="/forms/master?startOver=true"]').should('contain', 'Formular ausfüllen')
    })
  })
})
