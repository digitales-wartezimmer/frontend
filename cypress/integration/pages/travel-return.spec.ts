describe('Pages', () => {
  describe('Travel Return', () => {
    before(() => {
      cy.acceptCookies()
      cy.setI18nCookie('de')
      cy.visit('/travel-return')
      cy.location('pathname').should('eq', '/travel-return')
    })
    it('renders the page', () => {
      cy.contains('Digitales Wartezimmer')
      cy.screenshot()
    })

    it('renders the title', () => {
      cy.get('h2').contains('Covid-19 Reiserückkehrerformular')
    })

    it('renders the sponsored by bmbf logo', () => {
      cy.get('img[alt="Bundesministerium für Bildung und Forschung"]').should('exist')
    })

    it('renders a link to the forms', () => {
      cy.get('.e-button[href="/forms/travel-return?startOver=true"]').should('contain', 'Formular ausfüllen')
    })
  })
})
