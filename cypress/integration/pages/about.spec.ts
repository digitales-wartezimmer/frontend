describe('Pages', () => {
  describe('About', () => {
    before(() => {
      cy.acceptCookies()
      cy.setI18nCookie('de')
      cy.visit('/about')
      cy.location('pathname').should('eq', '/about')
    })
    it('renders the page', () => {
      cy.contains('Impressum')
      cy.screenshot()
    })

    it('displays an email', () => {
      cy.contains('info@digitales-wartezimmer.org')
    })

    it('displays the legal information', () => {
      cy.contains('Digitales Wartezimmer GbR')
      cy.contains('Rostockerstraße 41')
      cy.contains('10553 Berlin')
    })
  })
})
