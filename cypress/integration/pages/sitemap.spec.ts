describe('Pages', () => {
  describe('Sitemap', () => {
    before(() => {
      cy.acceptCookies()
      cy.setI18nCookie('de')
      cy.visit('/sitemap')
      cy.location('pathname').should('eq', '/sitemap')
    })
    it('renders the page', () => {
      cy.contains('Sitemap')
      cy.screenshot()
    })

    it('displays the links of the pages', () => {
      cy.get('h2').should('contain', 'Sitemap').parent().within(() => {
        cy.get('a[href="/"]')
        cy.get('a[href="/project"]')
        cy.get('a[href="/privacy"]')
        cy.get('a[href="/about"]')
      })
    })
  })
})
