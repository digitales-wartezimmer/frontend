import * as userData from '../../../fixtures/forms/userData/indexCase.json'

describe('Features', () => {
  describe('Forms', () => {
    describe('Index Case', () => {
      beforeEach(() => {
        cy.acceptCookies()
        cy.setI18nCookie('de')
        cy.clearLocalStorage('diwa')
        cy.visit('/forms/index-case', {
          onBeforeLoad: (win) => {
            win.sessionStorage.clear()
          },
        })
        cy.location('pathname').should('eq', '/forms/index-case/health_office')
        cy.server()
        cy.route('GET', '**/healthOffice*', 'fixture:forms/health-office/single.json').as('getHealthOffice')
        cy.route('POST', '**/indexCase', 'fixture:forms/submit-responses/indexCase.json').as('getSubmission')
        cy.route('GET', '**/plzInfo*', 'fixture:forms/cities.json').as('getCities')
      })

      it('without caseId', () => {
        const data = userData
        cy.formEnterSectionZip(data)
        cy.formEnterSectionIndexPersonalContactInformation(data)
        cy.formEnterSectionIndexContacts(data)
        cy.formEnterSectionPersonalNote(data)

        cy.contains(data.firstName)
        cy.contains(data.lastName)
        cy.contains(data.zip)
        cy.formEnterSectionReview(data)

        cy.wait('@getSubmission').then((xhr: any) => {
          cy.contains(xhr.response.body.data.healthOfficeEmail)
          cy.contains(xhr.response.body.data.subject)
          cy.contains(data.firstName)
          cy.contains(data.lastName)
          cy.contains(data.contacts[0].lastName)
          cy.contains(data.contacts[1].lastName)
        })
      })

      it('with caseId', () => {
        const data = { ...userData }
        data.caseId = '123abc'
        cy.formEnterSectionZip(data)
        cy.formEnterSectionIndexPersonalContactInformation(data)
        cy.formEnterSectionIndexContacts(data)
        cy.formEnterSectionPersonalNote(data)

        cy.contains(data.firstName)
        cy.contains(data.lastName)
        cy.contains(data.zip)
        cy.formEnterSectionReview(data)

        cy.wait('@getSubmission').then((xhr: any) => {
          cy.contains(xhr.response.body.data.healthOfficeEmail)
          cy.contains(xhr.response.body.data.subject)
          cy.contains(data.firstName)
          cy.contains(data.lastName)
          cy.contains(data.contacts[0].lastName)
          cy.contains(data.contacts[1].lastName)
        })
      })
      it('send to SORMAS', () => {
        cy.route('POST', '**/indexCase', 'fixture:forms/submit-responses/indexCaseSormas.json').as('getSubmission')
        const data = { ...userData }
        data.caseId = '123abc'
        cy.formEnterSectionZip(data)
        cy.formEnterSectionIndexPersonalContactInformation(data)
        cy.formEnterSectionIndexContacts(data)
        cy.formEnterSectionPersonalNote(data)

        cy.contains(data.firstName)
        cy.contains(data.lastName)
        cy.contains(data.zip)
        cy.formEnterSectionReview(data)

        cy.wait('@getSubmission').then(() => {
          cy.get('.success-display__title').contains('Deine Daten wurden an das zuständige Gesundheitsamt übermittelt')
        })
      })
    })
  })
})
