import * as userData from '../../../fixtures/forms/userData/registration-as-contact.json'

describe('Features', () => {
  describe('Forms', () => {
    describe('Registration As Contact', () => {
      beforeEach(() => {
        cy.acceptCookies()
        cy.setI18nCookie('de')
        cy.clearLocalStorage('diwa')
        cy.visit('/forms/registration-as-contact', {
          onBeforeLoad: (win) => {
            win.sessionStorage.clear()
          },
        })
        cy.location('pathname').should('eq', '/forms/registration-as-contact/fetch_health_office')
        cy.server()
        cy.route('GET', '**/healthOffice*', 'fixture:forms/health-office/single.json').as('getHealthOffice')
        cy.route('POST', '**/patientSubmission', 'fixture:forms/submit-responses/patientSubmission.json').as('getSubmission')
        cy.route('GET', '**/plzInfo*', 'fixture:forms/cities.json').as('getCities')
      })

      it('contact by personal contact', () => {
        const data = userData
        cy.formEnterSectionZip(data)
        cy.formEnterSectionPersonalInformation(data)
        cy.formEnterSectionContactCategory(data)
        cy.formEnterSectionPersonalContactInformation(data)
        cy.formEnterSectionCategory(data)
        cy.formEnterSectionSymptoms(data)
        cy.formEnterSectionPreconditions(data)
        cy.formEnterSectionPersonalNote(data)

        cy.contains(data.firstName)
        cy.contains(data.lastName)
        cy.contains(data.street)
        cy.contains(data.zip)
        cy.contains(data.city)
        cy.contains(data.email)
        cy.contains(data.phone)
        cy.contains(data.contactFirstName)
        cy.contains(data.contactLastName)
        cy.contains(data.contactPhone)
        cy.contains(data.contactEmail)
        cy.formEnterSectionReview(data)

        cy.wait('@getSubmission').then((xhr: any) => {
          cy.contains(xhr.response.body.data.healthOfficeEmail)
          cy.contains(xhr.response.body.data.subject)
          cy.contains(data.firstName)
          cy.contains(data.lastName)
          cy.contains(data.street)
          cy.contains(data.zip)
          cy.contains(data.city)
          cy.contains(data.email)
          cy.contains(data.phone)
        })
      })

      it('contact by cwa', () => {
        const data = { ...userData }
        data.hasSymptoms = 'no'
        data.gender = 'F'
        data.sourceOfInfection = 'cwa'
        data.preconditionHeartDisease = 'no'
        cy.formEnterSectionZip(data)
        cy.formEnterSectionPersonalInformation(data)
        cy.formEnterSectionContactCategory(data)
        cy.formEnterSectionCWAContactInformation(data)
        cy.formEnterSectionSymptoms(data)
        cy.formEnterSectionPreconditions(data)
        cy.formEnterSectionPersonalNote(data)

        cy.contains(data.firstName)
        cy.contains(data.lastName)
        cy.contains(data.street)
        cy.contains(data.zip)
        cy.contains(data.city)
        cy.contains(data.email)
        cy.contains(data.phone)
        cy.formEnterSectionReview(data)

        cy.wait('@getSubmission').then((xhr: any) => {
          cy.contains(xhr.response.body.data.healthOfficeEmail)
          cy.contains(xhr.response.body.data.subject)
          cy.contains(data.firstName)
          cy.contains(data.lastName)
          cy.contains(data.street)
          cy.contains(data.zip)
          cy.contains(data.city)
          cy.contains(data.email)
          cy.contains(data.phone)
        })
      })

      it('contact by establishment', () => {
        const data = { ...userData }
        data.hasSymptoms = 'yes'
        data.gender = 'D'
        data.sourceOfInfection = 'establishment'
        data.preconditionHeartDisease = 'no'
        cy.formEnterSectionZip(data)
        cy.formEnterSectionPersonalInformation(data)
        cy.formEnterSectionContactCategory(data)
        cy.formEnterSectionEstablishmentContactInformation(data)
        cy.formEnterSectionSymptoms(data)
        cy.formEnterSectionPreconditions(data)
        cy.formEnterSectionPersonalNote(data)

        cy.contains(data.firstName)
        cy.contains(data.lastName)
        cy.contains(data.street)
        cy.contains(data.zip)
        cy.contains(data.city)
        cy.contains(data.email)
        cy.contains(data.phone)
        cy.formEnterSectionReview(data)

        cy.wait('@getSubmission').then((xhr: any) => {
          cy.contains(xhr.response.body.data.healthOfficeEmail)
          cy.contains(xhr.response.body.data.subject)
          cy.contains(data.firstName)
          cy.contains(data.lastName)
          cy.contains(data.street)
          cy.contains(data.zip)
          cy.contains(data.city)
          cy.contains(data.email)
          cy.contains(data.phone)
        })
      })
    })
  })
})
