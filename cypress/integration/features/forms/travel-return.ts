import * as userData from '../../../fixtures/forms/userData/travelReturn.json'

describe('Features', () => {
  describe('Forms', () => {
    describe('Travel Return', () => {
      beforeEach(() => {
        cy.acceptCookies()
        cy.setI18nCookie('de')
        cy.clearLocalStorage('diwa')
        cy.visit('/forms/travel-return', {
          onBeforeLoad: (win) => {
            win.sessionStorage.clear()
          },
        })
        cy.location('pathname').should('eq', '/forms/travel-return/health_office')
        cy.server()
        cy.route('GET', '**/healthOffice*', 'fixture:forms/health-office/single.json').as('getHealthOffice')
        cy.route('POST', '**/travelReturn', 'fixture:forms/submit-responses/travelReturn.json').as('getSubmission')
        cy.route('GET', '**/plzInfo*', 'fixture:forms/cities.json').as('getCities')
      })

      it('with test', () => {
        const data = userData
        cy.formEnterSectionZip(data)
        cy.formEnterSectionPersonalInformation(data)
        cy.formEnterSectionTravel(data)
        cy.formEnterSectionSymptoms(data)
        cy.formEnterSectionTest(data)
        cy.formEnterSectionPreconditions(data)
        cy.formEnterSectionPersonalNote(data)

        cy.contains(data.firstName)
        cy.contains(data.lastName)
        cy.contains(data.street)
        cy.contains(data.zip)
        cy.contains(data.city)
        cy.contains(data.email)
        cy.contains(data.phone)
        cy.formEnterSectionReview(data)

        cy.wait('@getSubmission').then((xhr: any) => {
          cy.contains(xhr.response.body.data.healthOfficeEmail)
          cy.contains(xhr.response.body.data.subject)
          cy.contains(data.firstName)
          cy.contains(data.lastName)
          cy.contains(data.street)
          cy.contains(data.zip)
          cy.contains(data.city)
          cy.contains(data.email)
          cy.contains(data.phone)
        })
      })

      it('without test', () => {
        const data = { ...userData }
        data.hasTested = 'no'
        data.gender = 'F'
        data.hasSymptoms = 'no'
        cy.formEnterSectionZip(data)
        cy.formEnterSectionPersonalInformation(data)
        cy.formEnterSectionTravel(data)
        cy.formEnterSectionSymptoms(data)
        cy.formEnterSectionTest(data)
        cy.formEnterSectionPreconditions(data)
        cy.formEnterSectionPersonalNote(data)

        cy.contains(data.firstName)
        cy.contains(data.lastName)
        cy.contains(data.street)
        cy.contains(data.zip)
        cy.contains(data.city)
        cy.contains(data.email)
        cy.contains(data.phone)
        cy.formEnterSectionReview(data)

        cy.wait('@getSubmission').then((xhr: any) => {
          cy.contains(xhr.response.body.data.healthOfficeEmail)
          cy.contains(xhr.response.body.data.subject)
          cy.contains(data.firstName)
          cy.contains(data.lastName)
          cy.contains(data.street)
          cy.contains(data.zip)
          cy.contains(data.city)
          cy.contains(data.email)
          cy.contains(data.phone)
        })
      })
    })
  })
})
