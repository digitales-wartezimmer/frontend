// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('acceptCookies', () => {
  cy.setCookie('cookie:accepted', 'true')
})

Cypress.Commands.add('setI18nCookie', (lang) => {
  cy.setCookie('i18n_redirected', lang)
})

Cypress.Commands.overwrite('screenshot', (originalFn, subject, name, options) => {
  if (Cypress.env('RECORD_KEY') || Cypress.env('FORCE_SCREENSHOTS') || (name && name.force) || (options && options.force)) {
    return originalFn(subject, name, options)
  } else {
    Cypress.log({
      $el: subject,
      name: 'screenshot',
      message: 'Skipped Screenshot',
    })
    return null
  }
})

Cypress.Commands.add('formEnterField', (id, value, type = 'field', options) => {
  const isSelectField = ['radio', 'switch', 'checkbox'].includes(type)
  const isDateField = type === 'date'
  const idPrefix = isDateField ? 'field' : type
  const elementId = `#${idPrefix}-${id}`

  if (isSelectField) {
    cy.get(`${elementId}-${value}`).check(value, options)
  } else if (isDateField) {
    cy.get(elementId).clear(options).type(value, options).blur()
    cy.get(elementId).click()
  } else {
    cy.get(elementId).clear({
      force: true,
      ...options,
    }).type(value, options).blur()
  }
})
Cypress.Commands.add('formSubmit', (options) => {
  cy.get('.p-form-index__buttons > .e-button--primary').should('exist').click(options)
})

Cypress.Commands.add('formEnterSectionZip', (data) => {
  cy.formEnterField('zip', data.zip)
  cy.get('.m-fetch-health-office__match-name').should('contain.text', 'Bezirksamt Mitte von Berlin')
  cy.formSubmit()
})

Cypress.Commands.add('formEnterSectionPersonalInformation', (data) => {
  cy.formEnterField('first-name', data.firstName)
  cy.formEnterField('last-name', data.lastName)
  cy.formEnterField('birthday', data.birthday, 'date')
  cy.formEnterField('gender', data.gender, 'switch', { force: true })
  cy.formEnterField('email', data.email)
  cy.formEnterField('phone', data.phone)
  cy.formEnterField('street', data.street)
  cy.get('#field-zip').should('have.value', data.zip).and('have.attr', 'disabled')
  cy.get('#field-city').should('have.value', 'Berlin Mitte')
  cy.formEnterField('city', data.city)
  cy.formSubmit()
})

Cypress.Commands.add('formEnterSectionContactCategory', (data) => {
  cy.formEnterField('source-of-infection', data.sourceOfInfection, 'radio')
  cy.formSubmit()
})

Cypress.Commands.add('formEnterSectionPersonalContactInformation', (data) => {
  cy.formEnterField('contact-first-name', data.contactFirstName)
  cy.formEnterField('contact-last-name', data.contactLastName)
  cy.formEnterField('contact-phone', data.contactPhone)
  cy.formEnterField('contact-email', data.contactEmail)
  cy.formEnterField('locality', data.locality)
  cy.formEnterField('last-contact', data.lastContact, 'date')
  cy.formSubmit()
})

Cypress.Commands.add('formEnterSectionIndexContacts', (data) => {
  data.contacts.forEach((contact, index) => {
    cy.formEnterField(`${index}_first-name`, contact.firstName, 'field', { force: true })
    cy.formEnterField(`${index}_last-name`, contact.lastName, 'field', { force: true })
    if (contact.phone.length > 0) {
      cy.formEnterField(`${index}_phone`, contact.phone, 'field', { force: true })
    }
    if (contact.email.length > 0) {
      cy.formEnterField(`${index}_email`, contact.email, 'field', { force: true })
    }
    if (contact.address && contact.address.street.length > 0) {
      cy.formEnterField(`${index}_street`, contact.address.street, 'field', { force: true })
    }
    if (contact.address && contact.address.houseNumber.length > 0) {
      cy.formEnterField(`${index}_house-number`, contact.address.houseNumber, 'field', { force: true })
    }
    if (contact.address && contact.address.zip.length > 0) {
      cy.formEnterField(`${index}_zip`, contact.address.zip, 'field', { force: true })
    }
    if (contact.address && contact.address.city.length > 0) {
      cy.formEnterField(`${index}_city`, contact.address.city, 'field', { force: true })
    }
    if (index < (data.contacts.length - 1)) {
      cy.get('.m-contacts > .e-button').click()
    }
  })
  cy.formSubmit()
})
Cypress.Commands.add('formEnterSectionIndexPersonalContactInformation', (data) => {
  cy.formEnterField('first-name', data.firstName)
  cy.formEnterField('last-name', data.lastName)
  if (data.caseId) {
    cy.formEnterField('case-id', data.caseId)
  }
  cy.formSubmit()
})

Cypress.Commands.add('formEnterSectionCWAContactInformation', (data) => {
  cy.formEnterField('count-risk-encounters', data.countRiskEncounters)
  cy.formEnterField('days-since-risk-encounter', data.dateLastRiskEncounter, 'field', { force: true })
  cy.formSubmit()
})

Cypress.Commands.add('formEnterSectionEstablishmentContactInformation', (data) => {
  cy.formEnterField('locality', data.locality)
  cy.formEnterField('last-contact', data.lastContact, 'date')
  cy.formSubmit()
})

Cypress.Commands.add('formEnterSectionCategory', (data) => {
  cy.formEnterField('category', data.contactCategory, 'radio')
  cy.formEnterField('reason', data.reason)
  cy.formSubmit()
})

Cypress.Commands.add('formEnterSectionSymptoms', (data) => {
  cy.formEnterField('has-symptoms', data.hasSymptoms, 'radio')
  if (data.hasSymptoms === 'yes') {
    cy.formEnterField('symptoms', data.symptoms)
    cy.formEnterField('begin-of-symptoms', data.beginOfSymptoms, 'date')
  }
  cy.formSubmit()
})
Cypress.Commands.add('formEnterSectionPreconditions', (data) => {
  cy.formEnterField('immune-disease', data.preconditionImmuneDisease, 'radio')
  cy.formEnterField('heart-disease', data.preconditionHeartDisease, 'radio')
  if (data.preconditionImmuneDisease === 'yes' || data.preconditionHeartDisease === 'yes') {
    cy.formEnterField('disease-explanation', data.preconditionDescription)
  }
  if (['F', 'D'].includes(data.gender)) {
    cy.formEnterField('pregnancy', data.pregnancy, 'radio')
    if (data.pregnancy === 'yes') {
      cy.formEnterField('pregnancy-week', data.pregnancyWeek)
    }
  }
  cy.formSubmit()
})
Cypress.Commands.add('formEnterSectionPersonalNote', (data) => {
  cy.formEnterField('personal-note', data.personalNote)
  cy.formSubmit()
})
Cypress.Commands.add('formEnterSectionReview', (data) => {
  cy.formEnterField('consent-given', data.consent, 'checkbox')
  cy.formSubmit()
})

Cypress.Commands.add('formEnterSectionTravel', (data) => {
  cy.formEnterField('travel-country', data.travelCountry)
  cy.formEnterField('travel-region', data.travelRegion, 'field', { force: true })
  cy.formEnterField('stay-duration', data.travelDuration)
  cy.formEnterField('date-of-entry', data.dateOfEntry, 'date')
  cy.formEnterField('mode-of-entry', data.modeOfEntry, 'checkbox')
  cy.formSubmit()
})
Cypress.Commands.add('formEnterSectionTest', (data) => {
  cy.formEnterField('has-tested', data.hasTested, 'radio')
  if (data.hasTested === 'yes') {
    cy.formEnterField('date-of-test', data.dateOfTest, 'date')
    cy.formEnterField('test-country', data.testCountry)
    cy.formEnterField('test-result', data.testResult, 'radio')
  }
  cy.formSubmit()
})
