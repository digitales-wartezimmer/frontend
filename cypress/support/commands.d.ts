/// <reference types="cypress" />
// eslint-disable-file

declare namespace Cypress {
  interface Chainable {
    /**
     * Custom command to accept cookie usage, to prevent cookie hint from showing.
     * @example cy.acceptCookies()
     */
    acceptCookies(): void
    /**
     * Custom command to set the cookie for i18n.
     * @example cy.setI18nCookie('en')
     */
    setI18nCookie(lang: string): void
    /**
     * Take a screenshot of the application under test and the Cypress Command Log.
     * The screenshot will only be taken in recorded sessions. A screenshot can be forced with the force option
     *
     * @see https://on.cypress.io/screenshot
     * @example
     *    cy.screenshot()
     *    cy.get(".post").screenshot()
     *    cy.screenshot({ force: true })
     */
    screenshot(options?: Partial<Loggable & Timeoutable & ScreenshotOptions & { force: boolean }>): Chainable<null>
    /**
     * Take a screenshot of the application under test and the Cypress Command Log and save under given filename.
     * The screenshot will only be taken in recorded sessions. A screenshot can be forced with the force option
     *
     * @see https://on.cypress.io/screenshot
     * @example
     *    cy.get(".post").screenshot("post-element")
     *    cy.get(".post").screenshot("post-element", { force: true })
     */
    screenshot(fileName: string, options?: Partial<Loggable & Timeoutable & ScreenshotOptions & { force: boolean }>): Chainable<null>

    /**
     * Custom command to enter data in an input field
     *
     * @example
     *    cy.formEnterField('first-name', 'Andreas')
     *    cy.formEnterField('source-of-infection', 'personal', 'radio)
     */
    formEnterField(id: string, value: string, type?: 'field' | 'radio' | 'switch' | 'checkbox', options?: Object): void
    /**
     * Custom command to submit data in form
     *
     * @example
     *    cy.formSubmit()
     */
    formSubmit(options?: Object): void

    /**
     * Custom command to enter data from fixture into form for zip section
     * @example
     *    cy.formEnterSectionZip(data)
     */
    formEnterSectionZip(data: Object): void
    /**
     * Custom command to enter data from fixture into form for personal Information section
     * @example
     *    cy.formEnterSectionPersonalInformation(data)
     */
    formEnterSectionPersonalInformation(data: Object): void
    /**
     * Custom command to enter data from fixture into form for contact category section
     * @example
     *    cy.formEnterSectionContactCategory(data)
     */
    formEnterSectionContactCategory(data: Object): void
    /**
     * Custom command to enter data from fixture into form for personal contact information section
     * @example
     *    cy.formEnterSectionPersonalContactInformation(data)
     */
    formEnterSectionPersonalContactInformation(data: Object): void
    /**
     * Custom command to enter data from fixture into form for cwa contact information section
     * @example
     *    cy.formEnterSectionCWAContactInformation(data)
     */
    formEnterSectionCWAContactInformation(data: Object): void
    /**
     * Custom command to enter data from fixture into form for establishment contact information section
     * @example
     *    cy.formEnterSectionEstablishmentContactInformation(data)
     */
    formEnterSectionEstablishmentContactInformation(data: Object): void
    /**
     * Custom command to enter data from fixture into form for category section
     * @example
     *    cy.formEnterSectionCategory(data)
     */
    formEnterSectionCategory(data: Object): void
    /**
     * Custom command to enter data from fixture into form for symptoms section
     * @example
     *    cy.formEnterSectionSymptoms(data)
     */
    formEnterSectionSymptoms(data: Object): void
    /**
     * Custom command to enter data from fixture into form for preconditions section
     * @example
     *    cy.formEnterSectionPreconditions(data)
     */
    formEnterSectionPreconditions(data: Object): void
    /**
     * Custom command to enter data from fixture into form for personal note section
     * @example
     *    cy.formEnterSectionPersonalNote(data)
     */
    formEnterSectionPersonalNote(data: Object): void
    /**
     * Custom command to enter data from fixture into form for review section
     * @example
     *    cy.formEnterSectionReview(data)
     */
    formEnterSectionReview(data: Object): void
    /**
     * Custom command to enter data from fixture into form for travel section
     * @example
     *    cy.formEnterSectionTravel(data)
     */
    formEnterSectionTravel(data: Object): void
    /**
     * Custom command to enter data from fixture into form for test section
     * @example
     *    cy.formEnterSectionTest(data)
     */
    formEnterSectionTest(data: Object): void
    /**
     * Custom command to enter data from fixture into form for contact section
     * @example
     *    cy.formEnterSectionIndexContacts(data)
     */
    formEnterSectionIndexContacts(data: Object): void
    /**
     * Custom command to enter data from fixture into form for personal contact information section
     * @example
     *    cy.formEnterSectionIndexPersonalContactInformation(data)
     */
    formEnterSectionIndexPersonalContactInformation(data: Object): void
  }
}
