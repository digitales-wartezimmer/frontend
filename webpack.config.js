const path = require('path')

module.exports = {
  resolve: {
    // for WebStorm, not relevant for nuxt
    alias: {
      '@': path.resolve(__dirname),
      '~': path.resolve(__dirname),
    },
  },
}
