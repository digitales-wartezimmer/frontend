import { Context } from '@nuxt/types'

export default function ({ $axios, $toast, app }: Context) {
  $axios.onResponseError((error) => {
    const code = error.response ? error.response.status : undefined
    if (code && code === 401) {
      $toast.error(app.i18n.t('ui.auth.session_expired') as string, {
        action: {
          text: app.i18n.t('ui.auth.reload') as string,
          onClick: () => {
            location.reload()
          },
          class: 'e-toast__action',
        },
        singleton: true,
      })
    }
  })
}
