import { localize } from 'vee-validate'
import { Context } from '@nuxt/types'
import de from '~/content/locales/original.yaml'
import en from '~/content/locales/en.yaml'

export default function ({ app }: Context) {
  app.i18n.setLocaleMessage('de', de)
  app.i18n.setLocaleMessage('en', en)

  app.i18n.onLanguageSwitched = (oldLocale, newLocale) => { // eslint-disable-line @typescript-eslint/no-unused-vars
    localize(newLocale)
  }
}
