import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'

dayjs.extend(customParseFormat)

export const past = {
  validate: (value: string) => {
    const germanDate = dayjs(value, 'DD.MM.YYYY')
    if (germanDate.isValid()) {
      return germanDate.isBefore(dayjs(), 'day')
    }
    return dayjs(value).isBefore(dayjs(), 'day')
  },
}
