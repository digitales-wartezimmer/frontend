import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'

dayjs.extend(customParseFormat)

export const maxDaysBefore = {
  validate: (value: string, { otherValue, days }: any) => {
    let date = dayjs(value, 'DD.MM.YYYY')
    if (!date.isValid()) {
      date = dayjs(value)
    }
    let otherDate = dayjs(otherValue, 'DD.MM.YYYY')
    if (!otherDate.isValid()) {
      otherDate = dayjs(otherValue)
    }
    const dateShifted = otherDate.subtract(parseInt(days, 10), 'day')
    return dateShifted.isBefore(date)
  },
  params: ['otherValue', 'days'],
}
