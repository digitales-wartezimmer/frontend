import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'

dayjs.extend(customParseFormat)

export const alive = {
  validate: (value: string) => {
    const dead = dayjs('1900-01-01')
    const germanDate = dayjs(value, 'DD.MM.YYYY')
    if (germanDate.isValid()) {
      return germanDate.isAfter(dead, 'day')
    }
    return dayjs(value).isAfter(dead, 'day')
  },
}
