import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'

dayjs.extend(customParseFormat)

export const date = {
  validate: (value: string) => {
    return (dayjs(value).isValid() || dayjs(value, 'DD.MM.YYYY').isValid() || dayjs(value, 'YYYY-MM-DD').isValid())
  },
}
