import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'

dayjs.extend(customParseFormat)

export const notFuture = {
  validate: (value: string) => {
    const germanDate = dayjs(value, 'DD.MM.YYYY')
    if (germanDate.isValid()) {
      return !germanDate.isAfter(dayjs(), 'day')
    }
    return !dayjs(value).isAfter(dayjs(), 'day')
  },
}
