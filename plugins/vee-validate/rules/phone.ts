import { PhoneNumberUtil } from 'google-libphonenumber'
const phoneUtil = PhoneNumberUtil.getInstance()

export const phone = {
  validate: (value: string) => {
    try {
      const number = phoneUtil.parseAndKeepRawInput(value, 'DE')
      if (!number) {
        return false
      }
      return phoneUtil.isValidNumber(number)
    } catch {
      return false
    }
  },
}
