export const minArrayLength = {
  validate: (value: Array<string>, args: any) => {
    return {
      required: true,
      valid: value && Array.isArray(value) && value.length >= parseInt(args.length, 10),
    }
  },
  params: ['length'],
  computesRequired: true,
}
