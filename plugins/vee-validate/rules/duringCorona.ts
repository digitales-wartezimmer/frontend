import dayjs from 'dayjs'
import customParseFormat from 'dayjs/plugin/customParseFormat'

dayjs.extend(customParseFormat)

export const duringCorona = {
  validate: (value: string) => {
    const pandemic = dayjs('2019-12-01')
    const germanDate = dayjs(value, 'DD.MM.YYYY')
    if (germanDate.isValid()) {
      return germanDate.isAfter(pandemic, 'day')
    }
    return dayjs(value).isAfter(pandemic, 'day')
  },
}
