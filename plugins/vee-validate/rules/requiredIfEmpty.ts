
export const requiredIfEmpty = {
  params: ['target'],
  validate (value: any, { target }: any) {
    if (target && target.trim().length > 0) {
      return true
    }
    return !!value && value.trim().length > 0
  },
  computesRequired: true,
}
