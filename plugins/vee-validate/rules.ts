import { alive as aliveRule } from './rules/alive'
import { date as dateRule } from './rules/date'
import { duringCorona as duringCoronaRule } from './rules/duringCorona'
import { past as pastRule } from './rules/past'
import { notFuture as notFutureRule } from './rules/notFuture'
import { minArrayLength as minArrayLengthRule } from './rules/minArrayLength'
import { phone as phoneRule } from './rules/phone'
import { maxDaysBefore as maxDaysBeforeRule } from './rules/maxDaysBefore'
import { minDaysAfter as minDaysAfterRule } from './rules/minDaysAfter'
import { requiredIfEmpty as requiredIfEmptyRule } from './rules/requiredIfEmpty'

export const alive = aliveRule
export const date = dateRule
export const duringCorona = duringCoronaRule
export const past = pastRule
export const notFuture = notFutureRule
export const minArrayLength = minArrayLengthRule
export const phone = phoneRule
export const maxDaysBefore = maxDaysBeforeRule
export const minDaysAfter = minDaysAfterRule
export const requiredIfEmpty = requiredIfEmptyRule

export const messages = {
  de: {
    messages: {
      alive: 'Das Datum muss mindestens im 20. Jahrhundert liegen.',
      date: 'Das Format muss TT.MM.JJJJ entsprechen.',
      during_corona: 'Das Datum muss nach Beginn der Pandemie liegen.',
      past: 'Das Datum muss in der Vergangenheit liegen.',
      not_future: 'Das Datum darf nicht in der Zukunft liegen.',
      min_array_length: 'Es müssen mindestens {length} Elemente ausgewählt werden.',
      phone: 'Es muss eine gültige Telefonnummer angegeben werden (z.B. +49...).',
      max_days_before: 'Das {_field_} darf maximal {days} Tage vor dem Einreisedatum liegen.',
      min_days_after: 'Das {_field_} muss mindestens {days} Tage nach dem Einreisedatum liegen.',
      required_if_empty: '{_field_} ist ein Pflichtfeld.',
    },
  },
  en: {
    messages: {
      alive: 'The date must be in the 20th century, at least.',
      date: 'The format must correspond to DD.MM.YYYY, without spaces.',
      during_corona: 'The selected date must lie after the begin of the pandemic.',
      past: 'The selected date must lie in the past.',
      not_future: 'The selected date must not lie in the future.',
      min_array_length: 'At least {length} elements must be selected.',
      phone: 'A valid phone number needs to be provided (e.g. +49...).',
      max_days_before: 'The {_field_} must lie within {days} days before the date of entry.',
      min_days_after: 'The {_field_} must lie at least {days} days after the date of entry.',
      required_if_empty: '{_field_} is mandatory.',
    },
  },
}
