/* eslint-disable camelcase */
import Vue from 'vue'
import { extend, localize, ValidationObserver, ValidationProvider } from 'vee-validate'
import {
  alpha, email, length, max_value, max, min, min_value, numeric, regex, required,
} from 'vee-validate/dist/rules'
import en from 'vee-validate/dist/locale/en.json'
import de from 'vee-validate/dist/locale/de.json'
import {
  alive, date, duringCorona, past, messages, notFuture, minArrayLength, phone, maxDaysBefore, minDaysAfter, requiredIfEmpty,
} from './rules'

extend('alive', alive)
extend('alpha', alpha)
extend('date', date)
extend('during_corona', duringCorona)
extend('email', email)
extend('length', length)
extend('max', max)
extend('max_value', max_value)
extend('min', min)
extend('min_value', min_value)
extend('numeric', numeric)
extend('past', past)
extend('regex', regex)
extend('required', required)
extend('not_future', notFuture)
extend('min_array_length', minArrayLength)
extend('max_days_before', maxDaysBefore)
extend('min_days_after', minDaysAfter)
extend('phone', phone)
extend('required_if_empty', requiredIfEmpty)

Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)

localize({
  en,
  de,
})
localize(messages)
