import createPersistedState from 'vuex-persistedstate'
import { Context } from '@nuxt/types'
// @ts-ignore
const SecureLS = require('secure-ls')

export default ({ store, isHMR }: Context) => {
  if (process.browser) {
    if (isHMR) {
      return
    }
    let storage: any = window.sessionStorage
    if (process.env.NODE_ENV === 'production') {
      const lib = new SecureLS({ encodingType: 'aes' })
      // @ts-ignore
      lib.ls = {
        setItem: (key: string, value: any) => {
          if (key === (lib as any).utils.metaKey) {
            return window.localStorage.setItem(key, value)
          }
          return window.sessionStorage.setItem(key, value)
        },
        getItem: (key: string) => {
          if (key === (lib as any).utils.metaKey) {
            return window.localStorage.getItem(key)
          }
          return window.sessionStorage.getItem(key)
        },
        removeItem: (key: string) => window.sessionStorage.removeItem(key),
        get length () {
          return window.sessionStorage.length
        },
        key: (i: number) => window.sessionStorage.key(i),
      }
      storage = {
        getItem: (key: string) => lib.get(key),
        setItem: (key: string, value: any) => lib.set(key, value),
        removeItem: (key: string) => lib.remove(key),
      }
    }
    createPersistedState({
      storage,
      key: 'diwa',
      paths: [
        'registrationAsContact',
        'travelReturn',
      ],
    })(store)
  }
}
