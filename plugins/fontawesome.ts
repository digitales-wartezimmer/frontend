import Vue from 'vue'
import { library, config } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faAngleLeft, faArrowRight, faAt, faBars, faCheck, faCircle, faClone, faEdit, faEnvelope,
  faEnvelopeOpenText, faFax, faFileMedical, faLanguage, faExternalLinkAlt, faLock, faMapMarker,
  faMars, faPhone, faSlash, faSpellCheck, faTimes, faTransgenderAlt, faUsers, faVenus, faMailBulk,
  faQuestionCircle, faSpinner, faDownload, faPlus, faPaperclip, faCheckCircle,
} from '@fortawesome/free-solid-svg-icons'
import {
  faGitlab, faLinkedin, faTwitter, faYoutube,
} from '@fortawesome/free-brands-svg-icons'

// This is important, we are going to let Nuxt.js worry about the CSS
config.autoAddCss = false

library.add(
  faAngleLeft, faArrowRight, faAt, faBars, faCheck, faCircle, faClone, faEdit, faEnvelope,
  faEnvelopeOpenText, faFax, faFileMedical, faLanguage, faExternalLinkAlt, faLock, faMapMarker,
  faMars, faPhone, faSlash, faSpellCheck, faTimes, faTransgenderAlt, faUsers, faVenus, faMailBulk,
  faQuestionCircle, faSpinner, faDownload, faPlus, faPaperclip, faCheckCircle,
)

library.add(
  faGitlab, faLinkedin, faTwitter, faYoutube,
)

Vue.component('FontAwesomeIcon', FontAwesomeIcon)
