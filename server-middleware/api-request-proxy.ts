import express from 'express'
import requestProxy from 'express-request-proxy'

const app = express()

app.set('trust proxy', true)

app.all('/*', (req, res, next) => {
  const proxy = requestProxy({
    url: `${process.env.API_BASE_URL}${req.path}`,
    userAgent: false,
    headers: {
      Cookie: req.headers.cookie,
    },
  })
  proxy(req, res, next)
})

export default app
