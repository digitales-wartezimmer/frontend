export default {
  target: 'server',
  /*
  ** Headers of the page
  */
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'og:type', name: 'og:type', property: 'og:type', content: 'website' },
      { hid: 'og:image', name: 'og:image', property: 'og:image', content: `${process.env.BASE_URL}/share.png` },
      { hid: 'og:url', name: 'og:url', property: 'og:url', content: process.env.BASE_URL },
      { hid: 'twitter-image', name: 'twitter:image', content: `${process.env.BASE_URL}/share_twitter.jpg` },
      { hid: 'twitter-site', name: 'twitter:site', content: process.env.BASE_URL },
      { hid: 'twitter-creator', name: 'twitter:creator', content: '@Dig_Wartezimmer' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },
  router: {
    middleware: ['idempotency', 'api-authenticate', 'google-floc-opt-out'],
    extendRoutes: (routes) => {
      routes.push({ path: '/index-case', redirect: '/forms/index-case' })
      return routes
    },
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    'normalize.css/normalize.css',
    '@/assets/styles/main.scss',
    '@fortawesome/fontawesome-svg-core/styles.css',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/fontawesome.ts', ssr: true },
    { src: '~/plugins/i18n.ts', ssr: true },
    { src: '~/plugins/axios.ts', ssr: true },
    { src: '~/plugins/jsonld.ts', ssr: true },
    { src: '~/plugins/localStorage.ts', ssr: false },
    { src: '~/plugins/scroll-lock.ts', ssr: true },
    { src: '~/plugins/vee-validate/vee-validate.ts', ssr: true },
    { src: '~/plugins/vtooltip.ts', ssr: true },
    { src: '~/plugins/vue-datepicker.ts', ssr: true },
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://typescript.nuxtjs.org/guide/
    '@nuxt/typescript-build',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    'nuxt-helmet',
    ['cookie-universal-nuxt', { alias: 'cookies' }],
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    'nuxt-dayjs-module',
    '@nuxtjs/pwa',
    [
      'nuxt-i18n',
      {
        baseUrl: process.env.BASE_URL,
        seo: false, // see doc and layouts
        locales: [
          {
            code: 'en',
            iso: 'en-GB',
          },
          {
            code: 'de',
            iso: 'de-DE',
          },
        ],
        defaultLocale: 'de',
        strategy: 'prefix_and_default',
        vueI18n: {
          fallbackLocale: 'de',
        },
        vueI18nLoader: true,
      },
    ],
    '@nuxtjs/toast',
    '@nuxtjs/sitemap', // last to be loaded!
  ],
  dayjs: {
    locales: ['de', 'en'],
    defaultLocale: 'de',
  },
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: '/',
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config) {
      config.module.rules.push({
        test: /\.ya?ml$/,
        use: 'js-yaml-loader',
      })
    },
    transpile: [
      'vee-validate/dist/rules',
    ],
  },
  typescript: {
    typeCheck: {
      eslint: {
        enabled: true,
        files: ['./**/*.{ts,vue}'],
      },
    },
  },
  publicRuntimeConfig: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000',
    TWITTER_URL: process.env.TWITTER_URL || 'https://twitter.com/Dig_Wartezimmer',
    YOUTUBE_URL: process.env.YOUTUBE_URL || 'https://www.youtube.com/watch?v=9DmbGI-MK68',
    LINKEDIN_URL: process.env.LINKEDIN_URL || 'https://www.linkedin.com/company/digitales-wartezimmer',
  },
  privateRuntimeConfig: {
    API_TOKEN: process.env.API_TOKEN || 'API_TOKEN_NOT_SUPPLIED_TO_CLIENT',
    API_BASE_URL: process.env.API_BASE_URL || 'API_BASE_URL_NOT_SUPPLIED_TO_CLIENT',
  },
  // helmet options
  // @see https://helmetjs.github.io/docs/
  helmet: {},
  render: {
    csp: true,
  },
  serverMiddleware: [
    { path: '/api', handler: '~/server-middleware/api-request-proxy.ts' },
  ],
}
